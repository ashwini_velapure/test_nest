package com.nest.beans;

import com.qmetry.qaf.automation.data.BaseDataBean;

public class MyLeaveListBean extends BaseDataBean  {
	private String fromDate;
	private String toDate;
	private String leaveStatus;
	private String leaveType;
	

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public void setLeaveStatus(String leaveStatus) {
		this.leaveStatus = leaveStatus;
	}
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}
	public String getFromDate() {
		return fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public String getLeaveStatus() {
		return leaveStatus;
	}
	public String getLeaveType() {
		return leaveType;
	}

}
