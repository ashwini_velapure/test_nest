package com.nest.beans;

import com.qmetry.qaf.automation.data.BaseDataBean;
import com.qmetry.qaf.automation.data.BaseFormDataBean;
import com.qmetry.qaf.automation.ui.annotations.UiElement;
import com.qmetry.qaf.automation.util.Randomizer;

public class TeamLeaveListTableBean extends BaseFormDataBean {

	private String dateType;
	private String leaveDays;
	private String empName;
	private String project;
	private String leaveType;
	private String leaveStatus;
	private String action;

	public void setDateType(String datetype) {
		this.dateType = datetype;
	}
	public void setLeaveDays(String leavedays) {
		this.leaveDays = leavedays;
	}
	public void setEmpName(String empname) {
		this.empName = empname;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public void setLeaveType(String leavetype) {
		this.leaveType = leavetype;
	}
	public void setLeaveStatus(String leavestatus) {
		this.leaveStatus = leavestatus;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getDateType() {
		return dateType;
	}
	public String getLeaveDays() {
		return leaveDays;
	}
	public String getEmpName() {
		return empName;
	}
	public String getProject() {
		return project;
	}
	public String getLeaveType() {
		return leaveType;
	}
	public String getLeaveStatus() {
		return leaveStatus;
	}
	public String getAction() {
		return action;
	}

}
