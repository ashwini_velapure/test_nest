package com.nest.beans;

import com.qmetry.qaf.automation.data.BaseDataBean;

public class ApplyTeamLeaveBean extends BaseDataBean {

	private String empName;
	private String leaveType;
	private String leaveReason;
	private String dayType;
	private String comment;
	private String days;

	public String getEmpName() {
		return empName;
	}
	public String getLeaveType() {
		return leaveType;
	}
	public String getLeaveReason() {
		return leaveReason;
	}
	public String getDayType() {
		return dayType;
	}
	public String getComment() {
		return comment;
	}
	public String getDays() {
		return days;
	}
	public void setEmpName(String empname) {
		this.empName = empname;
	}
	public void setLeaveType(String leavetype) {
		this.leaveType = leavetype;
	}
	public void setLeaveReason(String leavereason) {
		this.leaveReason = leavereason;
	}
	public void setDayType(String daytype) {
		this.dayType = daytype;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public void setDays(String days) {
		this.days = days;
	}

}
