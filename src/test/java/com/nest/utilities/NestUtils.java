package com.nest.utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class NestUtils {

	static WebDriverWait wait = new WebDriverWait(getDriver(), 30);

	public static QAFExtendedWebDriver getDriver() {
		return new WebDriverTestBase().getDriver();
	}

	public static WebElement waitForElementToBeClickable(WebElement ele) {
		return wait.until(ExpectedConditions.elementToBeClickable(ele));
	}

	public static void clickUsingJavaScript(WebElement ele) {
		JavascriptExecutor executor =
				(JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].click();", ele);
	}

	public String checkForView() {
		if (CommonStep.getText("label.viewer.userhomepage") == "Manager View") {
			return "Manager View";
		} else {
			return "Employee View";
		}
	}

	public static List<QAFWebElement> getQAFWebElements(String loc) {
		return new QAFExtendedWebElement("xpath=/html").findElements(loc);
	}

	public static void scrollUpToElement(QAFWebElement ele) {
		JavascriptExecutor executor =
				(JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].scrollIntoView()", ele);
	}

	public static String getText(QAFExtendedWebDriver driver, WebElement ele) {
		return (String) ((JavascriptExecutor) driver)
				.executeScript("return jQuery(arguments[0]).text();", ele);
	}

	public static void clickUsingJavaScript(String loc, String... value) {
		clickUsingJavaScript(new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), value)));
	}

	private static String createLeaveDate(int increment) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMMM-yyyy");
		Calendar calendar = Calendar.getInstance();
		Date date = new Date();
		String today = dateFormat.format(date);
		try {
			calendar.setTime(dateFormat.parse(today));
		} catch (Exception e) {
			Reporter.log("Not able to parse the date." + e, MessageTypes.Fail);
		}
		calendar.add(Calendar.DATE, increment); // number of days to add
		String newDate = dateFormat.format(calendar.getTime());
		Date dt1 = null;
		try {
			dt1 = dateFormat.parse(newDate);
		} catch (Exception e) {
			Reporter.log("Not able to parse the date." + e, MessageTypes.Fail);
		}
		dateFormat.applyPattern("EEEE dd-MMMM-yyyy");
		return dateFormat.format(dt1);
	}

	public static String getLeaveDate(int increment) {
		String finalDay;
		String validDate = "";
		boolean flag = true;
		while (flag) {
			finalDay = createLeaveDate(increment);
			String[] array = finalDay.split(" ");
			if (array[0].equals("Saturday") || array[0].equals("Sunday")) {
				if (increment > 0)
					increment++;
				else increment--;
			} else {
				flag = false;
				validDate = array[1];
			}
		}
		return validDate;
	}

	public static void verifyMessagePresent(String loc, String message) {
		boolean result = false;
		List<QAFWebElement> elements = NestUtils.getQAFWebElements(loc);
		for (QAFWebElement ele : elements) {
			if (ele.getText().trim().toLowerCase().contains(message.toLowerCase())) {
				result = true;
				break;
			}
		}
		if (result)
			Reporter.log(message + " is present", MessageTypes.Pass);
		else Reporter.log(message + " is not present", MessageTypes.Fail);
	}
	
	public static boolean verifyMessageVisible(String loc, String message) {
		boolean result = false;
		List<QAFWebElement> elements = NestUtils.getQAFWebElements(loc);
		for (QAFWebElement ele : elements) {
			if (ele.getText().trim().toLowerCase().contains(message.toLowerCase())) {
				result = true;
				break;
			}
		}
		return result;
	}

	public static void closeAllStatusBars(String loc) {
		List<QAFWebElement> statusBars = NestUtils.getQAFWebElements(loc);
		for (QAFWebElement statusBar : statusBars) {
			if (statusBar.isDisplayed())
				statusBar.findElement(By.tagName("img")).click();
		}
	}
}
