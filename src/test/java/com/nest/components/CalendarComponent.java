package com.nest.components;

import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;

public class CalendarComponent extends QAFWebComponent {

	public CalendarComponent(String locator) {
		super(locator);
	}

	public CalendarComponent() {
		this("component.calendar.calendarcomponent");
	}

	private void selectYear(String date) {
		int expectedYear = Integer.parseInt(date.split("-")[2]);
		int actualYear;
		boolean flag = true;
		while (flag) {
			actualYear = Integer.parseInt(CommonStep
					.getText("label.month.year.calendarcomponent").trim().split(" ")[1]);
			if (expectedYear == actualYear) {
				flag = false;
			}
			if (actualYear < expectedYear) {
				CommonStep.click("button.arrow.right.calendarcomponent");
			}
		}
	}

	private void selectMonth(String date) {
		String expectedMonth = date.split("-")[1];
		CommonStep.click("label.month.year.calendarcomponent");
		NestUtils.clickUsingJavaScript("label.month.calendarcomponent", expectedMonth);
	}
	
	public void selectDate(String date) {
		selectYear(date);
		selectMonth(date);
		NestUtils.clickUsingJavaScript("label.date.calendarcomponent", date.split("-")[0].trim());
	}
}
