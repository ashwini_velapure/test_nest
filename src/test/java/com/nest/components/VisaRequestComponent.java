package com.nest.components;

import java.util.List;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class VisaRequestComponent extends QAFWebComponent {

	public VisaRequestComponent(String locator) {
		super(locator);
		// TODO Auto-generated constructor stub
	}

	@FindBy(locator = "column.employee.name")
	private List<QAFWebElement> employeeName;

	@FindBy(locator = "column.country")
	private List<QAFWebElement> country;

	@FindBy(locator = "column.type.of.visa")
	private List<QAFWebElement> typeOfVisa;

	@FindBy(locator = "column.initiated.on")
	private List<QAFWebElement> initiatedOn;

	@FindBy(locator = "column.visa.request.status")
	private List<QAFWebElement> visaRequestStatus;

	@FindBy(locator = "column.visa.status")
	private List<QAFWebElement> visaStatus;

	public List<QAFWebElement> getEmployeeName() {
		return employeeName;
	}

	public List<QAFWebElement> getCountry() {
		return country;
	}

	public List<QAFWebElement> getTypeOfVisa() {
		return typeOfVisa;
	}

	public List<QAFWebElement> getInitiatedOn() {
		return initiatedOn;
	}

	public List<QAFWebElement> getVisaRequestStatus() {
		return visaRequestStatus;
	}

	public List<QAFWebElement> getVisaStatus() {
		return visaStatus;
	}


}
