package com.nest.components;

import static com.qmetry.qaf.automation.step.CommonStep.*;

import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class DateBoxComponent extends QAFWebComponent {

	public DateBoxComponent(String locator) {
		super(locator);
		// TODO Auto-generated constructor stub
	}

	public QAFWebElement getLeaveType() {
		return parentElement.findElement("button.leavetype.applyleavepage");
		
	}

	public void selectLeaveType(String leaveType) {
		for (QAFWebElement option : NestUtils
				.getQAFWebElements("list.options.leavetype.applyleavepage")) {
			if (option.getText().trim().equalsIgnoreCase(leaveType.trim())) {
				option.click();
				break;
			}
		}
	}

}
