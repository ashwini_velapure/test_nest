package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.*;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class ExpensePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}
	int rowcount;
	static String titleValue;
	int randomNum;
	int num;

	@QAFTestStep(description = "user performs action on expense title name")
	public void click_On_ExpenseTitle() {
		waitForNotVisible("wait.loader");
		List<QAFWebElement> pageNumber = new QAFExtendedWebElement("xpath=/html")
				.findElements("number.page.expensepage");
		for (int i = 0; i < pageNumber.size(); i++) {
			pageNumber.get(i).click();
			if (new QAFExtendedWebElement("title.expense.expensepage").isPresent()) {
				CommonPage.clickUsingJavaScript("title.expense.expensepage");
				break;
			}
		}
	}
	public void verifyExpectedText(boolean flag) {
		if (flag) {
			Reporter.logWithScreenShot(
					"Expected element is present  in View Expense Page",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot(
					"Expected element is not present  in View Expense Page",
					MessageTypes.Fail);
		}
	}

	public List<QAFWebElement> getEmployeeDetails() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("level.employeedetails.viewexpensepage");
	}

	public List<QAFWebElement> getEmployeeDetailsVaue() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("text.employeedetails.viewexpense");
	}

	@QAFTestStep(description = "user verifies view expense details")
	public void verify_View_Expense_Details() {
		waitForNotVisible("wait.loader");
		boolean flag = CommonPage.verifyLocContainsText("title.expense.viewexpensepage",
				"security testing expense");
		verifyExpectedText(flag);
		Validator.verifyThat("back button is present",
				verifyPresent("button.back.viewexpensepage"), Matchers.equalTo(true));
		Validator.verifyThat("expensedetails is present",
				verifyPresent("expensedetails.viewexpensepage"), Matchers.equalTo(true));

		Validator.verifyThat("expensedate is present",
				verifyPresent("item.expensedate.viewexpensepage"),
				Matchers.equalTo(true));
		Validator.verifyThat("projectname is present",
				verifyPresent("item.projectname.viewexpensepage"),
				Matchers.equalTo(true));
		Validator.verifyThat("expensecategory is present",
				verifyPresent("expensecategory.viewexpensepage"), Matchers.equalTo(true));
		Validator.verifyThat("baseamount title is present",
				verifyPresent("title.baseamount.viewexpensepage"),
				Matchers.equalTo(true));

		Validator.verifyThat("billnumber title is present",
				verifyPresent("title.billnumber.viewexpensepage"),
				Matchers.equalTo(true));

		Validator.verifyThat("remarks title is present",
				verifyPresent("title.remarks.viewexpensepage"), Matchers.equalTo(true));
		Validator.verifyThat("attachment title is present",
				verifyPresent("title.attachment.viewexpensepage"),
				Matchers.equalTo(true));
		Validator.verifyThat("employeeid number is present",
				verifyPresent("number.employeeid.viewexpensepage"),
				Matchers.equalTo(true));
		for (int i = 0; i < getEmployeeDetails().size(); i++) {
			getEmployeeDetails().get(i).verifyPresent();
		}
		for (int j = 0; j < getEmployeeDetailsVaue().size(); j++) {
			getEmployeeDetailsVaue().get(j).verifyPresent();
		}
	}

	public List<QAFWebElement> getExpenseSummaryDetails() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("label.expensesummary.viewexpensepage");
	}
	@QAFTestStep(description = "user verifies expense summary details")
	public void verify_Expense_Summary_Details() {
		for (int i = 0; i < getExpenseSummaryDetails().size(); i++) {
			getExpenseSummaryDetails().get(i).verifyPresent();
		}
	}

	@QAFTestStep(description = "user navigates to view expense page")
	public void verify_Navigation_To_Viewexpense_Details() {
		waitForNotVisible("wait.loader");
		String viewexpense = driver.getCurrentUrl();
		if (viewexpense.contains("viewmyexpense")) {
			Reporter.log("view expense details page is open");
		} else {
			Reporter.logWithScreenShot("view expense details page is not open",
					MessageTypes.Fail);
		}
	}

	public List<QAFWebElement> getDropdownDetails(String alldropdownvalues) {
		return new QAFExtendedWebElement("xpath=/html").findElements(alldropdownvalues);
	}

	public void select_Value_From_Dropdown(String dropdownlocator,
			String dropdownvaluesloc, String visibletext) {
		CommonPage.clickUsingJavaScript(dropdownlocator);
		for (int i = 0; i < getDropdownDetails(dropdownvaluesloc).size(); i++) {
			if (getDropdownDetails(dropdownvaluesloc).get(i).getText()
					.equals(visibletext)) {
				getDropdownDetails(dropdownvaluesloc).get(i).click();
			}
		}
	}

	@QAFTestStep(description = "user chooses any value from Link to travel Request")
	public void choose_Value_From_LinkToTravel() {
		select_Value_From_Dropdown("button.linktotravel.newexpensepage",
				"options.linktotravel.expensepage", "office");
	}
	@QAFTestStep(description = "user performs action on new expense")
	public void click_On_NewExpense() {
		waitForNotVisible("wait.loader");
		CommonPage.clickUsingJavaScript("button.newexpense.expensepage");
		waitForNotVisible("wait.loader");
	}

	@QAFTestStep(description = "user verifies expense details")
	public void verify_LinkedTravel_Innercontent() {
		waitForNotVisible("wait.loader");
		List<QAFWebElement> linkedTravelContent = new QAFExtendedWebElement("xpath=/html")
				.findElements("text.linktravelrelated.expensepage");
		Reporter.log("" + linkedTravelContent.size());
		for (int i = 0; i < linkedTravelContent.size(); i++) {

			String value = linkedTravelContent.get(i).getText();

			Reporter.log("  " + i + linkedTravelContent.get(i).getText());
			if (value != null && !value.isEmpty()) {
				Reporter.logWithScreenShot(
						"Inner content value in linked travel expense is present",
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						"<font color='red'>Inner content value in linked travel expense is not present</font>");
			}
		}
	}

	@QAFTestStep(description = "user verifies travel details")
	public void verify_TravelDetails_Innercontent() {
		waitForNotVisible("wait.loader");
		List<QAFWebElement> travelContent = new QAFExtendedWebElement("xpath=/html")
				.findElements("text.traveldetails.expensepage");
		Reporter.log("" + travelContent.size());
		for (int i = 0; i < travelContent.size(); i++) {

			String value = travelContent.get(i).getText();

			Reporter.log("  " + i + travelContent.get(i).getText());
			if (value != null && !value.isEmpty()) {
				Reporter.logWithScreenShot(
						"Inner content value in travel details is present",
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						"<font color='red'>Inner content value in travel details is not present</font>");
			}
		}
	}
	@QAFTestStep(description = "user performs action on pending request")
	public void click_On_PendingRequest() {
		waitForNotVisible("wait.loader");
		CommonPage.clickUsingJavaScript("title.name.teamexpensepage");
		waitForNotVisible("wait.loader");
	}

	@QAFTestStep(description = "user verifies expense details for pending request")
	public void verify_Expense_Details_PendingRequest() {
		Validator.verifyThat("name title is present",
				verifyPresent("title.name.viewexpensepage"), Matchers.equalTo(true));
		Validator.verifyThat("approvalflow is present",
				verifyPresent("approvalflow.viewexpensepage"), Matchers.equalTo(true));
		for (int i = 0; i < NestUtils.getQAFWebElements("button.approve.viewexpensepage")
				.size(); i++) {
			NestUtils.getQAFWebElements("button.approve.viewexpensepage").get(i)
					.verifyPresent();
		}
		for (int j = 0; j < NestUtils
				.getQAFWebElements("text.employeedetails.managerviewexpensepage")
				.size(); j++) {
			String value = NestUtils
					.getQAFWebElements("text.employeedetails.managerviewexpensepage")
					.get(j).getText();

			Reporter.log("  " + j + NestUtils
					.getQAFWebElements("text.employeedetails.managerviewexpensepage")
					.get(j).getText());
			if (value != null && !value.isEmpty()) {
				Reporter.logWithScreenShot(
						"Inner content value in employee details is present",
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						"<font color='red'>Inner content value in employee details is not present</font>");
			}
		}
		for (int k = 0; k < NestUtils
				.getQAFWebElements("text.expensesumary.viewexpensepage").size(); k++) {
			String value =
					NestUtils.getQAFWebElements("text.expensesumary.viewexpensepage")
							.get(k).getText();

			Reporter.log("  " + k
					+ NestUtils.getQAFWebElements("text.expensesumary.viewexpensepage")
							.get(k).getText());
			if (value != null && !value.isEmpty()) {
				Reporter.logWithScreenShot(
						"Inner content value in expense summary details is present",
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						"<font color='red'>Inner content value in expense summary details is not present</font>");
			}
		}
		for (int l = 0; l < NestUtils.getQAFWebElements("items.expense.viewexpensepage")
				.size(); l++) {
			String value = NestUtils.getQAFWebElements("items.expense.viewexpensepage")
					.get(l).getText();

			Reporter.log("  " + l + NestUtils
					.getQAFWebElements("items.expense.viewexpensepage").get(l).getText());
			if (value != null && !value.isEmpty()) {
				Reporter.logWithScreenShot(
						"Inner content value in expense items is present",
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						"<font color='red'>Inner content value in expense items is not present</font>");
			}
		}
	}

	@QAFTestStep(description = "user click on new expense button")
	public void clickOnExpenseButton() {
		waitForNotVisible("wait.loader");
		verifyPresent("button.newexpense.expensepage");
		click("button.newexpense.expensepage");
	}
	@QAFTestStep(description = "user fill the required details")
	public void fillExpenseDetails() {
		titleValue = "Relocation";
		waitForNotVisible("wait.loader");
		verifyPresent("text.title.newexpensepage");
		verifyPresent("textbox.title.newexpensepage");
		sendKeys(titleValue, "textbox.title.newexpensepage");
		click("dropdown.project.newexpensepage");
		click("value.testclient.project.newexpensepage");
		verifyPresent("dropdown.expensecategory.newexpensepage");
		click("dropdown.expensecategory.newexpensepage");
		click("firstvalue.expensecategory.newexpensepage");
		verifyPresent("textbox.expenseamount.newexpensepage");
		sendKeys("11100", "textbox.expenseamount.newexpensepage");
	}

	@QAFTestStep(description = "user click on save button")
	public void clickonSaveButton() {
		waitForVisible("save.button.newexpensepage");
		QAFWebElement element = new QAFExtendedWebElement("save.button.newexpensepage");
		NestUtils.clickUsingJavaScript(element);
	}

	@QAFTestStep(description = "user click on reset button")
	public void clickOnResetButton() {

		QAFWebElement element = new QAFExtendedWebElement("reset.button.newexpensepage");
		NestUtils.clickUsingJavaScript(element);
	}

	public List<QAFWebElement> getSavedExpenseList() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("list.saved.expense.expensepage");
	}

	@QAFTestStep(description = "user details should be saved")
	public void checkUserDetailsSaved() {
		for (QAFWebElement savedexpense : getSavedExpenseList()) {
			if (savedexpense.getText().trim().contains(titleValue)) {
				Reporter.log("User Details has been Saved", MessageTypes.Pass);
			}
		}
	}

	public QAFWebElement getTitleTextbox() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElement("textbox.title.newexpensepage");
	}

	@QAFTestStep(description = "user details should be cleared")
	public void checkDetailsCleared() {
		verifyPresent("text.title.newexpensepage");
		if (getTitleTextbox().getAttribute("class").contains("ng-empty")) {
			Reporter.log("User details has been cleared", MessageTypes.Pass);
		} else {
			Reporter.log("User details not cleared", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verify new expense page")
	public void verifyNewExpensePage() {

		verifyPresent("textbox.title.newexpensepage");
		verifyPresent("dropdown.linktotravelrequest.newexpensepage");
		verifyPresent("datefield.expensedate.newexpensepage");
		verifyPresent("dropdown.project.newexpensepage");
		verifyPresent("dropdown.expensecategory.newexpensepage");
		verifyPresent("textbox.expenseamount.newexpensepage");
		verifyPresent("textbox.billnumber.newexpensepage");
		verifyPresent("textbox.remarks.newexpensepage");
		verifyPresent("attachment.newexpensepage");
		verifyPresent("globalattachments.newexpensepage");
		verifyPresent("addrow.button.newexpensepage");
		verifyPresent("submit.button.newexpensepage");
		verifyPresent("save.button.newexpensepage");
		verifyPresent("back.button.newexpensepage");
		verifyPresent("reset.button.newexpensepage");
	}

	@QAFTestStep(description = "user verify new expense form")
	public void verifyNewExpenseForm() {

		verifyPresent("text.title.newexpensepage");
		verifyPresent("text.linktotravelrequest.newexpensepage");
		verifyPresent("text.expensedate.newexpensepage");
		verifyPresent("text.project.newexpensepage");
		verifyPresent("text.expensecategory.newexpensepage");
		verifyPresent("text.expenseamount.newexpensepage");
		verifyPresent("text.billnumber.newexpensepage");
		verifyPresent("text.remarks.newexpensepage");
		verifyPresent("text.attachment.newexpensepage");
		verifyPresent("text.attachments.newexpensepage");
		verifyPresent("text.action.newexpensepage");
		verifyPresent("submit.button.newexpensepage");
		verifyPresent("save.button.newexpensepage");
		verifyPresent("reset.button.newexpensepage");
		verifyPresent("globalattachments.newexpensepage");
		verifyPresent("addrow.button.newexpensepage");
	}

	public List<QAFWebElement> getExpenseRow() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("list.expenserow.newexpensepage");
	}

	@QAFTestStep(description = "user click on add row button")
	public void clickOnAddRowButton() {
		waitForNotVisible("wait.loader");
		rowcount = getExpenseRow().size();

		verifyPresent("addrow.button.newexpensepage");
		click("addrow.button.newexpensepage");
	}

	@QAFTestStep(description = "user verify new blank row is added")
	public void verifyNewBlankRow() {
		waitForNotVisible("wait.loader");
		int newrowcount = getExpenseRow().size();
		if (newrowcount > rowcount) {
			Reporter.log("new blank row is added", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("new blank Row is not added", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verifies the breadcrumb {0}")
	public void verify_Breadcrumb(String title) {
		String navigationTitle =
				new QAFExtendedWebElement("title.reimbursement.expensepage").getText();
		if (navigationTitle.contains(title)) {
			Reporter.log("breadcrumb is present as per menu navigation",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("breadcrumb is not present as per menu navigation",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user navigates to view expense details page")
	public void verify_Viewexpense_Details_Navigation() {
		waitForNotVisible("wait.loader");
		String viewexpense = driver.getCurrentUrl();
		if (viewexpense.contains("viewexpense")) {
			Reporter.log("view expense details page is open in manager view",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot(
					"view expense details page is not open in manager view",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user performs action on pagination number")
	public void click_On_PageNumber() {
		waitForNotVisible("wait.loader");
		List<QAFWebElement> pageNumber = new QAFExtendedWebElement("xpath=/html")
				.findElements("number.page.expensepage");
		pageNumber.get(1).click();
	}

	@QAFTestStep(description = "user verifies page navigation")
	public void verify_Page_Navigation() {
		List<QAFWebElement> pageNumber = new QAFExtendedWebElement("xpath=/html")
				.findElements("number.page.expensepage");
		for (int i = 0; i < pageNumber.size(); i++) {
			pageNumber.get(i).click();
			if (new QAFExtendedWebElement("title.expense.expensepage").isPresent()) {
				Reporter.log("user is navigated to the particular page",
						MessageTypes.Pass);
				break;
			}
		}
	}

	@QAFTestStep(description = "user performs action on page number")
	public void click_On_PaginationNumber() {
		waitForNotVisible("wait.loader");
		List<QAFWebElement> pageNumber = new QAFExtendedWebElement("xpath=/html")
				.findElements("number.page.expensepage");
		NestUtils.scrollUpToElement(pageNumber.get(1));
		pageNumber.get(1).click();
	}

	@QAFTestStep(description = "user verifies page navigation in managerial view")
	public void verify_Requiredpage_Navigation() {
		if (verifyPresent("title.name.teamexpensepage")) {
			Reporter.log("user is navigated to the particular page", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("user is not navigated to the particular page",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user fill invalid details")
	public void fillInvalidExpenseDetails() {
		waitForNotVisible("wait.loader");
		verifyPresent("text.title.newexpensepage");
		click("dropdown.project.newexpensepage");
		click("value.testclient.project.newexpensepage");
		verifyPresent("dropdown.expensecategory.newexpensepage");
		click("dropdown.expensecategory.newexpensepage");
		click("firstvalue.expensecategory.newexpensepage");
		verifyPresent("textbox.expenseamount.newexpensepage");
		sendKeys("11100", "textbox.expenseamount.newexpensepage");
	}

	@QAFTestStep(description = "user verify error indication in form")
	public void verifyErrorIndication() {
		waitForNotVisible("wait.loader");
		verifyPresent("text.error.title.newexpensepage");
		final String input =
				new QAFExtendedWebElement("text.error.title.newexpensepage").getText();
		if (input.trim().equals("This is a required field.")) {
			Reporter.log("Error Indication appeared below Title field",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot(
					"Error Indication doesnot appeared below Title field",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user click on submit button")
	public void clickonSubmitButton() {

		waitForVisible("submit.button.newexpensepage");
		QAFWebElement element = new QAFExtendedWebElement("submit.button.newexpensepage");
		NestUtils.clickUsingJavaScript(element);
	}

	@QAFTestStep(description = "user verifies Teams Reimbursement List page")
	public void verify_TeamsReimbursement_Page() {
		waitForNotVisible("wait.loader");
		Validator.verifyThat("reimbursementlist title is present",
				verifyPresent("title.reimbursementlist.teamexpensepage"),
				Matchers.equalTo(true));
		Validator.verifyThat("reimbursement pagetitle is present",
				verifyPresent("pagetitle.reimbursement.teamexpensepage"),
				Matchers.equalTo(true));
		Validator.verifyThat("startdate option is present",
				verifyPresent("option.startdate.teamexpensepage"),
				Matchers.equalTo(true));
		Validator.verifyThat("enddate option is present",
				verifyPresent("option.enddate.teamexpensepage"), Matchers.equalTo(true));
		Validator.verifyThat("direct label is present",
				verifyPresent("label.direct.teamexpensepage"), Matchers.equalTo(true));
		List<QAFWebElement> buttons = new QAFExtendedWebElement("xpath=/html")
				.findElements("button.search.teamexpensepage");
		for (int i = 0; i < buttons.size(); i++) {
			buttons.get(i).verifyPresent();
		}
		List<QAFWebElement> calenderIcon = new QAFExtendedWebElement("xpath=/html")
				.findElements("icon.calender.teamexpensepage");
		for (int j = 0; j < calenderIcon.size(); j++) {
			calenderIcon.get(j).verifyPresent();
		}
		List<QAFWebElement> columnName = new QAFExtendedWebElement("xpath=/html")
				.findElements("title.tablecolumn.teamexpensepage");
		for (int k = 0; k < columnName.size(); k++) {
			columnName.get(k).verifyPresent();
		}
		List<QAFWebElement> pageNumber = new QAFExtendedWebElement("xpath=/html")
				.findElements("number.page.expensepage");
		for (int l = 0; l < pageNumber.size(); l++) {
			pageNumber.get(l).verifyPresent();
		}
		List<QAFWebElement> dropdown = new QAFExtendedWebElement("xpath=/html")
				.findElements("button.employee.teamexpensepage");
		for (int m = 0; m < dropdown.size(); m++) {
			dropdown.get(m).verifyPresent();
		}
	}

	@QAFTestStep(description = "user verifies my expense list page")
	public void verify_MyExpenseList_Page() {
		waitForNotVisible("wait.loader");
		Validator.verifyThat("myexpense title is present",
				verifyPresent("title.myexpense.myexpensepage"), Matchers.equalTo(true));
		Validator.verifyThat("reimbursement pagetitle is present",
				verifyPresent("pagetitle.reimbursement.teamexpensepage"),
				Matchers.equalTo(true));
		Validator.verifyThat("newexpense button is present",
				verifyPresent("button.newexpense.expensepage"), Matchers.equalTo(true));
		List<QAFWebElement> columnTitle = new QAFExtendedWebElement("xpath=/html")
				.findElements("title.tablecolumn.myexpensepage");
		for (int i = 0; i < columnTitle.size(); i++) {
			columnTitle.get(i).verifyPresent();
		}
		List<QAFWebElement> pageNumber = new QAFExtendedWebElement("xpath=/html")
				.findElements("number.page.expensepage");
		for (int j = 0; j < pageNumber.size(); j++) {
			pageNumber.get(j).verifyPresent();
		}
	}

	@QAFTestStep(description = "user click on copy button")
	public void clickOnCopyButton() {
		waitForNotVisible("wait.loader");
		rowcount = getExpenseRow().size();
		verifyPresent("button.copy.newexpensepage");
		click("button.copy.newexpensepage");
	}
	@QAFTestStep(description = "current row should be copied")
	public void verifyRowCopied() {
		waitForNotVisible("wait.loader");
		int newrowcount = getExpenseRow().size();
		if (newrowcount > rowcount) {
			Reporter.log("current row copied", MessageTypes.Pass);
		} else Reporter.logWithScreenShot("current row not copied", MessageTypes.Fail);

	}

	public List<QAFWebElement> getDeleteButtonList() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("list.button.delete.newexpensepage");
	}
	@QAFTestStep(description = "user click on delete button")
	public void clickOnDeleteButton() {
		waitForNotVisible("wait.loader");
		rowcount = getExpenseRow().size();
		int size = getDeleteButtonList().size();
		getDeleteButtonList().get(size - 1).click();

	}
	@QAFTestStep(description = "row should be deleted")
	public void verifyRowDeleted() {
		waitForNotVisible("wait.loader");
		int newRowcount = getExpenseRow().size();
		if (newRowcount == 1) {
			Reporter.log("expense row deleted", MessageTypes.Pass);
		} else
			Reporter.logWithScreenShot("not able to delete expense row",
					MessageTypes.Fail);
	}

	public List<QAFWebElement> getstatusList() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("list.status.expensepage");
	}

	@QAFTestStep(description = "user verifies my expense List screen")
	public void verifyExpenseListScreen() {
		waitForNotVisible("wait.loader");
		String[] statusList =
				{"", "Pending (Manager)", "Approved, Paid", "Rejected (Manager)"};

		verifyPresent("text.requestDate.expensepage");
		verifyPresent("text.title.expensepage");
		verifyPresent("text.approvalstatus.expensepage");
		verifyPresent("button.newexpense.expensepage");
		verifyPresent("number.page.expensepage");

		for (int i = 0; i <= getstatusList().size() - 1; i++) {
			Arrays.asList(statusList).contains(getstatusList().get(i).getText());
		}
	}

	@QAFTestStep(description = "user verifies expense category screen")
	public void verifyExpenseCategoryScreen() {

		waitForNotVisible("wait.loader");
		verifyPresent("text.column.expensecategory.expensecategorypage");
		verifyPresent("text.column.categorydescription.expensecategorypage");
		verifyPresent("text.column.actual.expensecategorypage");
		verifyPresent("text.column.elligibleamount.expensecategorypage");
		verifyPresent("text.column.status.expensecategorypage");
		verifyPresent("text.column.action.expensecategorypage");
		verifyPresent("button.addexpensecategory.expensecategorypage");
		verifyPresent("number.page.expensepage");
		verifyPresent("list.toggleoption.expensecategorypage");
		verifyPresent("list.editicon.expensecategorypage");
	}

	@QAFTestStep(description = "user click on add expense category button")
	public void clickOnAddExpenseCategoryButton() {
		
			waitForVisible("button.addexpensecategory.expensecategorypage");
			QAFExtendedWebElement element = new QAFExtendedWebElement("button.addexpensecategory.expensecategorypage");
			NestUtils.clickUsingJavaScript(element);	
	}
	
	@QAFTestStep(description = "user verify add expense category screen")
	public void verifyAddExpenseCategoryScreen() {
		
		waitForNotVisible("wait.loader");
		waitForVisible("text.expensetitle.addexpensecategorypage");
		verifyPresent("text.expensetitle.addexpensecategorypage");
		verifyPresent("text.expensedescription.addexpensecategorypage");
		verifyPresent("text.actual.addexpensecategorypage");
		verifyPresent("text.status.addexpensecategorypage");
		verifyPresent("button.submit.addexpensecategorypage");
		verifyPresent("button.back.addexpensecategorypage");
		
		String value = new QAFExtendedWebElement("dropdown.actual.addexpensecategorypage").getText();
		if(value.contains("No")) {
			verifyPresent("text.eligibleamount.addexpensecategorypage");
			Reporter.logWithScreenShot("Eligible Amount text box is present", MessageTypes.Pass);
		}else{
			verifyNotVisible("text.eligibleamount.addexpensecategorypage");
			Reporter.logWithScreenShot("Eligible Amount text box is not present by default", MessageTypes.Pass);
		}	
	}
	 public int getRandomNum() {
		Random random = new Random();
		randomNum = random.nextInt(100) + 100;
		return randomNum;
	}
	 
		@QAFTestStep(description = "user fill valid details")
		public void fillValidDetails() {
			
			waitForNotVisible("wait.loader");
			waitForVisible("textbox.expensetitle.addexpensecategorypage");
			num= getRandomNum();
			titleValue="expense"+num;		
			sendKeys(titleValue, "textbox.expensetitle.addexpensecategorypage");
			sendKeys("description", "textbox.expensedescription.addexpensecategorypage");
			waitForPresent("dropdown.actual.addexpensecategorypage");
			click("dropdown.actual.addexpensecategorypage");
			click("value.yes.actualdropdown.addexpensecategorypage");
			waitForPresent("dropdown.status.addexpensecategorypage");
			click("dropdown.status.addexpensecategorypage");
			click("value.active.statusdropdown.addexpensecategorypage");	
		}
		
		public List<QAFWebElement> getExpenseCategoryList() {
			return new QAFExtendedWebElement("xpath=/html")
					.findElements("list.saved.expensecategory.expensecategorypage");
		}

		@QAFTestStep(description = "user details should be added")
		public void verifyUserDetailAdded() {
			waitForNotVisible("wait.loader");
			boolean status = false;
			
			for (QAFWebElement savedexpense : getExpenseCategoryList()) {	
				if (savedexpense.getText().trim().contains(titleValue)) {
					Reporter.log("User Details has been Added", MessageTypes.Pass);
					status = true;
				}
			}
			if (status == false)
				Reporter.logWithScreenShot("User Details not Added", MessageTypes.Fail);	
		}
		@QAFTestStep(description = "user fill invalid expense category details")
		public void fillInvalidExpense() {
			
			waitForNotVisible("wait.loader");
		
			sendKeys("description", "textbox.expensedescription.addexpensecategorypage");
			waitForPresent("dropdown.actual.addexpensecategorypage");
			click("dropdown.actual.addexpensecategorypage");
			click("value.yes.actualdropdown.addexpensecategorypage");
			waitForPresent("dropdown.status.addexpensecategorypage");
			click("dropdown.status.addexpensecategorypage");
			click("value.active.statusdropdown.addexpensecategorypage");	
		}
		
		@QAFTestStep(description = "user verify error highlighted")
		public void verifyErrorTitleBox() {
			verifyPresent("error.expensetitle.expensecategorypage");
			click("button.back.addexpensecategorypage");	
		}
		
		@QAFTestStep(description = "user click on submit button on expense category page")
		public void clickOnSubmit() {
			   waitForNotVisible("wait.loader");
			   waitForVisible("button.submit.addexpensecategorypage");
			   QAFExtendedWebElement element = new QAFExtendedWebElement("button.submit.addexpensecategorypage");
			   NestUtils.clickUsingJavaScript(element);
		}
		
		@QAFTestStep(description = "user click on back button on expense category page")
		public void clickOnBackButton() { 
			waitForNotVisible("wait.loader");
			waitForVisible("button.back.addexpensecategorypage");
			QAFExtendedWebElement element = new QAFExtendedWebElement("button.back.addexpensecategorypage");
			NestUtils.clickUsingJavaScript(element);
		}
		
		@QAFTestStep(description ="user details should not be saved")
		public void verifyUserDetailNotSaved() {
			
			waitForNotVisible("wait.loader");
			waitForVisible("list.saved.expensecategory.expensecategorypage");
			if (getExpenseCategoryList().get(0).getText().trim().equals(titleValue)) {
				Reporter.logWithScreenShot("User Details has been Added even after clickong on back", MessageTypes.Fail);
			}
		}
		
		@QAFTestStep(description ="user click on expense first available expense")
		public void a1() {
			waitForNotVisible("wait.loader");
			waitForVisible("list.saved.expense.expensepage");
			
			if (getSavedExpenseList().size()== 0) {
				Reporter.log("No expense list available", MessageTypes.Fail);
			}else {
				getSavedExpenseList().get(0).click();
				Reporter.log("clicked on first expense", MessageTypes.Pass);
			}
		}
		
		public List<QAFWebElement> getExpenseColumnList() {
			return new QAFExtendedWebElement("xpath=/html")
					.findElements("list.text.expensesummarytable.viewexpensepage");
		}
		
		@QAFTestStep(description = "user verifies view expense details screen elements")
		public void verifyViewExpenseScreen() {
			waitForNotVisible("wait.loader");
			waitForVisible("text.employeename.viewexpensepage");
			
			verifyPresent("text.employeename.viewexpensepage");
			verifyPresent("text.employeeid.viewexpensepage");	
			String[] employeecolumn ={"Group", "Designation", "Band","Location"};
			for (int i = 0; i <= getstatusList().size() - 1; i++) {
			Arrays.asList(employeecolumn).contains(getEmployeeDetails().get(i).getText());
				}
			
			String[] expenseDetails ={"Currency", "Employee Paid Expense", "Total Expense","Less Company Paid Expense", "Less Advance Cash", "Balance (Pay/Recover)"};
			for (int i = 0; i <= getstatusList().size() - 1; i++) {
			Arrays.asList(expenseDetails).contains(getExpenseColumnList().get(i).getText());
				}
	
			verifyPresent("item.expensedate.viewexpensepage");
			verifyPresent("item.projectname.viewexpensepage");
			verifyPresent("expensecategory.viewexpensepage");
			verifyPresent("title.baseamount.viewexpensepage");
			verifyPresent("title.billnumber.viewexpensepage");
			verifyPresent("title.remarks.viewexpensepage");
			verifyPresent("title.attachment.viewexpensepage");
			verifyPresent("text.approvalflow.viewexpensepage");
		}
		
		@QAFTestStep(description = "user verify team reimbursement screen")
		public void verifyTeamReimbursementPage() {
			
			waitForNotVisible("wait.loader");
			verifyPresent("option.startdate.teamexpensepage");
			verifyPresent("option.enddate.teamexpensepage");
			List<QAFWebElement> buttons = new QAFExtendedWebElement("xpath=/html")
					.findElements("button.search.teamexpensepage");
			for (int i = 0; i < buttons.size(); i++) {
				buttons.get(i).verifyPresent();
			}
			List<QAFWebElement> calenderIcon = new QAFExtendedWebElement("xpath=/html")
					.findElements("icon.calender.teamexpensepage");
			for (int j = 0; j < calenderIcon.size(); j++) {
				calenderIcon.get(j).verifyPresent();
			}
			verifyPresent("text.name.teamexpensepage");
			verifyPresent("label.direct.teamexpensepage");
			
			List<QAFWebElement> columnName = new QAFExtendedWebElement("xpath=/html")
					.findElements("title.tablecolumn.teamexpensepage");
			String[] statusList ={"Pending", "Approved", "Rejected","Canceled"};
			for (int i = 0; i <= getstatusList().size() - 1; i++) {
			Arrays.asList(statusList).contains(columnName.get(i).getText());
				}
		}

}
