package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.*;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.util.Validator;

public class MyProfilePage {

	@QAFTestStep(description = "verify band of employee is {0}", stepName = "verifyBandOfEmployeeIs")
	public void verifyBandOfEmployeeIs(String bandnumber) {
		Validator.verifyThat("Band " + bandnumber + "is visible",
				getText("band.name").toString().trim().contains(bandnumber), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verifies options for nominations for {0} user")
	public void verifyOptionsForNominations(String bandName) {
		if (bandName.equals("Band 6")) {
			verifyVisible("reward.you.made.my.day");
			verifyNotVisible("reward.pat.on.back");
			verifyNotVisible("reward.bright.spark");
		} else if (bandName.equals("Band 5")) {
			verifyVisible("reward.you.made.my.day");
			verifyVisible("reward.pat.on.back");
			verifyNotVisible("reward.bright.spark");
		} else {
			verifyVisible("reward.you.made.my.day");
			verifyVisible("reward.pat.on.back");
			verifyVisible("reward.bright.spark");
		}
	}

}
