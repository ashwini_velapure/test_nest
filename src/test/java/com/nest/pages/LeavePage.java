package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.clear;
import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.verifyVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;

import java.io.File;
import java.text.DecimalFormat;
import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;

import com.nest.beans.ApplyLeaveBean;
import com.nest.beans.ApplyTeamLeaveBean;
import com.nest.beans.TeamLeaveListTableBean;
import com.nest.beans.MyLeaveListBean;
import com.nest.components.CalendarComponent;
import com.nest.components.TeamLeaveListTableRowComponent;
import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class LeavePage extends WebDriverBaseTestPage<WebDriverTestPage> {
	CalendarComponent calendar = new CalendarComponent();
	static MyLeaveListBean myLeaveDataBean = new MyLeaveListBean();
	int date = 0;
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	public void verifySection() {
		CommonStep.assertPresent("label.holidaysListSection.userhomepage");
		Reporter.logWithScreenShot("Holidays list section is present showing Holidays");
	}

	@QAFTestStep(description = "user should see My Leave List page")
	public void verifyMyLeaveListPage() {
		waitForNotVisible("wait.loader");
		verifyPresent("common.breadcrumb");
		verifyPresent("label.leavesummary.header.myleavelistpage");
	}

	@QAFTestStep(description = "user should see Apply Team Leave page")
	public void verifyApplyTeamLeavePage() {
		waitForNotVisible("wait.loader");
		verifyPresent("common.breadcrumb");
	}

	@QAFTestStep(description = "user verifies the fields present on Apply Team Leave page")
	public void verifyFieldsOnMyLeaveListPage() {
		verifyPresent("button.emp.name.applyteamleavepage");
		verifyPresent("button.leave.type.applyteamleavepage");
		verifyPresent("label.leave.balance.applyteamleavepage");
		verifyPresent("button.leave.reason.applyteamleavepage");
		verifyPresent("date.from.leave");
		verifyPresent("date.to.leave");
		// verifyPresent("radio.daytype.leave.applyteamleavepage");
		verifyPresent("input.comment.applyteamleavepage");
		verifyPresent("button.apply.applyteamleavepage");
	}

	@QAFTestStep(description = "user fill {0} details and assign leave")
	public void applyEmployeeLeave(String key) {
		ApplyTeamLeaveBean data = new ApplyTeamLeaveBean();
		data.fillFromConfig(key);
		click("button.emp.name.applyteamleavepage");
		sendKeys(data.getEmpName(), "input.empname.search.applyteamleavepage");
		click("option.empname.searchresult.applyteamleavepage");
		click("button.leave.type.applyteamleavepage");
		NestUtils.clickUsingJavaScript(
				select("list.options.leavetype.applyteamleavepage", data.getLeaveType()));
		waitForNotVisible("wait.loader");
		String balance =
				new QAFExtendedWebElement("label.leave.balance.applyteamleavepage")
						.getAttribute("value");

		Reporter.log("Available PTOs : " + balance, MessageTypes.Info);

		fillToDate(NestUtils.getLeaveDate(2 + Integer.parseInt(data.getDays())));
		fillFromDate(NestUtils.getLeaveDate(2));

		click("button.leave.reason.applyteamleavepage");
		NestUtils.clickUsingJavaScript(select(
				"list.options.leavereason.applyteamleavepage", data.getLeaveReason()));
		if (Integer.parseInt(data.getDays()) == 0)
			NestUtils.clickUsingJavaScript("radio.daytype.leave.applyteamleavepage",
					data.getDayType());
		sendKeys(data.getComment(), "input.comment.applyteamleavepage");
		click("button.apply.applyteamleavepage");
		waitForNotVisible("wait.loader");
		applyLeaveAgain(data.getDays());
	}

	public void fillFromDate(String fromDate) {
		click("icon.calendar.from");
		calendar.selectDate(fromDate);
	}

	public void fillToDate(String toDate) {
		click("icon.calendar.to");
		calendar.selectDate(toDate);
	}

	private QAFWebElement select(String loc, String selOption) {
		QAFWebElement element = null;
		for (QAFWebElement option : NestUtils.getQAFWebElements(loc)) {
			if (option.getText().trim().toLowerCase()
					.contentEquals(selOption.toLowerCase())) {
				element = option;
				break;
			}
		}
		return element;
	}

	@QAFTestStep(description = "user see available leave page")
	public void userSeeAvailableLeavePage() {
		waitForNotVisible("wait.loader");
		Validator.verifyThat("Page Load Verification", driver.getTitle(),
				org.hamcrest.Matchers.containsString("Infostretch NEST"));
	}

	@QAFTestStep(description = "user should fill the form and apply for leave")
	public void userShouldFillTheFormAndApplyForLeave() {
		verifyPresent("from.start.date.applyleavepage");
		verifyPresent("to.end.date.applyleavepage");

		fillFromDate(NestUtils.getLeaveDate(2));
		waitForNotVisible("wait.loader");
		fillToDate(NestUtils.getLeaveDate(2));
		waitForNotVisible("wait.loader");

		verifyPresent("dropdown.leave.reason.applyleavepage");

		click("dropdown.leave.reason.applyleavepage");
		List<QAFWebElement> options =
				NestUtils.getQAFWebElements("options.leave.reasons.applyleavepage");
		for (QAFWebElement option : options) {
			if (option.getText().equalsIgnoreCase("")) {
				option.click();
			} else continue;
		}
		CommonPage.waitForLocToBeClickable("button.apply.leave.applyleavepage");
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 250);");
		click("button.apply.leave.applyleavepage");

	}

	@QAFTestStep(description = "user should see team leave list page")
	public void verifyTeamLeaveListPage() {
		waitForNotVisible("wait.loader");
		verifyPresent("common.breadcrumb");
	}

	@QAFTestStep(description = "user verifies leaves list table of Team Leave List page")
	public void verifyTableHeadersTeamLeaveListPage() {
		String[] headers = ConfigurationManager.getBundle()
				.getString("teamleavelist.table.headers").split(":");
		for (String header : headers) {
			if (verifyTextPresent("list.headers.leavestable.teamleavelistpage",
					header.trim()))
				Reporter.log(header + " is present.", MessageTypes.Pass);
			else Reporter.log(header + " is not present.", MessageTypes.Fail);
		}
	}

	private boolean verifyTextPresent(String loc, String verifyText) {
		boolean present = false;
		for (QAFWebElement option : NestUtils.getQAFWebElements(loc)) {
			if (option.getText().trim().contains(verifyText)) {
				present = true;
				break;
			}
		}
		return present;
	}

	@QAFTestStep(description = "user verifies search related fields of Team Leave List page")
	public void verifySearchFieldsTeamLeaveListPage() {
		select("list.datetype.teamleavelistpage",
				ConfigurationManager.getBundle().getString("datetype"));
		verifyPresent("date.from.leave");
		verifyPresent("date.to.leave");
		verifyPresent("button.emp.name.teamleavelistpage");
		verifyPresent("button.account.teamleavelistpage");
		verifyPresent("button.leavetype.teamleavelistpage");
		verifyPresent("button.leavestatus.teamleavelistpage");
		verifyPresent("button.search.teamleavelistpage");
		verifyPresent("button.reset.teamleavelist");
	}

	@QAFTestStep(description = "user searches with deatils {0}")
	public void fillSearchCriteriadetails(String searchKey) {
		TeamLeaveListTableBean leavesdatabean = new TeamLeaveListTableBean();
		leavesdatabean.fillFromConfig(searchKey);
		select("list.datetype.teamleavelistpage", leavesdatabean.getDateType()).click();
		selectEmployee(leavesdatabean.getEmpName());
		waitForNotVisible("wait.loader");
		selectProject(leavesdatabean.getProject());
		waitForNotVisible("wait.loader");
		selectLeaveType(leavesdatabean.getLeaveType());
		selectLeaveStatus(leavesdatabean.getLeaveStatus());
		click("button.search.teamleavelistpage");
		waitForNotVisible("wait.loader");
	}

	public void selectEmployee(String empName) {
		click("button.emp.name.teamleavelistpage");
		CommonStep.clear("input.search.emp.name.teamleavelistpage");
		sendKeys(empName, "input.search.emp.name.teamleavelistpage");
		click("list.search.names.emp.name.teamleavelistpage");
	}

	public void selectProject(String projectName) {
		if (!projectName.isEmpty()) {
			click("button.account.teamleavelistpage");
			sendKeys(projectName, "input.search.project.teamleavelistpage");
			click("");
		}
	}

	private void selectLeaveType(String leaveType) {
		click("button.leavetype.teamleavelistpage");
		QAFWebElement element =
				select("list.options.leavetype.teamleavelistpage", leaveType);
		if (!element.findElement("xpath=.//span").getAttribute("class")
				.contains("glyphicon-ok"))
			element.click();
	}

	private void selectLeaveStatus(String leaveStatus) {
		click("button.leavestatus.teamleavelistpage");
		// select("list.options.leavestatus.teamleavelistpage", leaveStatus).click();

		QAFWebElement element =
				select("list.options.leavestatus.teamleavelistpage", leaveStatus);
		if (!element.findElement("xpath=.//span").getAttribute("class")
				.contains("glyphicon-ok"))
			element.click();
	}

	@QAFTestStep(description = "user verifies search results with {0}")
	public void verifySearchresults(String searchKey) {
		waitForNotVisible("wait.loader");
		TeamLeaveListTableBean leavesdatabean = new TeamLeaveListTableBean();
		leavesdatabean.fillFromConfig(searchKey);
		for (QAFWebElement element : NestUtils
				.getQAFWebElements("row.leavetable.teamleavelistpage")) {

			TeamLeaveListTableRowComponent tableData = new TeamLeaveListTableRowComponent(
					(QAFExtendedWebElement) element, "");

			Validator.verifyThat("Employee Name",
					tableData.getEmployeeNameAndId().getText(),
					Matchers.containsString(leavesdatabean.getEmpName()));

			if (leavesdatabean.getProject() != null)
				Validator.verifyThat("Project Name", tableData.getProject().getText(),
						Matchers.containsString(leavesdatabean.getProject()));

			Validator.verifyThat("Leave Type", tableData.getLeaveType().getText(),
					Matchers.containsString(leavesdatabean.getLeaveType()));

			Validator.verifyThat("Leave Status", tableData.getStatus().getText(),
					Matchers.containsString(leavesdatabean.getLeaveStatus()));
		}
	}

	@QAFTestStep(description = "user verifies reset functionality with {0}")
	public void resetFunctionality(String searchKey) {
		TeamLeaveListTableBean leavesdatabean = new TeamLeaveListTableBean();
		leavesdatabean.fillFromConfig(searchKey);

		String empName = CommonStep.getText("button.emp.name.teamleavelistpage");
		String projectName = CommonStep.getText("button.account.teamleavelistpage");
		String leaveType = CommonStep.getText("button.leavetype.teamleavelistpage");
		String leaveStatus = CommonStep.getText("button.leavestatus.teamleavelistpage");

		Reporter.logWithScreenShot("Before editing the fields.");
		select("list.datetype.teamleavelistpage", leavesdatabean.getDateType()).click();
		// right not not using, in future it may useful
		// click("icon.calendar.from");
		// click("icon.calendar.to");
		selectEmployee(leavesdatabean.getEmpName());
		waitForNotVisible("wait.loader");

		selectProject(leavesdatabean.getProject());
		waitForNotVisible("wait.loader");
		selectLeaveType(leavesdatabean.getLeaveType());
		selectLeaveStatus(leavesdatabean.getLeaveStatus());
		Reporter.logWithScreenShot("After editing the fields.");
		click("button.reset.teamleavelist");
		waitForNotVisible("wait.loader");
		Reporter.logWithScreenShot("Fields after clicking on reset button");
		Validator.verifyThat(CommonStep.getText("button.emp.name.teamleavelistpage"),
				Matchers.containsString(empName));
		Validator.verifyThat(CommonStep.getText("button.account.teamleavelistpage"),
				Matchers.containsString(projectName));
		Validator.verifyThat(CommonStep.getText("button.leavetype.teamleavelistpage"),
				Matchers.containsString(leaveType));
		Validator.verifyThat(CommonStep.getText("button.leavestatus.teamleavelistpage"),
				Matchers.containsString(leaveStatus));
	}

	public void verifyLeaveRequests() {
		CommonStep.assertVisible("lable.requestType.requests");
	}

	public void verifyExpenseRequests() {
		CommonStep.verifyText("lable.requestType.requests", "My Expense List");
	}

	public void verifyTravelRequests() {
		CommonStep.verifyText("lable.requestType.requests", "My Travel Requests");
	}

	public void verifyotherRequests() {
		CommonStep.verifyText("lable.requestType.requests", "Others");
	}

	public QAFExtendedWebElement LeavesRequest() {
		QAFExtendedWebElement LeaveRequest =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("link.RequestsLabel.requestpage"), "Leave "));
		return LeaveRequest;
	}

	public QAFExtendedWebElement expenseRequests() {
		QAFExtendedWebElement LeaveRequest =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("link.RequestsLabel.requestpage"), "Expense"));
		return LeaveRequest;
	}

	public QAFExtendedWebElement travelRequests() {
		QAFExtendedWebElement LeaveRequest =
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("link.RequestsLabel.requestpage"), "Travel"));
		return LeaveRequest;
	}

	@QAFTestStep(description = "user select Floating holidays")
	public void iSelectFloatingHolidays() {
		click("dropdownArrow.userhomepage");
	}

	public void applyLeaveAgain(String days) {
		boolean status1 = NestUtils.verifyMessageVisible("label.status.common",
				ConfigurationManager.getBundle()
						.getString("leave.already.applied.for.selected.date"));
		boolean status2 = NestUtils.verifyMessageVisible("label.status.common",
				ConfigurationManager.getBundle().getString("to.date.cannot.be.less"));
		int date = 0;
		while (status1 || status2) {
			NestUtils.closeAllStatusBars("label.status.common");
			fillToDate(NestUtils.getLeaveDate(((2 + date) + Integer.parseInt(days))));
			fillFromDate(NestUtils.getLeaveDate((2 + date)));
			click("button.apply.applyteamleavepage");
			waitForNotVisible("wait.loader");
			status1 = NestUtils.verifyMessageVisible("label.status.common",
					ConfigurationManager.getBundle()
							.getString("leave.already.applied.for.selected.date"));
			status2 = NestUtils.verifyMessageVisible("label.status.common",
					ConfigurationManager.getBundle().getString("to.date.cannot.be.less"));
			date = date + 2;
		}
	}

	@QAFTestStep(description = "user perform action on leave request with given {0} data")
	public void searchAndPerformActionOnLeaveRequest(String searchKey) {
		waitForNotVisible("wait.loader");
		TeamLeaveListTableBean leavesdatabean = new TeamLeaveListTableBean();
		leavesdatabean.fillFromConfig(searchKey);
		QAFWebElement element =
				NestUtils.getQAFWebElements("row.leavetable.teamleavelistpage").get(0);
		TeamLeaveListTableRowComponent tableData =
				new TeamLeaveListTableRowComponent((QAFExtendedWebElement) element, "");
		tableData.getActions().click();
		tableData.performAction(leavesdatabean.getAction());
		if (verifyVisible("label.title.popup.cancelrequest.teamleavelistpage")) {
			clear("input.comment.popup.cancelrequest.teamleavelistpage");
			sendKeys("Test : Cancel Leave Request",
					"input.comment.popup.cancelrequest.teamleavelistpage");
			click("button.submit.popup.cancelrequest.teamleavelistpage");
		}
		waitForNotVisible("wait.loader");
		click("button.submit.leaverequest.teamleavelistpage");
	}

	@QAFTestStep(description = "user approves all leave request with given {0} data")
	public void searchAndApproveAllLeaveRequest(String searchKey) {
		waitForNotVisible("wait.loader");
		TeamLeaveListTableBean leavesdatabean = new TeamLeaveListTableBean();
		leavesdatabean.fillFromConfig(searchKey);

		for (QAFWebElement element : NestUtils
				.getQAFWebElements("row.leavetable.teamleavelistpage")) {

			TeamLeaveListTableRowComponent tableData = new TeamLeaveListTableRowComponent(
					(QAFExtendedWebElement) element, "");
			tableData.getActions().click();
			tableData.performAction(leavesdatabean.getAction());
			if (verifyVisible("label.title.popup.cancelrequest.teamleavelistpage")) {
				clear("input.comment.popup.cancelrequest.teamleavelistpage");
				sendKeys("Test : Cancel Leave Request",
						"input.comment.popup.cancelrequest.teamleavelistpage");
				click("button.submit.popup.cancelrequest.teamleavelistpage");
			}
			click("button.approveall.leaverequests.teamleavelistpage");
		}
	}

	@QAFTestStep(description = "user selects {0} report and export it")
	public void exportLeaves(String exportType) {
		click("button.exportdropdown.teamleavelistpage");
		NestUtils.clickUsingJavaScript(
				select("list.options.exportdropdown.teamleavelistpage", exportType));
		click("button.export.teamleavelistpage");
	}

	@QAFTestStep(description = "user verifies exported report")
	public void verifyLeavesExported() {
		waitForNotVisible("wait.loader");
		boolean fileFound = false;
		File downloads = new File(System.getProperty("user.home") + "/downloads/");
		for (String file : downloads.list()) {
			if (verifyFilePresent(file)) {
				fileFound = true;
				new File(System.getProperty("user.home") + "/downloads/" + file + ".xls")
						.delete();
				break;
			}
		}
		if (fileFound)
			Reporter.log("File exported successfully.", MessageTypes.Pass);
		else Reporter.logWithScreenShot("File not exported.", MessageTypes.Fail);

	}

	private boolean verifyFilePresent(String fileName) {
		boolean isFileFound = false;
		String[] reportTypes = ConfigurationManager.getBundle()
				.getString("export.report.types").split(":");
		for (String reportType : reportTypes) {
			if (fileName.contains(reportType.trim())) {
				isFileFound = true;
				break;
			}
		}
		return isFileFound;
	}

	@QAFTestStep(description = "user should see available leave page")
	public void verifyAvailableLeavePage() {
		waitForNotVisible("wait.loader");
		verifyPresent("label.available.leaves.header.availableleavepage");
	}

	@QAFTestStep(description = "user should fill the form and apply for leave with data {0}")
	public void userApplyForOneFullDayLeave(String dataKey) {
		ApplyLeaveBean applyLeave = new ApplyLeaveBean();
		applyLeave.fillFromConfig(dataKey);

		fillToDate(
				NestUtils.getLeaveDate(2 + Integer.parseInt(applyLeave.getLeaveDays())));
		waitForNotVisible("wait.loader");
		fillFromDate(NestUtils.getLeaveDate(2));
		waitForNotVisible("wait.loader");

		click("dropdown.leave.reason.applyleavepage");
		NestUtils.clickUsingJavaScript(
				select("list.options.leavereason.applyteamleavepage",
						applyLeave.getLeaveReason()));
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 250);");
		if (!applyLeave.getLeaveType().isEmpty()) {
			setLeaveType(applyLeave.getLeaveType(), applyLeave.getUserType());
		}
		if (!applyLeave.getDayType().isEmpty()) {
			setDayType(applyLeave.getDayType());
		}
		click("button.apply.leave.applyleavepage");
		waitForNotVisible("wait.loader");
		applyAgainIfFails(applyLeave.getLeaveReason(), applyLeave.getLeaveDays(),
				applyLeave.getLeaveType(), applyLeave.getDayType(),
				applyLeave.getUserType());
	}

	public void applyAgainIfFails(String reason, String days, String leaveType,
			String dayType, String userType) {
		boolean status1 = NestUtils.verifyMessageVisible("label.status.common",
				ConfigurationManager.getBundle()
						.getString("leave.already.applied.for.selected.date"));
		boolean status2 = NestUtils.verifyMessageVisible("label.status.common",
				ConfigurationManager.getBundle().getString("to.date.cannot.be.less"));
		int date = 0;
		while (status1 || status2) {
			NestUtils.closeAllStatusBars("label.status.common");
			waitForNotVisible("wait.loader");
			fillToDate(NestUtils.getLeaveDate(((2 + date) + Integer.parseInt(days))));
			waitForNotVisible("wait.loader");
			fillFromDate(NestUtils.getLeaveDate((2 + date)));
			waitForNotVisible("wait.loader");

			click("dropdown.leave.reason.applyleavepage");
			NestUtils.clickUsingJavaScript(
					select("list.options.leavereason.applyteamleavepage", reason));
			((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 250);");
			if (!leaveType.isEmpty()) {
				setLeaveType(leaveType, userType);
			}
			if (!dayType.isEmpty()) {
				setDayType(dayType);
			}
			click("button.apply.leave.applyleavepage");
			waitForNotVisible("wait.loader");
			status1 = NestUtils.verifyMessageVisible("label.status.common",
					ConfigurationManager.getBundle()
							.getString("leave.already.applied.for.selected.date"));
			status2 = NestUtils.verifyMessageVisible("label.status.common",
					ConfigurationManager.getBundle().getString("to.date.cannot.be.less"));
			date = date + 2;
		}
	}

	@QAFTestStep(description = "user verifies leave request is not present in search results with {0}")
	public void verifySearchResult(String searchKey) {
		waitForNotVisible("wait.loader");
		TeamLeaveListTableBean leavesdatabean = new TeamLeaveListTableBean();
		leavesdatabean.fillFromConfig(searchKey);
		for (QAFWebElement element : NestUtils
				.getQAFWebElements("row.leavetable.teamleavelistpage")) {
			TeamLeaveListTableRowComponent tableData = new TeamLeaveListTableRowComponent(
					(QAFExtendedWebElement) element, "");
			tableData.getEmployeeNameAndId().verifyNotPresent();
		}
	}

	public void setLeaveType(String leaveType, String userType) {
		String from = new QAFExtendedWebElement("date.from.leave").getAttribute("value");
		String to = new QAFExtendedWebElement("date.to.leave").getAttribute("value");

		if (from.equalsIgnoreCase(to)) {
			new QAFExtendedWebElement("component.datebox.applyleavepage")
					.findElement("button.leavetype.applyleavepage").click();
			if (!userType.isEmpty())
				if (userType.equalsIgnoreCase("USA"))
					verifyLeaveTypes("us.user.leave.types");
				else if (userType.equalsIgnoreCase("UK"))
					verifyLeaveTypes("uk.user.leave.types");
			// we can use this to verify Leave types for INDIA location
			// else if(userType.equalsIgnoreCase("INDIA"))
			// verifyLeaveTypes("");
			selectLeaveTypeInBox(leaveType);
		} else {
			// Code need to be develop for leaves more than 1 day
		}
	}

	public void verifyLeaveTypes(String dataKey) {
		String[] leaveTypes =
				ConfigurationManager.getBundle().getString(dataKey).split(":");
		for (String leaveType : leaveTypes) {
			if (verifyTextPresent("list.options.leavetype.applyleavepage",
					leaveType.trim()))
				Reporter.log(leaveType + " is present.", MessageTypes.Pass);
			else Reporter.log(leaveType + " is not present.", MessageTypes.Fail);
		}
	}

	public void selectLeaveTypeInBox(String leaveType) {
		for (QAFWebElement option : NestUtils
				.getQAFWebElements("list.options.leavetype.applyleavepage")) {
			if (option.getText().trim().toLowerCase()
					.contentEquals(leaveType.toLowerCase())) {
				option.click();
				break;
			}
		}
	}

	public void setDayType(String dayType) {
		String from = new QAFExtendedWebElement("date.from.leave").getAttribute("value");
		String to = new QAFExtendedWebElement("date.to.leave").getAttribute("value");
		if (from.equalsIgnoreCase(to)) {
			new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
					.getString("button.day.type.applyleavepage"), dayType)).click();
		} else {
			// Code need to be develop for leaves more than 1 day
		}
	}

	@QAFTestStep(description = "user should fill the form and apply for back dated leave with data {0}")
	public void userApplyForBackDatedLeave(String dataKey) {
		ApplyLeaveBean applyLeave = new ApplyLeaveBean();
		applyLeave.fillFromConfig(dataKey);
		fillFromDate(NestUtils
				.getLeaveDate((-2 + Integer.parseInt(applyLeave.getLeaveDays()))));
		waitForNotVisible("wait.loader");
		fillToDate(NestUtils.getLeaveDate(-2));
		waitForNotVisible("wait.loader");
		click("dropdown.leave.reason.applyleavepage");
		NestUtils.clickUsingJavaScript(
				select("list.options.leavereason.applyteamleavepage",
						applyLeave.getLeaveReason()));
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 250);");

		if (!applyLeave.getLeaveType().isEmpty()) {
			setLeaveType(applyLeave.getLeaveType(), applyLeave.getUserType());
		}
		if (!applyLeave.getDayType().isEmpty()) {
			setDayType(applyLeave.getDayType());
		}
		click("button.apply.leave.applyleavepage");
		waitForNotVisible("wait.loader");
		applyAgainIfFails(applyLeave.getLeaveReason(), applyLeave.getLeaveDays(),
				applyLeave.getLeaveType(), applyLeave.getDayType(),
				applyLeave.getUserType());
	}

	@QAFTestStep(description = "user applies a special leave with data {0}")
	public void applySpecialLeave(String dataKey) {
		ApplyLeaveBean applyLeave = new ApplyLeaveBean();
		applyLeave.fillFromConfig(dataKey);
		click("button.specialleave.applyleavepage");
		verifyVisible("button.close.popup.applyleavepage");
		NestUtils.clickUsingJavaScript(selectSpecialLeave(
				"list.leave.type.popup.applyleavepage", applyLeave.getLeaveType()));
		fillToDate(
				NestUtils.getLeaveDate(2 + Integer.parseInt(applyLeave.getLeaveDays())));
		waitForNotVisible("wait.loader");
		fillFromDate(NestUtils.getLeaveDate(2));
		waitForNotVisible("wait.loader");
		click("button.leave.apply.popup.applyleavepage");
	}

	@QAFTestStep(description = "user verifies special leave message {0} is present")
	public void verifySpecialLeaveMessage(String message) {
		waitForNotVisible("wait.loader");
		if (verifyTextPresent("label.footer.message.popup.applyleavepage", message))
			Reporter.logWithScreenShot(message + " is present.", MessageTypes.Pass);
		else Reporter.logWithScreenShot(message + " is not present.", MessageTypes.Fail);
	}

	private QAFWebElement selectSpecialLeave(String loc, String selOption) {
		QAFWebElement element = null;
		for (QAFWebElement option : NestUtils.getQAFWebElements(loc)) {
			if (option.getText().trim().toLowerCase().contains(selOption.toLowerCase())) {
				element = option;
				break;
			}
		}
		return element;
	}
	@QAFTestStep(description = "user verifies leaves list table of My Leave List page")
	public void verifyTableHeadersMyLeaveListPage() {
		String[] headers = ConfigurationManager.getBundle()
				.getString("myLeaveList.table.headers").split(":");
		for (String header : headers) {
			if (verifyTextPresent("list.headers.leavestable.teamleavelistpage",
					header.trim()))
				Reporter.log(header + " is present.", MessageTypes.Pass);
			else Reporter.log(header + " is not present.", MessageTypes.Fail);
		}
	}
	
	@QAFTestStep(description = "user verifies the fields present on My Leave List page")
	public void verifyFieldsOnTeamLeaveListPage() {
		verifyPresent("date.from.leave");
		verifyPresent("date.to.leave");
		verifyPresent("button.leave.type.applyteamleavepage");
		verifyPresent("button.myLeaveList.status");	
	}

	@QAFTestStep(description = "user searches with criteria {0}")
	public void searchLeaveWithCriteria(String searchKey) {
		myLeaveDataBean.fillFromConfig(searchKey);
		setField("date.from.leave",myLeaveDataBean.getFromDate());
		waitForNotVisible("wait.loader");
		setField("date.to.leave",myLeaveDataBean.getToDate());
		waitForNotVisible("wait.loader");
		selectValueFromdrop("button.leave.type.applyteamleavepage",myLeaveDataBean.getLeaveType());
		waitForNotVisible("wait.loader");
		selectValueFromdrop("button.myLeaveList.status",myLeaveDataBean.getLeaveStatus());
		waitForNotVisible("wait.loader");
		click("button.myLeaveList.search");
		waitForNotVisible("wait.loader");
	}


	public void setField(String key, String value) {
		click(key);
		CommonStep.clear(key);
		sendKeys(value, key);
	}
	
	private void selectValueFromdrop(String key, String value) {
		waitForNotVisible("wait.loader");
		click(key);
		QAFWebElement element = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("link.dropdown.options"), value));
		waitForVisible(String.format(ConfigurationManager.getBundle().getString("link.dropdown.options"), value));
		element.click();
	}
	
	@QAFTestStep(description = "verify if user is able to search leave for the given criteria")
	public void verifyMyLeaveListTable() {
		List<QAFWebElement> listOfElement = NestUtils.getQAFWebElements(String.format(ConfigurationManager.getBundle().getString("list.myLeaveList.table.column"), "Status"));
		for(QAFWebElement element : listOfElement) {
			Validator.verifyThat(element.getText().toString().trim(),Matchers.equalToIgnoringWhiteSpace(myLeaveDataBean.getLeaveStatus()));
		}
		
	}

	@QAFTestStep(description = "user should navigate on {0} page")
	public void verifyNavigation(String expectedValue) {
		waitForNotVisible("wait.loader");
		verifyPresent("text.applyLeave.title");
		Validator.verifyThat(CommonStep.getText("text.applyLeave.title").trim(),Matchers.equalTo(expectedValue));
	}
	
	public void fillDate(String loc, String Date) {
		click(loc);
		calendar.selectDate(Date);
	}
	
	
	@QAFTestStep(description = "user apply leave for multiple days with data as {0}")
	public void applyLeaveForMultipleDays(String dataKey) 
	{
		
		ApplyLeaveBean applyLeave = new ApplyLeaveBean();
		applyLeave.fillFromConfig(dataKey);
		
		fillFromDate(NestUtils.getLeaveDate((2 + date)));
	    waitForNotVisible("wait.loader");
	    
		fillToDate(NestUtils.getLeaveDate((2 + date) + Integer.parseInt(applyLeave.getLeaveDays())));
		waitForNotVisible("wait.loader");
		
		click("dropdown.leave.reason.applyleavepage");
		NestUtils.clickUsingJavaScript(select("list.options.leavereason.applyteamleavepage", applyLeave.getLeaveReason()));
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 250);");
		
		if (!applyLeave.getLeaveType().isEmpty()) {
			Reporter.logWithScreenShot("user type : " + applyLeave.getUserType());
			setLeaveType(applyLeave.getLeaveType(), applyLeave.getUserType());
		}
		if (!applyLeave.getDayType().isEmpty()) {
			List<QAFWebElement> listOfElement = NestUtils.getQAFWebElements(String.format(ConfigurationManager.getBundle().getString("button.radio.dayType"), " Half  "));
			if(applyLeave.getDayType().equalsIgnoreCase("First-Half"))
				listOfElement.get(0).click();
			else
				listOfElement.get(listOfElement.size()-1).click();
		}
		click("button.apply.leave.applyleavepage");
		waitForNotVisible("wait.loader");
		
		boolean status1 = NestUtils.verifyMessageVisible("label.status.common",ConfigurationManager.getBundle().getString("leave.already.applied.for.selected.date"));
		boolean status2 = NestUtils.verifyMessageVisible("label.status.common",ConfigurationManager.getBundle().getString("to.date.cannot.be.less"));
	    if (status1 || status2) {
	    	NestUtils.closeAllStatusBars("label.status.common");
			date = date + 2;
			applyLeaveForMultipleDays(dataKey);
		}
	    if (!applyLeave.getDayType().isEmpty()) {
	    	 float leaveDays = Float.parseFloat(applyLeave.getLeaveDays());
	    	 leaveDays = (float) (leaveDays - 0.50);
	    	 DecimalFormat df = new DecimalFormat("0.00");
	    	 df.setMaximumFractionDigits(2);
	    	 String str_leaveDays = df.format(leaveDays);
	         verifyAppliedLeave("Leave Duration",str_leaveDays);
	    }else {
	    	 verifyAppliedLeave("Leave Duration",applyLeave.getLeaveDays());
	    }
	}
	
	@QAFTestStep(description = "user cancel leave from My leave list page")
	public void cancelLeaveFromMyLeaveList() 
	{
		waitForNotVisible("wait.loader");
		selectValueFromdrop("button.myLeaveList.select","Cancel");
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 300);");
		CommonStep.waitForEnabled("button.myLeaveList.submit");
		click("button.myLeaveList.submit");
		waitForNotVisible("wait.loader");
	}
	
	
	@QAFTestStep(description = "user apply half day leave with data as {0}")
	public void applyHalfDayLeave(String dataKey) 
	{
		ApplyLeaveBean applyLeave = new ApplyLeaveBean();
		applyLeave.fillFromConfig(dataKey);
		
		fillFromDate(NestUtils.getLeaveDate((date)));
	    waitForNotVisible("wait.loader");
	    
		fillToDate(NestUtils.getLeaveDate((date)));
		waitForNotVisible("wait.loader");
		
		click("dropdown.leave.reason.applyleavepage");
		NestUtils.clickUsingJavaScript(select("list.options.leavereason.applyteamleavepage", applyLeave.getLeaveReason()));
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 250);");
		
		if (!applyLeave.getLeaveType().isEmpty()) {
			Reporter.logWithScreenShot("user type : " + applyLeave.getUserType());
			setLeaveType(applyLeave.getLeaveType(), applyLeave.getUserType());
		}
		if (!applyLeave.getDayType().isEmpty()) {
			new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
					.getString("button.radio.dayType"), applyLeave.getDayType())).click();
		}
		click("button.apply.leave.applyleavepage");
		waitForNotVisible("wait.loader");
		
		boolean status1 = NestUtils.verifyMessageVisible("label.status.common",ConfigurationManager.getBundle().getString("leave.already.applied.for.selected.date"));
		boolean status2 = NestUtils.verifyMessageVisible("label.status.common",ConfigurationManager.getBundle().getString("to.date.cannot.be.less"));
	    if (status1 || status2) {
	    	NestUtils.closeAllStatusBars("label.status.common");
			date = date + 1;
			applyHalfDayLeave(dataKey);
		}
	    verifyAppliedLeave("Leave Duration",applyLeave.getDayType());
	}
	
	
	public void verifyAppliedLeave(String columnName, String exptectedValue) {
		if(columnName.equalsIgnoreCase("Leave Duration") && exptectedValue.matches("Second-Half|First-Half")) {
			exptectedValue = "0.50";
		  }else if(columnName.equalsIgnoreCase("Leave Duration") && !exptectedValue.contains(".")) {
			exptectedValue = exptectedValue+ ".00";
		  }
		List<QAFWebElement> listOfElement = NestUtils.getQAFWebElements(String.format(ConfigurationManager.getBundle().getString("list.myLeaveList.table.column"), columnName));
		Validator.verifyThat(listOfElement.get(0).getText().toString().trim(),Matchers.equalToIgnoringWhiteSpace(exptectedValue));
	}

	
	
}
