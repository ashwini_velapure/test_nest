package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.verifyText;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class CommonPage {

	static WebDriverWait wait = new WebDriverWait(NestUtils.getDriver(), 30);
	
	@QAFTestStep(description = "I scroll to {element}")
	public static void scrollUpToElement(String ele) {
		JavascriptExecutor executor =
				(JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].scrollIntoView()",
				new QAFExtendedWebElement(ele));
	}

	@QAFTestStep(description = "I click using javascript {loc}")
	public static void clickUsingJavaScript(String loc) {
		JavascriptExecutor executor =
				(JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].click();", new QAFExtendedWebElement(loc));
	}
	
	@QAFTestStep(description = "I wait for {loc} to be clickable")
	public static void waitForLocToBeClickable(String loc) {
		wait.until(
				ExpectedConditions.elementToBeClickable(new QAFExtendedWebElement(loc)));
	}

	@QAFTestStep(description = "verify {loc} text contains {text}")
	public static boolean verifyLocContainsText(String loc, String text) {
		return new QAFExtendedWebElement(loc).getText().contains(text);
	}

	
	@QAFTestStep(description = "user switch to manager view")
	public void switchToViews() {
		CommonStep.waitForEnabled("btn.SwitchingViews.userhomepage");
		CommonStep.click("btn.SwitchingViews.userhomepage");
		CommonStep.waitForNotVisible("loader.userhomepage");
	}
	
	@QAFTestStep(description = "user navigate back")
	public void iNavigateBack() {
		NestUtils.getDriver().navigate().back();
	}
	
	@QAFTestStep(description = "user change the view to {0}")
	public void switchView(String viewName) {
		waitForNotVisible("wait.loader");
		String actualView = CommonStep.getText("label.view.userhomepage");
		if (actualView.trim().toLowerCase().contentEquals(viewName.toLowerCase())) {
			Reporter.log(viewName + " is already selected.", MessageTypes.Pass);
		} else {
			click("button.view.userhomepage");
			waitForNotVisible("wait.loader");
		}
	}

	@QAFTestStep(description = "user verify the view {0}")
	public void verifyView(String viewName) {
		verifyText("label.view.userhomepage", viewName);
	}
	
	@QAFTestStep(description = "user verifies {0} message is present")
	public void verifyLeaveMessage(String message) {
		waitForNotVisible("wait.loader");
		NestUtils.verifyMessagePresent("label.status.common", message);
		NestUtils.closeAllStatusBars("label.status.common");
	}
}
