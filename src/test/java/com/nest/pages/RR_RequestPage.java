package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.verifyNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.verifyVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.hamcrest.Matchers;

import com.nest.components.RRRequestComponent;
import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.NotYetImplementedException;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class RR_RequestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "rr.requests.table")
	private List<RRRequestComponent> requestList;

	@FindBy(locator = "pagination.number.list")
	private List<QAFWebElement> paginations;

	public QAFExtendedWebElement getElement(String loc, String text) {
		QAFExtendedWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), text));
		return element;

	}

	@QAFTestStep(description = "user select {0} where status is {1}")
	public void userSelectWhereStatusIs(String filterLabel, String filterOption) {
		NestUtils.clickUsingJavaScript(getElement("filter.label", filterLabel));
		List<QAFWebElement> filterValues = NestUtils
				.getQAFWebElements(ConfigurationManager.getBundle().getString("filter.value.list"));
		for (QAFWebElement filterByValue : filterValues) {
			String username = filterOption.replace(".", " ");
			if (filterByValue.getText().toString().equals(username)) {
				if (filterLabel.equalsIgnoreCase("Select Nominee") || filterLabel.equalsIgnoreCase("Select Project")) {
					waitForVisible("input.search.box");
					sendKeys(username, "input.search.box");
				}
				NestUtils.scrollUpToElement(filterByValue);
				NestUtils.clickUsingJavaScript(filterByValue);
				break;
			}
		}
	}

	@QAFTestStep(description = "user click search button")
	public void clickSearch() {
		QAFWebElement searchButton = NestUtils.getDriver().findElement("search.button");
		NestUtils.scrollUpToElement(searchButton);
		waitForVisible("search.button");
		NestUtils.clickUsingJavaScript(searchButton);
	}

	@QAFTestStep(description = "user verifies R&R Requests where {0} is {1}")
	public void RRRequestsPage(String filtername, String filterValue) {
		verifyTableColumnsForRRRequests();
		verifyFilter(filtername, filterValue);
	}

	private void verifyFilter(String filtername, String filterValue) {
		List<QAFWebElement> statusList = NestUtils.getQAFWebElements(String
				.format(ConfigurationManager.getBundle().getString("request.list.table.column.values"), filtername));
		for (QAFWebElement status : statusList) {
			Validator.verifyThat("status is " + filterValue, status.getText().toString().trim(),
					Matchers.equalToIgnoringWhiteSpace(filterValue));
		}
	}

	public void verifyTableColumnsForRRRequests() {
		Validator.verifyThat("Nominator column is visible", getElement("table.column", "Nominee").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Reward Name column is visible", getElement("table.column", "Location").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Nominee column is visible", getElement("table.column", "Reward").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Department column is visible", getElement("table.column", "Posted Date").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Manager Status column is visible",
				getElement("table.column", "Manager Status").isDisplayed(), Matchers.equalTo(true));
		Validator.verifyThat("HR Status column is visible", getElement("table.column", "HR Status").isDisplayed(),
				Matchers.equalTo(true));
	}

	public void showAllButtons() {
		verifyVisible(String.format(ConfigurationManager.getBundle().getString("button.name"), "Back"));
		verifyVisible(String.format(ConfigurationManager.getBundle().getString("button.name"), "Approve"));
		verifyVisible(String.format(ConfigurationManager.getBundle().getString("button.name"), "Reject"));
	}

	private void showApprovebutton() {
		verifyVisible(String.format(ConfigurationManager.getBundle().getString("button.name"), "Back"));
		verifyVisible(String.format(ConfigurationManager.getBundle().getString("button.name"), "Approve"));
		// verifyNotVisible(String.format(ConfigurationManager.getBundle().getString("button.name"),
		// "Reject"));
	}

	public void showOnlyBackbutton() {
		verifyVisible(String.format(ConfigurationManager.getBundle().getString("button.name"), "Back"));
		verifyNotVisible(String.format(ConfigurationManager.getBundle().getString("button.name"), "Approve"));
		verifyNotVisible(String.format(ConfigurationManager.getBundle().getString("button.name"), "Reject"));

	}

	public boolean isApproveButtonVisible() {
		waitForNotVisible("loading.bar");
		NestUtils.scrollUpToElement(getElement("button.name", "Back"));
		QAFTestBase.pause(3000);
		if (getElement("button.name", "Approve").isPresent()) {
			return true;
		} else {
			waitForNotVisible("loading.bar");
			NestUtils.clickUsingJavaScript(getElement("button.name", "Back"));
			waitForNotVisible("loading.bar");
		}
		return false;
	}

	public void showButtonsAccordingtoStatus(String userrole, String statusName, String columnName) {
		NestUtils.scrollUpToElement(new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString("button.name"), "Back")));
		verifyVisible(String.format(ConfigurationManager.getBundle().getString("button.name"), "Back"));

		if (userrole.equals("manager") && (columnName.equals("Manager Status")) && statusName.equals("Pending")) {
			showAllButtons();
		} else if (userrole.equals("HR/Admin") && (columnName.equals("Manager Status"))
				&& statusName.equals("Approved")) {
			showAllButtons();
		} else if (userrole.equals("manager") && (columnName.equals("Manager Status"))
				&& statusName.equals("Approved")) {
			showOnlyBackbutton();
		} else if (userrole.equals("HR/Admin") && (columnName.equals("Manager Status"))
				&& statusName.equals("Pending")) {
			showOnlyBackbutton();
		} else if (userrole.equals("HR/Admin") && (columnName.equals("HR Status")) && statusName.equals("Pending")) {
			showApprovebutton();
		}

	}

	@QAFTestStep(description = "user verifies R&R Request list for {0} having staus {1} for {2}")
	public void vereifyRRRequestsList(String userType, String status, String fieldName) {
		showButtonsAccordingtoStatus(userType, status, fieldName);

	}

	@QAFTestStep(description = "user verify {page_name} of nominate module with all fileds")
	public void verifyNominatePages(String pageName) {

		CommonStep.verifyPresent("tab.cardselection.nominatepages");
		int totalCards = NestUtils.getDriver().findElements("cards.cardselection.nominatepages").size();
		switch (pageName) {
		case "You Made My Day":
			Validator.verifyThat("Select Card Block must have 5 cards.", totalCards == 5, Matchers.equalTo(true));
			CommonStep.verifyPresent("imput.comment.youmademydaypage");
			break;

		case "Pat On Back":
			Validator.verifyThat("Select Card Block must have 3 cards.", totalCards == 3, Matchers.equalTo(true));

			Validator.verifyThat("Enter Contributions text area should be present",
					new QAFExtendedWebElement(String.format(
							ConfigurationManager.getBundle().getString("imput.textarea"), "Enter Contributions"))
									.isPresent(),
					Matchers.equalTo(true));

			Validator
					.verifyThat("Select Project dropdown should be present",
							new QAFExtendedWebElement(String.format(
									ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"),
									"Select Project")).isPresent(),
							Matchers.equalTo(true));
			break;

		case "Bright Spark":
			Validator.verifyThat("Select Card Block must have 3 cards.", totalCards == 3, Matchers.equalTo(true));
			Validator.verifyThat("Enter challenging situations text area should be present",
					new QAFExtendedWebElement(
							String.format(ConfigurationManager.getBundle().getString("imput.textarea"),
									"Enter challenging situations")).isPresent(),
					Matchers.equalTo(true));
			Validator.verifyThat("Enter solutions provided text area should be present",
					new QAFExtendedWebElement(String.format(
							ConfigurationManager.getBundle().getString("imput.textarea"), "Enter solutions provided"))
									.isPresent(),
					Matchers.equalTo(true));
			Validator.verifyThat("Enter benefits accrued text area should be present",
					new QAFExtendedWebElement(String.format(
							ConfigurationManager.getBundle().getString("imput.textarea"), "Enter benefits accrued"))
									.isPresent(),
					Matchers.equalTo(true));
			Validator.verifyThat("Enter Contributions text area should be present",
					new QAFExtendedWebElement(String.format(
							ConfigurationManager.getBundle().getString("imput.textarea"), "Enter contributions"))
									.isPresent(),
					Matchers.equalTo(true));

			Validator
					.verifyThat("Select Project dropdown should be present",
							new QAFExtendedWebElement(String.format(
									ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"),
									"Select Project")).isPresent(),
							Matchers.equalTo(true));
			break;
		}

		Validator.verifyThat("Select groups dropdown should be present",
				new QAFExtendedWebElement(String.format(
						ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"), "Select Group"))
								.isPresent(),
				Matchers.equalTo(true));

		Validator.verifyThat("Select Band dropdown should be present",
				new QAFExtendedWebElement(String.format(
						ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"), "Select Band"))
								.isPresent(),
				Matchers.equalTo(true));

		Validator.verifyThat("Select Nominee dropdown should be present",
				new QAFExtendedWebElement(String.format(
						ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"), "Select Nominee"))
								.isPresent(),
				Matchers.equalTo(true));

		Validator.verifyThat("Post button should be present",
				new QAFExtendedWebElement(String
						.format(ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"), "Post"))
								.isPresent(),
				Matchers.equalTo(true));

		Validator.verifyThat("Preview button should be present",
				new QAFExtendedWebElement(String.format(
						ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"), "Preview"))
								.isPresent(),
				Matchers.equalTo(true));

		Validator.verifyThat("Cancel button should be present",
				new QAFExtendedWebElement(String
						.format(ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"), "Cancel"))
								.isPresent(),
				Matchers.equalTo(true));

	}

	@QAFTestStep(description = "user should see required filed with astric sign for {page_name} of nominate module")
	public void verifyAstersikSign(String pageName) {

		Validator.verifyThat("Select a Card field having astric sign",
				new QAFExtendedWebElement(String.format(
						ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"), "Select a Card"))
								.getText().contains("*"),
				Matchers.equalTo(true));

		Validator.verifyThat("Nominee field having astric sign",
				new QAFExtendedWebElement(String.format(
						ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"), "Nominee"))
								.getText().contains("*"),
				Matchers.equalTo(true));

		Validator
				.verifyThat("Message on Card field having astric sign",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"),
								"Message on Card")).getText().contains("*"),
						Matchers.equalTo(true));

		switch (pageName) {
		case "Pat On Back":
			Validator.verifyThat("Project field having astric sign",
					new QAFExtendedWebElement(String.format(
							ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"), "Project"))
									.getText().contains("*"),
					Matchers.equalTo(true));

			Validator
					.verifyThat("Key Contribution by Nominee field having astric sign",
							new QAFExtendedWebElement(String.format(
									ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"),
									"Key Contribution by Nominee")).getText().contains("*"),
							Matchers.equalTo(true));

			break;

		case "Bright Spark":
			Validator.verifyThat("Project field having astric sign",
					new QAFExtendedWebElement(String.format(
							ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"), "Project"))
									.getText().contains("*"),
					Matchers.equalTo(true));

			Validator
					.verifyThat("Challenging situations faced field having astric sign",
							new QAFExtendedWebElement(String.format(
									ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"),
									"Challenging situations faced")).getText().contains("*"),
							Matchers.equalTo(true));

			Validator
					.verifyThat("Solutions provided/Situation handling field having astric sign",
							new QAFExtendedWebElement(String.format(
									ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"),
									"Solutions provided/Situation handling")).getText().contains("*"),
							Matchers.equalTo(true));

			Validator
					.verifyThat("Rationale for Nomination field having astric sign",
							new QAFExtendedWebElement(String.format(
									ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"),
									"Rationale for Nomination")).getText().contains("*"),
							Matchers.equalTo(true));

			Validator
					.verifyThat("Benefits accrued field having astric sign",
							new QAFExtendedWebElement(String.format(
									ConfigurationManager.getBundle().getString("button.avlfields.nominatepages"),
									"Benefits accrued")).getText().contains("*"),
							Matchers.equalTo(true));
			break;

		}

	}

	@QAFTestStep(description = "user select the reward type {Bright Spark}")
	public void userSelectTheRewardType(String rewardType) {
		List<QAFWebElement> nominatorList = NestUtils.getQAFWebElements("column.nominatedBy.MyRewardPageListPage.rnr");
		int i = 0, numberOfRewards = 1;
		switch (rewardType) {
		case "Bright Spark":
			// if(numberOfRewards==1) {
			while (!nominatorList.isEmpty()) {
				nominatorList.get(i).click();
				if (CommonStep.verifyPresent("link.BrightSpark.MyRewardPageListPage.rnr")) {
					Reporter.log("Entered in if");
					CommonStep.click("link.BrightSpark.MyRewardPageListPage.rnr");
					break;
				}
				i++;
			}
			// }else {
			// get the list of all rewards available and click the latest one
			// }
			break;
		case "Pat On Back":
			// if(numberOfRewards==1) {
			while (!nominatorList.isEmpty()) {
				nominatorList.get(i).click();
				if (CommonStep.verifyPresent("link.PatOnBack.MyRewardPageListPage.rnr")) {
					Reporter.log("Entered in if");
					CommonStep.click("link.PatOnBack.MyRewardPageListPage.rnr");
					break;
				}
				Reporter.logWithScreenShot("PASS");
				i++;
			}
			// }else {
			// get the list of all rewards availabel and click the latest one
			// }
			break;

		case "You Made My Day":
			if (numberOfRewards == 1) {
				while (!nominatorList.isEmpty()
						&& !CommonStep.verifyPresent("link.YouMadeMyDay.MyRewardPageListPage.rnr")) {
					nominatorList.get(i).click();
					CommonStep.click("link.YouMadeMyDay.MyRewardPageListPage.rnr");
					i++;
				}
			} else {
				// get the list of all rewards availabel and click the latest
				// one
			}
			break;

		}

	}
	@SuppressWarnings("static-access")
	public String getRandomString(int count) {
		RandomStringUtils util = new RandomStringUtils();
		return util.randomAlphabetic(count);
	}

	@QAFTestStep(description = "user role {0} responds {1} request to {2} for reward {3}")
	public void approveOrRejectRequest(String usertype, String fromStatus, String toStatus, String reward) {
		NestUtils.scrollUpToElement(getElement("button.name", toStatus));
		getElement("button.name", toStatus).click();
		if (reward.equals("Pat On Back")) {
			if (usertype.equalsIgnoreCase("manager"))
				writeComment();
		} else if (reward.equals("Bright Spark")) {
			writeComment();
		}
	}

	private void writeComment() {
		String comment = getRandomString(20);
		sendKeys(comment, ConfigurationManager.getBundle().getString("input.comment.field"));
		getElement("button.name", "Submit").click();
		verifyVisible("success.approved.msg");
		waitForNotVisible("success.approved.msg");
		waitForNotPresent("loading.bar");
	}

	private void clickReward(List<RRRequestComponent> requestList, int i, int paginationNum) {

		requestList.get(i).getRewardName().get(i).waitForVisible();
		Reporter.log("in click rerward method:" + paginationNum + "index: " + i);
		ConfigurationManager.getBundle().setProperty("EXPECTED_NOMINEE",
				requestList.get(i).getNominee().get(i).getText());
		ConfigurationManager.getBundle().setProperty("EXPECTED_LOCATION",
				requestList.get(i).getLocationName().get(i).getText());
		ConfigurationManager.getBundle().setProperty("EXPECTED_REWARD",
				requestList.get(i).getRewardName().get(i).getText());
		ConfigurationManager.getBundle().setProperty("EXPECTED_MANAGER_STATUS",
				requestList.get(i).getManagerStatus().get(i).getText());
		ConfigurationManager.getBundle().setProperty("EXPECTED_HR_STATUS",
				requestList.get(i).getHrStatus().get(i).getText());
		ConfigurationManager.getBundle().setProperty("EXPECTED_POSTED_DATE",
				requestList.get(i).getPostedDate().get(i).getText());
		ConfigurationManager.getBundle().setProperty("INDEX", i);
		ConfigurationManager.getBundle().setProperty("PAGINATION_INDEX", paginationNum);
		NestUtils.clickUsingJavaScript(requestList.get(i).getRewardName().get(i));
	}

	@QAFTestStep(description = "verify status is changed from {0} to {1} for {2}")
	public void verifyStatusIsChanged(String fromStatus, String toStatus, String filterColumn) {
		String nominee = ConfigurationManager.getBundle().getProperty("EXPECTED_NOMINEE").toString();
		String location = ConfigurationManager.getBundle().getProperty("EXPECTED_LOCATION").toString();
		String reward = ConfigurationManager.getBundle().getProperty("EXPECTED_REWARD").toString();
		String managerstatus = ConfigurationManager.getBundle().getProperty("EXPECTED_MANAGER_STATUS").toString();
		String hrstatus = ConfigurationManager.getBundle().getProperty("EXPECTED_HR_STATUS").toString();
		String posteddate = ConfigurationManager.getBundle().getProperty("EXPECTED_POSTED_DATE").toString();
		int index = Integer.parseInt(ConfigurationManager.getBundle().getProperty("INDEX").toString());
		int pageIndex = Integer.parseInt(ConfigurationManager.getBundle().getProperty("PAGINATION_INDEX").toString());
		Reporter.log(requestList.get(index).getManagerStatus().get(index).getText());
		Reporter.log(pageIndex + ": pageIndex");
		Validator.verifyThat("Nominee name verified in list!",
				requestList.get(index).getNominee().get(index).getText().equals(nominee), Matchers.equalTo(true));
		Validator.verifyThat("location name verified in list!",
				requestList.get(index).getLocationName().get(index).getText().equals(location), Matchers.equalTo(true));
		Validator.verifyThat("Reward name verified in list!",
				requestList.get(index).getRewardName().get(index).getText().equals(reward), Matchers.equalTo(true));
		if (filterColumn.equals("manager")) {
			Validator.verifyThat("Manager staus verified in list!",
					requestList.get(index).getManagerStatus().get(index).getText().trim().equalsIgnoreCase(toStatus),
					Matchers.equalTo(true));
		} else {
			Validator.verifyThat("Manager staus verified in list!", requestList.get(index).getManagerStatus().get(index)
					.getText().trim().equalsIgnoreCase(managerstatus), Matchers.equalTo(true));

		}
		if (filterColumn.equalsIgnoreCase("HR/Admin")) {
			Validator.verifyThat("HR status appoved verified in list!",
					requestList.get(index).getHrStatus().get(index).getText().equals(toStatus), Matchers.equalTo(true));
		} else {
			Validator.verifyThat("HR status appoved verified in list!",
					requestList.get(index).getHrStatus().get(index).getText().equals(hrstatus), Matchers.equalTo(true));
		}

		Validator.verifyThat("Posted date appoved verified in list!",
				requestList.get(index).getPostedDate().get(index).getText().equals(posteddate), Matchers.equalTo(true));

	}

	public void sortColumn(String columnName) {
		getElement("column.name", columnName).waitForVisible();
		NestUtils.clickUsingJavaScript("column.name", columnName);
		waitForNotPresent(ConfigurationManager.getBundle().getString("loading.bar"));
		getElement("column.name", columnName).waitForVisible();
		NestUtils.clickUsingJavaScript("column.name", columnName);
		waitForNotPresent(ConfigurationManager.getBundle().getString("loading.bar"));
	}

	public boolean statusFoundFromRequests(String columnName, String status, int index) {
		if (columnName.equalsIgnoreCase("Manager Status")) {
			return requestList.get(index).getManagerStatus().get(index).getText().equalsIgnoreCase(status);
		} else if (columnName.equalsIgnoreCase("HR Status")) {
			return requestList.get(index).getHrStatus().get(index).getText().equals(status);
		}
		return false;

	}

	public void clickPagination(List<QAFWebElement> paginations, int index, int pageindex) {
		index = 0;
		waitForVisible("pagination.number.list");
		Reporter.log(paginations.size() + ":: ");
		paginations.get(pageindex).click();
		waitForNotVisible(ConfigurationManager.getBundle().getString("loading.bar"));
		pageindex++;
	}

	@QAFTestStep(description = "user clicks on {0} reward {1} by {2}")
	public void clickOnRewards(String reward, String status, String columnName) {
		int pageindex = 0;
		boolean RRRequestFound = false;
		boolean approveRecordfound = false;
		int size = requestList.size();

		for (int i = 0; i < size; i++) {
			boolean statusFound = statusFoundFromRequests(columnName, status, i);
			boolean rewardFound = requestList.get(i).getRewardName().get(i).getText().equalsIgnoreCase(reward);
			RRRequestFound = statusFound && rewardFound;
			approveRecordfound = false;
			if (RRRequestFound) {
				RRRequestFound = true;
				waitForNotVisible(ConfigurationManager.getBundle().getString("loading.bar"));
				clickReward(requestList, i, pageindex);
				waitForNotVisible(ConfigurationManager.getBundle().getString("loading.bar"));
				if (isApproveButtonVisible()) {
					approveRecordfound = true;
					break;
				} else {
					approveRecordfound = false;
					if (pageindex == paginations.size()) {
						break;
					}
				}
				if (!approveRecordfound && (i == size - 1) && (pageindex != paginations.size())) {
					i = 0;
					waitForVisible("pagination.number.list");
					NestUtils.clickUsingJavaScript(paginations.get(pageindex));
					waitForNotVisible(ConfigurationManager.getBundle().getString("loading.bar"));
					pageindex++;
				}

			} else {
				if (pageindex == paginations.size()) {
					break;
				}
				if ((i == size - 1) && (pageindex != paginations.size())) {
					i = 0;
					waitForVisible("pagination.number.list");
					Reporter.log(paginations.size() + ":: ");
					paginations.get(pageindex).click();
					waitForNotVisible(ConfigurationManager.getBundle().getString("loading.bar"));
					pageindex++;
				}
			}
		}
		if (!RRRequestFound || !approveRecordfound) {
			Reporter.log("No proper record found in existing request!", MessageTypes.Fail);
		}
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	@QAFTestStep(description = "usertype {0} clicks on {1} reward {2} by {3}")
	public void chooseStatusByUserRole(String usertype, String reward, String statusName, String columnName) {
		boolean RRRequestFound = false;
		int pageindex = 0;
		boolean approveRecordfound = false;

		for (int i = 0; i < requestList.size(); i++) {
			boolean statusFound = statusFoundFromRequests(columnName, statusName, i);
			boolean rewardFound = requestList.get(i).getRewardName().get(i).getText().equalsIgnoreCase(reward);
			RRRequestFound = statusFound && rewardFound;
			if (RRRequestFound) {
				requestList.get(i).getRewardName().get(i).click();
				break;
			} else {
				if (pageindex == paginations.size()) {
					break;
				}
				if ((i == requestList.size() - 1) && (pageindex != paginations.size())) {
					i = 0;
					waitForVisible("pagination.number.list");
					Reporter.log(paginations.size() + ":: ");
					paginations.get(pageindex).click();
					waitForNotVisible(ConfigurationManager.getBundle().getString("loading.bar"));
					pageindex++;
				}
			}
		}

	}

	@QAFTestStep(description = "user {0} nominates {1} for {2}")
	public void nominateAward(String username, String nomineeName, String awardName) {
		new UserHomePage().verifyHomePage();
		NavigationPage.menuNavigation(awardName);
		new ReportsPage().userSelectsACard();
		new RR_RequestPage().userSelectWhereStatusIs("Select Nominee", nomineeName);
		new RR_RequestPage().userSelectWhereStatusIs("Select Project", "test client");
		if (awardName.contains("Bright Spark")) {
			userEnterRequiredDetailsForBrightSpark();

		} else if (awardName.contains("Pat On Back")) {
			new ReportsPage().userEnterKeyContributions();
			new ReportsPage().postsComment();
		}
		new ReportsPage().userClickOnPostButton("Post");
	}

	@QAFTestStep(description = "Manager {0} of nominee {1} request for {2}")
	public void responseToRequestByManager(String username, String response, String awardName) {
		new LandingPage().switchToManagerView();
		NavigationPage.menuNavigation("R & R : R & R Requests");
		new RR_RequestPage().clickOnRewards(awardName, "Pending", "Manager Status");
		approveOrRejectRequest("manager", "pending", "Approve", awardName);
	}

	@QAFTestStep(description = "user selects and verify date")
	public void userSelectsAndVerifyDate() {

		String date = CommonStep.getText("datepiker.rnrrequestpage").toString();

		isValidDate(date);
		String fromDate = CommonStep.getText("datepiker.rnrrequestpage").toString();

		getCurrentSystemDate(fromDate);
	}

	public static boolean isValidDate(String inDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		dateFormat.setLenient(false);
		try {
			dateFormat.parse(inDate.trim());
		} catch (ParseException pe) {
			Reporter.logWithScreenShot("Date is not in the format dd-MM-yyyy", MessageTypes.Fail);
			return false;

		}
		Reporter.logWithScreenShot("Date is in the format dd-MM-yyyy", MessageTypes.Pass);
		return true;
	}

	public static boolean getCurrentSystemDate(String fromDate) {
		DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
		Date date = new Date(); // get current date time with Date()
		String date1 = dateFormat.format(date); // Now format the date
		// System.out.println("Current date and time is " +date1);
		if (fromDate.equalsIgnoreCase(date1)) {
			Reporter.logWithScreenShot("Current date is same as system date", MessageTypes.Pass);
			return true;
		} else {
			Reporter.logWithScreenShot("Current date is not same as system date", MessageTypes.Fail);
			return false;

		}

	}

	@QAFTestStep(description = "user should see reward card and back button")
	public void verifyRewardCardAndBackBtn() {
		CommonStep.verifyPresent("img.rewardCard.rnr");
		CommonStep.verifyPresent("btn.back.rnr");

	}

	@QAFTestStep(description = "user should see + sign on screen")
	public void userShouldSeeSignOnScreen() {
		CommonStep.verifyPresent("symbol.MyRewardPageList.rnr");
	}

	@QAFTestStep(description = "user should see pagination")
	public void userShouldSeePagination() {
		CommonStep.verifyPresent("pagination.MyRewardListPage.rnr");
	}


	@QAFTestStep(description = "user should see the {0}")
	public void userShouldSeeThe(String key) {

	}

	@QAFTestStep(description = "user enter required details to nominate for bright spark")
	public void userEnterRequiredDetailsForBrightSpark() {
		sendKeys(getRandomString(20), "txt.challenging.situations");
		sendKeys(getRandomString(20), "txt.solutions.provided");
		sendKeys(getRandomString(20), "txt.benefits.accured");
		sendKeys(getRandomString(20), "txt.rationale.nomination");
	}

	private void verifyBlocksinRequestPage() {
		NestUtils.scrollUpToElement(new QAFExtendedWebElement("request.details.label.nominee"));
		verifyVisible("request.details.label.nominee");
		verifyVisible("request.details.label.project");
		verifyVisible("request.details.label.reward.name");
		verifyVisible("request.details.label.posted.date");
		verifyVisible("request.details.label.nominated.by");
		verifyVisible("request.details.label.manager");
		verifyVisible("request.details.label.manager.status");
		verifyVisible("request.details.label.hr.status");
	}

	@QAFTestStep(description = "user selects from date {0} from calender")
	public void userSelectsFromDateFromCalender(String date) {
		CommonPage.waitForLocToBeClickable("date.to.rnrrequestpage");
		selectDate("date.to.rnrrequestpage", "btn.all.date.rnrrequestpage", date);
	}

	private void verifyStatusInBlocks(String fieldName, String values) {
		String[] fields = fieldName.split(" : ");
		String[] valueInFields = values.split(" : ");
		for (int i = 0; i < fields.length; i++) {
			Validator.verifyThat("Status values verified",
					getElement("request.details.value.by.field", fields[i].trim()).getText()
							.equalsIgnoreCase(valueInFields[i]),
					Matchers.equalTo(true));
		}
	}

	private void verifyBlockByRewardName(String reward) {
		Validator.verifyThat("reward is displayed!",
				getElement("request.details.value.by.field", "Reward Name").getText().equalsIgnoreCase(reward),
				Matchers.equalTo(true));
		verifyVisible("reward.card.image");
		verifyVisible("review.comments.field.managers.comments");
		verifyVisible("review.comments.field.hr.comments");

		if (reward.equalsIgnoreCase("Pat On Back")) {
			verifyVisible("value.addition.field.key.contribution");
		} else {
			verifyVisible("value.addition.field.challenging.stituation");
			verifyVisible("value.addition.field.solutions.provided");
			verifyVisible("value.addition.field.benefits.accured");
			verifyVisible("value.addition.field.rationale.for.nomination");
		}
		showOnlyBackbutton();
	}

	@QAFTestStep(description = "user selects to date {0} from calender")
	public void userSelectsToDateFromCalender(String date) {
		CommonPage.waitForLocToBeClickable("date.to.rnrrequestpage");
		selectDate("date.from.rnrrequestpage", "btn.all.date.rnrrequestpage", date);

	}

	@QAFTestStep(description = "user verifies request details page for reward {0} and fields {1} having values {2}")
	public void verifyRequestDetailPage(String reward, String fieldName, String values) {
		verifyBlocksinRequestPage();
		verifyStatusInBlocks(fieldName, values);
		verifyBlockByRewardName(reward);
	}

	public static void selectDate(String type, String loc, String date) {
		waitForNotVisible("image.loader.common");
		// waitForPageToLoad();
		click(type);
		QAFExtendedWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), date));
		element.click();
	}

	
	@QAFTestStep(description = "user verifies that R&R request page fields should be clear")
	public void userVerifiesThatRRRequestPageFieldsShouldBeClear() {
		List<QAFWebElement> options =
				NestUtils.getQAFWebElements("dropdown.select.fileds.rnrreportpage");
		for (QAFWebElement e : options) {
			String text = e.getText();
			if ((text).contains("Select")) {
				Reporter.log(text + " field has been cleared", MessageTypes.Pass);

			} else {
				Reporter.log(text + " field not cleared", MessageTypes.Fail);

			}
		}
		
		LocalDate today = LocalDate.now();
		String firstDay=today.withDayOfMonth(1).toString();
		String lastDay=today.withDayOfMonth(today.lengthOfMonth()).toString();
		String fromDate = CommonStep.getText("date.to.rnrrequestpage").toString();
		String toDate = CommonStep.getText("date.to.rnrrequestpage").toString();		
		ReportsPage.verifyDate(firstDay, fromDate);
		ReportsPage.verifyDate(lastDay, toDate);
	}
}
