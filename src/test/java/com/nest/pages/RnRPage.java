package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.verifyVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;

import com.nest.beans.BrightsparkBean;
import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class RnRPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	public QAFExtendedWebElement getElement(String loc, String text) {
		QAFExtendedWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), text));
		return element;

	}

	@QAFTestStep(description = "user click search button")
	public void clickSearch() {
		QAFWebElement searchButton = NestUtils.getDriver().findElement("search.button");
		NestUtils.scrollUpToElement(searchButton);
		waitForVisible("search.button");
		NestUtils.clickUsingJavaScript(searchButton);
		waitForNotPresent("loading.bar");
	}

	@QAFTestStep(description = "user verifies R&R Requests where {0} is {1}")
	public void RRRequestsPage(String filtername, String filterValue) {
		verifyTableColumnsForRRRequests();
		verifyFilter(filtername, filterValue);
	}

	private void verifyFilter(String filtername, String filterValue) {
		List<QAFWebElement> statusList =
				NestUtils.getQAFWebElements(String.format(ConfigurationManager.getBundle()
						.getString("request.list.table.column.values"), filtername));
		for (QAFWebElement status : statusList) {
			Validator.verifyThat("status is " + filterValue,
					status.getText().toString().trim(),
					Matchers.equalToIgnoringWhiteSpace(filterValue));
		}
	}

	public void verifyTableColumnsForRRRequests() {
		Validator.verifyThat("Nominator column is visible",
				getElement("table.column", "Nominee").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Reward Name column is visible",
				getElement("table.column", "Location").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Nominee column is visible",
				getElement("table.column", "Reward").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Department column is visible",
				getElement("table.column", "Posted Date").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Manager Status column is visible",
				getElement("table.column", "Manager Status").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("HR Status column is visible",
				getElement("table.column", "HR Status").isDisplayed(),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verifies R&R Request list for {0}")
	public void vereifyRRRequestsList(String userType) {
		List<QAFWebElement> requestList =
				NestUtils.getQAFWebElements("request.list.table");
		if (requestList.size() > 0)
			requestList.get(0).click();
		if (userType.equalsIgnoreCase("manager")) {
			verifyVisible(String.format(
					ConfigurationManager.getBundle().getString("button.name"),
					"Approve"));
		}
		verifyVisible(String.format(
				ConfigurationManager.getBundle().getString("button.name"), "Reject"));
		verifyVisible(String.format(
				ConfigurationManager.getBundle().getString("button.name"), "Back"));
	}

	@QAFTestStep(description = "user verify {page_name} of nominate module with all fileds")
	public void verifyNominatePages(String pageName) {
		verifyPresent("tab.cardselection.nominatepages");
		int totalCards = NestUtils.getDriver()
				.findElements("cards.cardselection.nominatepages").size();
		switch (pageName) {
			case "You Made My Day" :
				Validator.verifyThat("Select Card Block must have 5 cards.",
						totalCards == 5, Matchers.equalTo(true));
				verifyPresent("imput.comment.youmademydaypage");
				break;

			case "Pat On Back" :
				Validator.verifyThat("Select Card Block must have 3 cards.",
						totalCards == 3, Matchers.equalTo(true));

				Validator.verifyThat("Enter Contributions text area should be present",
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("imput.textarea"),
								"Enter Contributions")).isPresent(),
						Matchers.equalTo(true));

				Validator.verifyThat("Select Project dropdown should be present",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Select Project")).isPresent(),
						Matchers.equalTo(true));
				break;

			case "Bright Spark" :
				Validator.verifyThat("Select Card Block must have 3 cards.",
						totalCards == 3, Matchers.equalTo(true));
				Validator.verifyThat(
						"Enter challenging situations text area should be present",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("imput.textarea"),
								"Enter challenging situations")).isPresent(),
						Matchers.equalTo(true));
				Validator
						.verifyThat(
								"Enter solutions provided text area should be present",
								new QAFExtendedWebElement(String.format(
										ConfigurationManager.getBundle()
												.getString("imput.textarea"),
										"Enter solutions provided")).isPresent(),
								Matchers.equalTo(true));
				Validator
						.verifyThat("Enter benefits accrued text area should be present",
								new QAFExtendedWebElement(String.format(
										ConfigurationManager.getBundle()
												.getString("imput.textarea"),
										"Enter benefits accrued")).isPresent(),
								Matchers.equalTo(true));
				Validator.verifyThat("Enter Contributions text area should be present",
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("imput.textarea"),
								"Enter contributions")).isPresent(),
						Matchers.equalTo(true));

				Validator.verifyThat("Select Project dropdown should be present",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Select Project")).isPresent(),
						Matchers.equalTo(true));
				break;
		}

		Validator
				.verifyThat("Select groups dropdown should be present",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Select Group")).isPresent(),
						Matchers.equalTo(true));

		Validator.verifyThat("Select Band dropdown should be present",
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("button.avlfields.nominatepages"), "Select Band"))
								.isPresent(),
				Matchers.equalTo(true));

		Validator
				.verifyThat("Select Nominee dropdown should be present",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Select Nominee")).isPresent(),
						Matchers.equalTo(true));

		Validator.verifyThat("Post button should be present",
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("button.avlfields.nominatepages"), "Post"))
								.isPresent(),
				Matchers.equalTo(true));

		Validator.verifyThat("Preview button should be present",
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("button.avlfields.nominatepages"), "Preview"))
								.isPresent(),
				Matchers.equalTo(true));

		Validator.verifyThat("Cancel button should be present",
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("button.avlfields.nominatepages"), "Cancel"))
								.isPresent(),
				Matchers.equalTo(true));

	}

	@QAFTestStep(description = "user should see required filed with astric sign for {page_name} of nominate module")
	public void verifyAstersikSign(String pageName) {

		Validator.verifyThat("Select a Card field having astric sign",
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("button.avlfields.nominatepages"), "Select a Card"))
								.getText().contains("*"),
				Matchers.equalTo(true));

		Validator.verifyThat("Nominee field having astric sign",
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("button.avlfields.nominatepages"), "Nominee"))
								.getText().contains("*"),
				Matchers.equalTo(true));

		Validator.verifyThat("Message on Card field having astric sign",
				new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
						.getString("button.avlfields.nominatepages"), "Message on Card"))
								.getText().contains("*"),
				Matchers.equalTo(true));

		switch (pageName) {
			case "Pat On Back" :
				Validator
						.verifyThat("Project field having astric sign",
								new QAFExtendedWebElement(String.format(
										ConfigurationManager.getBundle().getString(
												"button.avlfields.nominatepages"),
										"Project")).getText().contains("*"),
								Matchers.equalTo(true));

				Validator.verifyThat(
						"Key Contribution by Nominee field having astric sign",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Key Contribution by Nominee")).getText().contains("*"),
						Matchers.equalTo(true));

				break;

			case "Bright Spark" :
				Validator
						.verifyThat("Project field having astric sign",
								new QAFExtendedWebElement(String.format(
										ConfigurationManager.getBundle().getString(
												"button.avlfields.nominatepages"),
										"Project")).getText().contains("*"),
								Matchers.equalTo(true));

				Validator.verifyThat(
						"Challenging situations faced field having astric sign",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Challenging situations faced")).getText().contains("*"),
						Matchers.equalTo(true));

				Validator.verifyThat(
						"Solutions provided/Situation handling field having astric sign",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Solutions provided/Situation handling")).getText()
										.contains("*"),
						Matchers.equalTo(true));

				Validator.verifyThat("Rationale for Nomination field having astric sign",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Rationale for Nomination")).getText().contains("*"),
						Matchers.equalTo(true));

				Validator.verifyThat("Benefits accrued field having astric sign",
						new QAFExtendedWebElement(String.format(
								ConfigurationManager.getBundle()
										.getString("button.avlfields.nominatepages"),
								"Benefits accrued")).getText().contains("*"),
						Matchers.equalTo(true));
				break;

		}

	}

	@QAFTestStep(description = "user select and verify selection of card should get highlighted")
	public void verifySelectedCards() {
		NestUtils.getDriver().findElements("cards.cardselection.nominatepages").get(0)
				.click();
		String selectedCardBorder =
				new QAFExtendedWebElement("select.selectedcard.nominatepages")
						.getCssValue("border-bottom-color");
		Validator.verifyThat("Selected card border should be orange",
				selectedCardBorder.equals(
						ConfigurationManager.getBundle().getString("orange.colorcode")),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user nominate pat on back for {0} {1} {2} {3}")
	public void nominatePatonBack(String group, String band, String nomineename,
			String project) {

		verifyPresent("select.card.r&r");
		click("select.card.r&r");
		verifyPresent("group.dropdown.r&r");
		NestUtils.clickUsingJavaScript("group.dropdown.r&r");
		sendKeys(group, "search.element.r&r");

		click("group.name.r&r");

		selectOptions("select.band.r&r", "band.list.r&r", band);

		verifyPresent("select.nominee.r&r");

		NestUtils.clickUsingJavaScript("select.nominee.r&r");

		sendKeys(nomineename, "search.element.r&r");
		String nomineeName = new QAFExtendedWebElement("nominee.name.r&r").getText();
		ConfigurationManager.getBundle().setProperty("NomineeName", nomineeName);
		NestUtils.clickUsingJavaScript("nominee.name.r&r");

		NestUtils.clickUsingJavaScript("select.project.r&r");
		sendKeys(project, "search.element.r&r");
		verifyPresent("project.name.r&r");
		click("project.name.r&r");

	}

	public void checkList() {

		Reporter.log("element list size ::" + returnList("nominee.list.r&r").size());

		List<QAFWebElement> list = returnList("nominee.list.r&r");

		for (int i = 0; i < list.size(); i++) {
			Reporter.log("list is ::" + list.get(i).getText().split("\\+ ")[1]);
			Reporter.log(ConfigurationManager.getBundle().getProperty("NomineeName")
					.toString());
			if (list.get(i).getText().split("\\+ ")[1]
					.equalsIgnoreCase(ConfigurationManager.getBundle()
							.getProperty("NomineeName").toString())) {

				list.get(i).click();

				break;

			}

		}

	}

	public List<QAFWebElement> returnList(String loc) {
		List<QAFWebElement> list = driver.findElements(loc);

		return list;

	}

	@QAFTestStep(description = "user verifies R&R nominee Request list for {0}")
	public void displayList(String loc) {

		CommonStep.waitForNotVisible("wait.loader");
		List<QAFWebElement> list = null;
		switch (loc) {
			case "Pat on Back" :
				list = returnList("list.patonback.r&r");

				Validator.verifyThat("nominee list is rendering...", list.size(),
						Matchers.greaterThan(0));

				for (int i = 0; i < list.size(); i++) {
					Reporter.log("list is ::" + list.get(i).getText());

				}
				break;
			case "Bright Spark" :
				list = returnList("list.brightspark.r&r");

				Validator.verifyThat("nominee list is rendering...", list.size(),
						Matchers.greaterThan(0));

				for (int i = 0; i < list.size(); i++) {
					Reporter.log("list is ::" + list.get(i).getText());

				}
				break;

		}

	}

	public static boolean verifyText(String loc, String text) {
		return new QAFExtendedWebElement(loc).getText().contains(text);
	}

	@QAFTestStep(description = "user click on reward {0}")
	public void checkReward(String loc) {
		CommonStep.waitForNotVisible("wait.loader");
		List<QAFWebElement> list = null;
		switch (loc) {
			case "Pat on Back" :
				list = returnList("list.rewards.r&r");

				for (int i = 0; i < list.size(); i++) {
					if (loc.equalsIgnoreCase(list.get(i).getText())) {
						list.get(i).click();
						CommonStep.waitForNotVisible("wait.loader");
						break;
					}
				}
				break;
			case "Bright Spark" :
				list = returnList("list.rewards.r&r");

				for (int i = 0; i < list.size(); i++) {
					if (loc.equalsIgnoreCase(list.get(i).getText())) {
						list.get(i).click();
						break;

					}
					break;
				}
		}
	}
	public void selectOptions(String loc, String Listloc, String value) {

		CommonPage.clickUsingJavaScript(loc);
		List<QAFWebElement> list =
				new QAFExtendedWebElement("xpath=/html").findElements(Listloc);
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getText().contains(value)) {
				list.get(i).click();
				break;
			}
		}
	}

	@QAFTestStep(description = "user logout from page")
	public void clicklogout() {
		verifyPresent("nominee.logout.r&r");
		NestUtils.clickUsingJavaScript("nominee.logout.r&r");

	}

	@QAFTestStep(description = "User select a card")
	public void userSelectACard() {
		verifyPresent("select.card.r&r");
		NestUtils.clickUsingJavaScript("select.card.r&r");

	}

	@QAFTestStep(description = "user text message on page")
	public void messageCard() {
		verifyPresent("message.cardsection.misc");
		CommonStep.sendKeys(
				"Congratulations! on winning Award for your outstanding performance in your project.",
				"message.cardsection.misc");
	}

	@QAFTestStep(description = "user text key contribution")
	public void keyContribution() {
		sendKeys("Good work..!!!", "key.contributiontextarea.r&r");
	}
	@QAFTestStep(description = "user click post button")
	public void clickPost() {
		QAFWebElement postButton = NestUtils.getDriver().findElement("btn.post.r&r");
		NestUtils.scrollUpToElement(postButton);
		waitForVisible("btn.post.r&r");
		NestUtils.clickUsingJavaScript(postButton);
	}

	@QAFTestStep(description = "user check nominee list")
	public void nomineeList() {

		checkList();

		NestUtils.clickUsingJavaScript("nomineelist.patonback.r&r");

		Reporter.logWithScreenShot("nominee list is display");

	}
	@QAFTestStep(description = "user verify manager name")
	public void manegerName() {

		waitForVisible("nominee.managername.r&r");
		String managerName =
				((JavascriptExecutor) driver)
						.executeScript("return arguments[0].innerText;",
								new QAFExtendedWebElement("nominee.managername.r&r"))
						.toString();
		// String managerName =
		// new QAFExtendedWebElement("nominee.managername.r&r").getText();
		Reporter.log("maneger name ::: " + managerName);

	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	@QAFTestStep(description = "user should see the fields")
	public void userShouldSeeTheFields() {
		CommonStep.waitForEnabled("cell.Rewards.MyRewardPageListPage.rnr");
		verifyPresent("cell.Rewards.MyRewardPageListPage.rnr");
		verifyPresent("cell.PublishedDate.MyRewardPageListPage.rnr");
		verifyPresent("cell.NominatedBy.MyRewardPageListPage.rnr");
	}

	@QAFTestStep(description = "user nominate bright spark for {0} {1} {2} {3}")
	public void nominateBrightSpark(String group, String band, String nomineename,
			String project) {

		verifyPresent("select.card.r&r");
		click("select.card.r&r");
		verifyPresent("group.dropdown.r&r");
		NestUtils.clickUsingJavaScript("group.dropdown.r&r");
		sendKeys(group, "search.element.r&r");

		NestUtils.clickUsingJavaScript("group.name.r&r");

		selectOptions("select.band.r&r", "band.list.r&r", band);

		verifyPresent("select.nominee.r&r");

		NestUtils.clickUsingJavaScript("select.nominee.r&r");

		sendKeys(nomineename, "search.element.r&r");
		String nomineeName = new QAFExtendedWebElement("nominee.name.r&r").getText();
		ConfigurationManager.getBundle().setProperty("NomineeName", nomineeName);
		NestUtils.clickUsingJavaScript("nominee.name.r&r");

		NestUtils.clickUsingJavaScript("select.project.r&r");
		sendKeys(project, "search.element.r&r");
		verifyPresent("project.name.r&r");
		click("project.name.r&r");

	}
	@QAFTestStep(description = "user fill {0} required details for bright spark")
	public void fillRequiredDetailsBrightSpark(String key) {

		BrightsparkBean detail = new BrightsparkBean();
		detail.fillFromConfig(key);
		CommonPage.clickUsingJavaScript("challenging.situations.r&r");
		sendKeys(detail.getChallengingSituations(), "challenging.situations.r&r");
		CommonPage.clickUsingJavaScript("solutions.provided.r&r");
		sendKeys(detail.getSolutionsProvided(), "solutions.provided.r&r");
		CommonPage.clickUsingJavaScript("benefits.accrued.r&r");
		sendKeys(detail.getBenefitsAccrued(), "benefits.accrued.r&r");
		CommonPage.clickUsingJavaScript("rationale.Nomination.r&r");
		sendKeys(detail.getRationaleForNomination(), "rationale.Nomination.r&r");
	}
	@QAFTestStep(description = "user nominate You Made My Day for {0} {1} {2}")
	public void nominateYouMadeMyDay(String group, String band, String nomineename) {

		verifyPresent("select.card.r&r");
		click("select.card.r&r");
		verifyPresent("group.dropdown.r&r");
		NestUtils.clickUsingJavaScript("group.dropdown.r&r");
		sendKeys(group, "search.element.r&r");

		click("group.name.r&r");

		selectOptions("select.band.r&r", "band.list.r&r", band);

		verifyPresent("select.nominee.r&r");

		NestUtils.clickUsingJavaScript("select.nominee.r&r");

		sendKeys(nomineename, "search.element.r&r");
		String nomineeName = new QAFExtendedWebElement("nominee.name.r&r").getText();
		ConfigurationManager.getBundle().setProperty("NomineeName", nomineeName);
		NestUtils.clickUsingJavaScript("nominee.name.r&r");
	}

	@QAFTestStep(description = "user click on nominee reward list for {0} {1} {2}")
	public void clickOnPatOnBack(String nomineeName, String status, String reward) {

		CommonStep.waitForNotVisible("wait.loader");
		NestUtils.clickUsingJavaScript("list.status.rnr", nomineeName, status, reward);
		CommonStep.waitForNotVisible("wait.loader");
		Reporter.logWithScreenShot("nominee list and reward is display");

	}

	@QAFTestStep(description = "user click on {0} button")
	public void button(String buttonName) {

		CommonStep.waitForNotVisible("wait.loader");

		JavascriptExecutor executor =
				(JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("window.scrollBy(0,1200)", "");

		QAFExtendedWebElement element = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("btn.all"), buttonName));

		NestUtils.clickUsingJavaScript(element);
		Reporter.logWithScreenShot("button is display");

	}

	@QAFTestStep(description = "user text approval comment and submit")
	public void submitApproval() {
		CommonStep.waitForNotVisible("wait.loader");
		verifyPresent("approval.comment.rnr");
		verifyVisible("approval.comment.rnr");
		NestUtils.clickUsingJavaScript("approval.comment.rnr");
		sendKeys("good work", "approval.comment.rnr");

		NestUtils.clickUsingJavaScript("submit.btn.rnr");

	}

	@QAFTestStep(description = "user verifies {0} message")
	public void verifyMessage(String message) {
		waitForNotVisible("wait.loader");
		NestUtils.verifyMessagePresent("label.status.common", message);

	}

	@QAFTestStep(description = "user verifies {0} Button")
	public void verifyButtons(String btn) {
		try {
			QAFExtendedWebElement element = new QAFExtendedWebElement(String
					.format(ConfigurationManager.getBundle().getString("btn.all"), btn));
			element.waitForVisible(5000);
			if (element.verifyPresent()) {

				Reporter.log("Button is present");
			}

		} catch (Exception e) {
			Reporter.log("Button is not present");

		}
	}

	@QAFTestStep(description = "user should see plus sign on screen")
	public void userShouldSeePlusSignOnScreen() {
		verifyPresent("symbol.MyRewardListPage.rnr");
	}

	@QAFTestStep(description = "user should see {0} for selected Rewards")
	public void userShouldSeeForSelectedRewards(String str0) {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		CommonStep.waitForEnabled("common.breadcrumb");
		String[] fieldList = str0.split(",");
		Reporter.log("String received" + Arrays.toString((fieldList)));

		for (int i = 0; i < fieldList.length; i++) {
			Validator
					.verifyThat(
							new QAFExtendedWebElement(String.format(
									ConfigurationManager.getBundle()
											.getString("list.buttonsOnRewardPage.rnr"),
									fieldList[i])).isPresent(),
							Matchers.equalTo(true));
		}

	}

	@QAFTestStep(description = "user should see {0} as {1}")
	public void userShouldSeeAs(String userRole, String status) {
		waitForNotVisible("wait.loader");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		QAFExtendedWebElement role = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("Label.status.Approved.rnr"),
				userRole));

		if (role.getText().equalsIgnoreCase("Manager Status")) {
			Validator
					.verifyThat(
							new QAFExtendedWebElement(
									String.format(
											ConfigurationManager.getBundle().getString(
													"Label.status.Approved.rnr"),
											userRole)).getText().contains(status),
							Matchers.equalTo(true));
			Reporter.logWithScreenShot(userRole + "is" + status);

		} else if (role.getText().equalsIgnoreCase("HR Status")) {
			Validator
					.verifyThat(
							new QAFExtendedWebElement(
									String.format(
											ConfigurationManager.getBundle().getString(
													"Label.status.Approved.rnr"),
											userRole)).getText().contains(status),
							Matchers.equalTo(true));
		}
	}
}
