package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.NotYetImplementedException;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class MISCPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	PublisherPage publisherPage = new PublisherPage();

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	@QAFTestStep(description = "user should see Birthday Calendar page")
	public void verifyBirthdayCalendarPage() {
		waitForNotVisible("wait.loader");
		verifyPresent("common.breadcrumb");

	}

	@QAFTestStep(description = "user should see Employment Anniversary page")
	public void verifyEmploymentAnniversaryPage() {
		waitForNotVisible("wait.loader");
		verifyPresent("common.breadcrumb");
	}
	@QAFTestStep(description = "user select any employee name")
	public void userSelectAnyEmployeeName() {

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		waitForNotVisible("wait.loader");
		CommonStep.waitForEnabled("list.EmpHavingbirthday.birthdaycalenderpage");

		List<QAFWebElement> empName = NestUtils
				.getQAFWebElements("list.EmpHavingbirthday.birthdaycalenderpage");
		if (empName.isEmpty()) {
			Reporter.log("No employee is having birthday in current month");
		}
		try {
			empName.get(2).click();
			CommonStep.waitForEnabled("common.breadcrumb");
			Reporter.log("Clicked successfully");
			waitForPageToLoad();
		} catch (Exception ex) {
			Reporter.log("failed to click an emp");
			Reporter.log(ex.getMessage(), MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user should see the breadcrumbs")
	public void userShouldSeeTheBreadcrumbs() {

		String[] expected = {"Home", "MISC", "Birthday Calender"};
		for (int i = 0; i <= expected.length; i++) {
			QAFExtendedWebElement breadcrumb = new QAFExtendedWebElement(
					String.format(ConfigurationManager.getBundle()
							.getString("list.breadcrum.userhomepage"), expected[i]));
			if (breadcrumb.equals(expected[i])) {
				Reporter.log("breadcrumbs Verified");
			}
		}
	}

	@QAFTestStep(description = "user should see required fields {0} and {1} with asterisk sign")
	public void userShouldSeeRequiredFieldsAndWithAsteriskSign(String str0, String str1) {
		verifyPresent("symbol.mandatoryfield1.birthdayCalendar");
		verifyPresent("symbol.mandatoryfield2.birthdayCalendar");
	}

	@QAFTestStep(description = "user should verify Breadcrumb {0}")
	public void userShouldVerifyBreadcrumb(String breadcrumb) {
		publisherPage.verifyBreadcrumbs("breadcrumb.mypost.publisher", breadcrumb);
	}

	@QAFTestStep(description = "User select a card")
	public void userSelectACard() {
		waitForPageToLoad();
		CommonStep.waitForNotVisible("loader.userhomepage");
		NestUtils.scrollUpToElement(NestUtils.getDriver()
				.findElement("common.breadcrumb"));

		try {

			NestUtils.waitForElementToBeClickable(
					NestUtils.getDriver().findElement("list.card.misc"));
			("list.card.misc").wait(60000);
		} catch (Exception e) {
			Reporter.log("card is not loaded" + e.getMessage());
		}

		List<QAFWebElement> cards = NestUtils.getQAFWebElements("list.card.misc");
		NestUtils.waitForElementToBeClickable(
				NestUtils.getDriver().findElement("list.card.misc"));

		cards.get(2).click();
	}
	@QAFTestStep(description = "card should be highlighted")
	public void cardShouldBeHighlighted() {
		verifyPresent("img.highlightedcard.misc");
		Reporter.log("Seleted card is highlighted");

	}

	@QAFTestStep(description = "user should see {0} cards and {1} drop downs and message box")
	public void userShouldSeeCards(long numberOfCards) {
		NestUtils.scrollUpToElement(NestUtils.getDriver()
				.findElement("common.breadcrumb"));
		numberOfCards = 5;
		List<QAFWebElement> cards = NestUtils.getQAFWebElements("list.card.misc");
		if (cards.size() == 5) {
			Reporter.log("Number of cards are 5");
		}
		CommonStep.waitForVisible("textArea.CommentSection.misc");
		if (verifyPresent("textArea.CommentSection.misc")) {
			Reporter.log("Message box is present");
		}
		if (verifyPresent("button.Post.birthdayCard.misc")) {
			Reporter.log("Post button is Present");
		}
		if (verifyPresent("button.Preview.birthdayCard.misc")) {
			Reporter.log("Preview button is Present");
		}
		if (verifyPresent("button.Cancel.birthdayCard.misc")) {
			Reporter.log("Cancel button is Present");
		}

	}

	@QAFTestStep(description = "user write Message and click on preview")
	public void userWriteMessageAndClickOnPreview() {
		waitForNotVisible("wait.loader");
		CommonStep.sendKeys("\n" + "Enjoy!!!", "textArea.CommentSection.misc");
		waitForPageToLoad();
		NestUtils.scrollUpToElement(
				NestUtils.getDriver().findElement("button.Cancel.birthdayCard.misc"));
		CommonStep.click("button.Preview.birthdayCard.misc");
	}

	@QAFTestStep(description = "user should see preview of card")
	public void userShouldSeePreviewOfCard() {
		CommonStep.waitForEnabled("img.cross.birthdayCard.misc");
		verifyPresent("img.cross.birthdayCard.misc");
		verifyPresent("label.Preview.birthdayCard.misc");

		Reporter.log("Preview can be seen");
	}
	@QAFTestStep(description = "user text messagebox on Employment Anniversary page")
	public void messagebox() {

		waitForPageToLoad();
		verifyPresent("message.cardsection.misc");
		CommonStep.sendKeys(
				"Wish you a Happy Work Anniversary! Congratulations on completion of your 7th year at Infostretch.",
				"message.cardsection.misc");
	}
	@QAFTestStep(description = "user click post button on Employment Anniversary page")
	public void clickPostbutton() {

		verifyPresent("post.button.misc");
		click("post.button.misc");
	}
	@QAFTestStep(description = "user select card on birthday calender page")
	public void selectCards() {
		waitForPageToLoad();

		verifyPresent("select.birthdaycard.misc");
		CommonPage.clickUsingJavaScript("select.birthdaycard.misc");

	}
	@QAFTestStep(description = "user text messagebox on birthday calender page")
	public void textmessagebox() {

		waitForVisible("message.cardsection.misc");
		verifyPresent("message.cardsection.misc");
		CommonStep.sendKeys("Wish you a very Happy Birthday!",
				"message.cardsection.misc");
	}
	@QAFTestStep(description = "user click post button on birthday calender page")
	public void selectPost() {

		QAFWebElement postButton = NestUtils.getDriver().findElement("btn.post.r&r");
		NestUtils.scrollUpToElement(postButton);
		waitForVisible("btn.post.r&r");
		NestUtils.clickUsingJavaScript(postButton);

	}
	@QAFTestStep(description = "user select employee name on Employment Anniversary page")
	public void verifyEmployeenamePage() {

		NestUtils.getQAFWebElements("list.EmpAnniversary.misc");
		List<QAFWebElement> empName =
				NestUtils.getQAFWebElements("list.EmpAnniversary.misc");

		empName.get(1).click();
	}
	@QAFTestStep(description = "user select card on Employment Anniversary page")
	public void card() {
		verifyPresent("select.Aniversarycard.misc");
		CommonPage.clickUsingJavaScript("select.Aniversarycard.misc");

	}

	@QAFTestStep(description = "user should verify {0} Page")
	public void userShouldVerifyPage(String header) {
		waitForNotVisible("wait.loader");
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		String s = CommonStep.getText("common.breadcrumb").trim();
		if (s.equalsIgnoreCase(header)) {
			Reporter.logWithScreenShot("User is on Happy birthday Page");
		} else Reporter.logWithScreenShot("Failed to reach the page");

	}
}
