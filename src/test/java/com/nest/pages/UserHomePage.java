package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.verifyEnabled;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.verifyText;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.NotYetImplementedException;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class UserHomePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	NestUtils nestUtils = new NestUtils();
	LeavePage leave = new LeavePage();

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

	@QAFTestStep(description = "user should see home page")
	public void verifyHomePage() {
		verifyPresent("button.logout.userhomepage");
		verifyPresent("icon.expand.leftmenu.userhomepage");
	}

	@QAFTestStep(description = "user click on View Training Calendar link")
	public void clickOnViewTrainingCalendarLink() throws AWTException {
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_PAGE_DOWN);
		robot.keyRelease(KeyEvent.VK_PAGE_DOWN);
		waitForPageToLoad();
		waitForAjaxToComplete();
		CommonStep.waitForNotVisible("loader.userhomepage");
		CommonStep.waitForEnabled("link.viewTrainingCalender.userHomepage", 600000);
		click("link.viewTrainingCalender.userHomepage");
		waitForPageToLoad();
	}

	@QAFTestStep(description = "user should see trainig calender")
	public void verifyTrainigCalenderPage() {
		waitForPageToLoad();
		CommonStep.waitForVisible("title.TrainingCalendar.userhomepage");
		CommonStep.assertVisible("title.TrainingCalendar.userhomepage");
	}

	@QAFTestStep(description = "user open any training listed in calendar of home page")
	public String openAnyTraining() {
		String nearesttraining = null;
		try {
			List<QAFWebElement> avltrainings =
					NestUtils.getDriver().findElements("list.trainings.userhome");
			nearesttraining = avltrainings.get(0).getText();
			NestUtils.clickUsingJavaScript(avltrainings.get(0));
		} catch (Exception e) {
			Reporter.log("No training listed in available screen");
			e.printStackTrace();
		}
		return nearesttraining;
	}

	@QAFTestStep(description = "user should see the list of leave requests")
	public void shouldSeeLeaveRequest() {
		leave.verifyLeaveRequests();
		Reporter.logWithScreenShot("User is on leave Request Page");
	}

	@QAFTestStep(description = "user click on view all link for Expense requests")
	public void iClickOnViewAllLinkForExpenseRequests() {
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		CommonStep.waitForNotVisible("loader.userhomepage");
		CommonStep.waitForEnabled("link.ViewAll.Requests.userhomepage");
		leave.expenseRequests().click();
		CommonPage.clickUsingJavaScript("link.ViewAll.Requests.userhomepage");
		Reporter.logWithScreenShot("User is on Expense Request Page");
	}

	@QAFTestStep(description = "user should see the list of expense request")
	public void iShouldSeeTheListOfExpenseRequest() {
		leave.verifyExpenseRequests();
		Reporter.logWithScreenShot("User is on expense Request Page");
	}

	@QAFTestStep(description = "user click on view all Travel requests")
	public void iClickOnViewAllTravelRequests() {
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		CommonStep.waitForNotVisible("loader.userhomepage");
		CommonStep.waitForEnabled("link.ViewAll.Requests.userhomepage");
		leave.travelRequests().click();
		waitForPageToLoad();
		CommonPage.clickUsingJavaScript("link.ViewAll.Requests.userhomepage");
		Reporter.logWithScreenShot("User is on Travel Request Page");
	}

	@QAFTestStep(description = "user should see the list of Travel requests")
	public void iShouldSeeTheListOfTravelRequests() {
		leave.verifyTravelRequests();
	}

	@QAFTestStep(description = "user should see the Holidays List section with dates")
	public void verifyHolidaysListSectionWithDates() {
		leave.verifySection();
	}

	@QAFTestStep(description = "user Click on a Leave Request row")
	public void ClickOnLeaveRequestRow() {

		waitForNotVisible("wait.loader");
		verifyPresent("row.leave.request.details.userhomepage");
		WebElement element = driver.findElement("row.leave.request.details.userhomepage");
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);

		waitForNotVisible("wait.loader");
	}

	public List<QAFWebElement> selectTabs() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("tab.requests.heading.userhomepage");
	}

	@QAFTestStep(description = "user click on a travel Request row")
	public void clickOnTravelRequestRow() {

		WebElement element =
				driver.findElement("link.row.travel.request.details.userhomepage");
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
		waitForNotVisible("wait.loader");

	}

	public int CalculateDiffBetwnDates(String startDateLoc, String endDateLoc) {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		final String firstInput = new QAFExtendedWebElement(startDateLoc).getText();
		final String secondInput = new QAFExtendedWebElement(endDateLoc).getText();
		final LocalDate firstDate = LocalDate.parse(firstInput, formatter);
		final LocalDate secondDate = LocalDate.parse(secondInput, formatter);
		final int days = (int) ChronoUnit.DAYS.between(firstDate, secondDate);
		return days;

	}

	@QAFTestStep(description = "user observe {0} section on HomePage")
	public void observeTrainingCalendarSection(String sectionName) {
		waitForNotVisible("wait.loader");
		CommonPage.scrollUpToElement("section.calender.userhomepage");
		boolean flag = CommonPage.verifyLocContainsText("section.calender.userhomepage",
				sectionName);
		if (flag) {
			Reporter.logWithScreenShot("calender section display in HomePage",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("calender section is not display in HomePage",
					MessageTypes.Info);
		}
	}

	public List<QAFWebElement> calenderDays() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("days.calender.userhomepage");
	}

	@QAFTestStep(description = "user verify Training Calendar should be displayed with one week range")
	public void verifyTrainingCalendarDisplayedOneWeekRange() {

		int diffDates = CalculateDiffBetwnDates("date.start.range.calender.homepage",
				"date.end.range.calender.homepage");
		int daysCountCalender = calenderDays().size() - 2;
		if (diffDates == daysCountCalender) {
			Reporter.logWithScreenShot(
					"Training Calendar should be displayed with one week range",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot(
					"Training Calendar should not be displayed with one week range",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verify There should be option to navigate to next week")
	public void verifyOptionToNavigateNext_week() {

		if (verifyEnabled("btn.next.week.calender.homepage")
				&& verifyPresent("btn.next.week.calender.homepage")) {
			Reporter.logWithScreenShot("There should be option to navigate to next week",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot(
					"There should not be option to navigate to next week",
					MessageTypes.Fail);
		}
	}

	public List<QAFWebElement> getRequestsTabs() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("list.requests.tabs.userhomepage");
	}

	@QAFTestStep(description = "user click on {0} request view all")
	public void clickOnViewAll(String requestType) {
		selectRequest(requestType);
		click("button.viewall.userhomepage");
	}

	@QAFTestStep(description = "user verify the view {0}")
	public void verifyView(String viewName) {
		verifyText("label.view.userhomepage", viewName);
		// verifyPresent("list.events.tabs.userhomepage");
	}

	@QAFTestStep(description = "user click on {0} event view all")
	public void clickOnEventViewAll(String eventType) {
		selectEvent(eventType);
		click("button.viewall.event.userhomepage");
	}

	@QAFTestStep(description = "user navigate back to previous page")
	public void navigateBack() {
		driver.navigate().back();
	}

	@QAFTestStep(description = "user verify {0} requests are displayed on screen")
	public void verifyRequests(String requests) {
		String[] arrRequests = requests.split(",");
		for (String request : arrRequests) {
			boolean status = false;
			for (QAFWebElement tab : getRequestsTabs()) {
				if (tab.getText().trim().contains(request.trim())) {
					Reporter.log(request + " request tab is found.", MessageTypes.Pass);
					status = true;
					break;
				}
			}
			if (status == false)
				Reporter.log(request + " request tab is not found.", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verify {0} request is selected by default")
	public void verifyActiveRequest(String request) {
		boolean status = false;
		for (QAFWebElement tab : getRequestsTabs()) {
			if (tab.getText().trim().contains(request)) {
				Reporter.log(request + " request tab is found.", MessageTypes.Pass);
				if (tab.getAttribute("class").contains("active"))
					Reporter.log(request + " request tab is selected by default.",
							MessageTypes.Pass);
				status = true;
				break;
			}
		}
		if (status == false)
			Reporter.log(request + " request tab is not found.", MessageTypes.Fail);

	}

	@QAFTestStep(description = "user verify holidays list section")
	public void verifyHolidaysList() {
		verifyPresent("link.holidays.userhomepage");
		verifyPresent("module.holidayslist.userhomepage");
		verifyPresent("title.holidays.userhomepage");
	}

	@QAFTestStep(description = "user perform action on view holidays")
	public void click_On_Viewholidays() {
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		CommonStep.waitForVisible("link.holidays.userhomepage");
		click("link.holidays.userhomepage");
	}

	@QAFTestStep(description = "user navigate to holiday module")
	public void verify_Navigation_To_HolidayModule() {
		waitForNotVisible("wait.loader");
		String holiday = driver.getCurrentUrl();
		if (holiday.contains("holiday")) {
			Reporter.log("holiday module page is open", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("holiday module page is not open",
					MessageTypes.Fail);;
		}
	}

	@QAFTestStep(description = "user navigate to travel request details page")
	public void verify_Navigation_To_TravelDtls() {
		waitForNotVisible("wait.loader");
		String travel = driver.getCurrentUrl();
		if (travel.contains("mytravelrequests")) {
			Reporter.log("travel request details page is open", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("travel request details page is not open",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verify leave balance section")
	public void verifyLeaveBalanceType() {
		verifyPresent("section.leave.userhomepage");
		QAFExtendedWebElement element = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("type.leave.userhomepage"),
				"PTO"));
		element.verifyPresent();
		element = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("type.leave.userhomepage"),
				"Comp Off"));
		element.verifyPresent();
		element = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("type.leave.userhomepage"),
				"Floating"));
		element.verifyPresent();
	}

	@QAFTestStep(description = "user click on View All link in Confirmation Review section of Manager view.")
	public void clickOnVeiwAllConfirmationReview() {

		waitForNotVisible("wait.loader");

		CommonPage.clickUsingJavaScript("link.viewAll.userhomepage");

	}

	@QAFTestStep(description = "user click on left arrow on training calender")
	public void clickOnLeftArrow() {

		waitForNotVisible("wait.loader");
		waitForVisible("icon.leftarrow.training.calender.userhomepage");
		QAFWebElement element = new QAFExtendedWebElement(
				"icon.leftarrow.training.calender.userhomepage");
		NestUtils.clickUsingJavaScript(element);
	}

	@QAFTestStep(description = "user click on right arrow on training calender")
	public void clickOnRightArrow() {
		waitForNotVisible("wait.loader");
		waitForVisible("icon.rightarrow.training.calender.userhomepage");
		QAFWebElement element = new QAFExtendedWebElement(
				"icon.rightarrow.training.calender.userhomepage");
		NestUtils.clickUsingJavaScript(element);
	}

	@QAFTestStep(description = "user should see last week calender")
	public void getlastWeekCalender() {
		verifyPresent("text.training.calender.userhomepage");
	}

	@QAFTestStep(description = "user should see next week calender")
	public void getNextWeekCalender() {
		verifyPresent("text.training.calender.userhomepage");
	}

	@QAFTestStep(description = "user Click on a Expense Request tab")
	public void ClickOnExpenseRequestRow() {

		waitForNotVisible("wait.loader");
		verifyPresent("tab.expense.requests.userhomepage");
		WebElement element = driver.findElement("tab.expense.requests.userhomepage");
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
		waitForNotVisible("wait.loader");
	}

	@QAFTestStep(description = "user should be navigated to Expense details page")
	public void verifyNavigationToExpensesDetailsPage() {
		waitForNotVisible("wait.loader");
		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.contains("myexpense")) {
			Reporter.logWithScreenShot("user navigated to expense details page",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("user not navigated to expense details page",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user should see Employee confirmation review page.")
	public void verifyEmployeeConfirmationReviewPage() {
		String confirmationFeedbackHeading =
				driver.findElement("heading.confirmationfeedback.userhomepage").getText();
		if (confirmationFeedbackHeading.contains("confirmation feedback Review"))
			Reporter.logWithScreenShot("Confirmation Review Page is displayed.",
					MessageTypes.Pass);

		else
			Reporter.logWithScreenShot("Confirmation Review Page is not displayed.",
					MessageTypes.Fail);
	}

	@QAFTestStep(description = "user click on Apply leave button")
	public void clickOnApplyLeaveButton() {
		waitForNotVisible("wait.loader");
		click("button.apply.leave.userhomepage");
	}

	@QAFTestStep(description = "user clicked on Manager View slide bar")
	public void clickedOnManagerViewSlideBar() {
		waitForPageToLoad();
		click("slider.userhomepage");

	}

	@QAFTestStep(description = "user should see Manager View")
	public void seeManagerView() {
		verifyPresent("text.manager.view.userhomepage");

	}

	@QAFTestStep(description = "user should see Planned leaves for this month section")
	public void seePlannedLeavesForThisMonthSection() {
		verifyPresent("number.of.days.planned.leaves.userhomepage");
		verifyPresent("text.employee.name.planeed.leaves.list.userhomepage");
		verifyPresent("leave.range.planned.leaves.userhomepage");
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,500)");
		CommonPage.scrollUpToElement("button.plannedleaves.view.all.userhomepage");
		verifyPresent("button.plannedleaves.view.all.userhomepage");

	}

	@QAFTestStep(description = "user click on view all button of {0}")
	public void clickOnViewAl(String requestType) {
		waitForNotVisible("image.loader.common");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		if (requestType.toLowerCase().contains("manager view")) {
			viewAll(requestType.split(" : ")[0].trim(),
					requestType.split(" : ")[1].trim());
		} else viewAll(null, requestType.trim());
	}

	private void viewAll(String viewType, String requestType) {
		if (viewType == null) {
			if (requestType.contentEquals(TabNames.LEAVE_REQUEST.getTab())
					|| requestType.contentEquals(TabNames.EXPENSE_REQUEST.getTab())
					|| requestType.contentEquals(TabNames.TRAVEL_REQUEST.getTab())) {
				selectRequest(requestType);
				waitForPageToLoad();
				waitForNotVisible("image.loader.common");
				CommonPage.waitForLocToBeClickable("button.viewall.userhomepage");
				click("button.viewall.userhomepage");
			} else if (requestType.contentEquals(TabNames.TRAINING_CALENDAR.getTab())) {
				NestUtils.scrollUpToElement(new QAFExtendedWebElement(
						"link.viewTrainingCalender.userHomepage"));
				click("link.viewTrainingCalender.userHomepage");
			} else if (requestType.contentEquals(TabNames.HOLIDAYS_LIST.getTab())) {
				click("link.holidays.userhomepage");
			} else Reporter.log(requestType + " is not found.", MessageTypes.Fail);
		} else if (viewType != null) {
			if (requestType.contentEquals(TabNames.LEAVE_REQUEST.getTab())
					|| requestType.contentEquals(TabNames.EXPENSE_REQUEST.getTab())
					|| requestType.contentEquals(TabNames.TRAVEL_REQUEST.getTab())
					|| requestType.contentEquals(TabNames.OTHER_REQUEST.getTab())) {
				selectRequestManager(requestType);
				click("link.ViewAll.Requests.userhomepage");
			} else if (requestType.contentEquals(TabNames.CONFIRMATION_REVIEW.getTab())) {
				click("link.viewAll.userhomepage");
			} else if (requestType.contentEquals(TabNames.UPCOMING_BIRTHDAYS.getTab())
					|| requestType
							.contentEquals(TabNames.UPCOMING_ANNIVERSARIES.getTab())) {
				selectEvent(requestType);
				CommonPage.scrollUpToElement("button.viewall.event.userhomepage");
				click("button.viewall.event.userhomepage");
			} else Reporter.log(requestType + " is not found.", MessageTypes.Fail);
		}

	}

	private enum TabNames {
		TRAINING_CALENDAR("Training Calendar"),
		LEAVE_REQUEST("Leave Request"),
		EXPENSE_REQUEST("Expense Request"),
		TRAVEL_REQUEST("Travel Request"),
		HOLIDAYS_LIST("Holiday Request"),
		OTHER_REQUEST("Other Request"),
		CONFIRMATION_REVIEW("Confirmation Review"),
		UPCOMING_BIRTHDAYS("Upcoming Birthdays"),
		UPCOMING_ANNIVERSARIES("Upcoming Anniversaries"),
		PLANNED_LEAVES("Planned Leaves");
		private final String tab;
		private TabNames(String tabName) {
			tab = tabName;
		}
		public String getTab() {
			return tab;
		}
	}

	public void selectRequest(String requestType) {
		waitForNotVisible("wait.loader");
		for (QAFWebElement tab : getRequestsTabs("list.requests.tabs.userhomepage")) {
			if (tab.getText().toLowerCase()
					.contains(requestType.split(" ")[0].toLowerCase())) {
				tab.click();
				waitForNotVisible("image.loader.common");
				return;
			} else continue;
		}
		Reporter.log(requestType + " tab is not found", MessageTypes.Fail);
	}

	public void selectRequestManager(String requestType) {
		waitForNotVisible("image.loader.common");
		for (QAFWebElement tab : getRequestsTabs(
				"list.requests.tabs.manager.userhomepage")) {
			if (tab.getText().toLowerCase().contains(requestType.toLowerCase())) {
				tab.click();
				waitForNotVisible("wait.loader");
				return;
			} else continue;
		}
		Reporter.log(requestType + " tab is not found", MessageTypes.Fail);
	}

	public void selectEvent(String eventType) {
		waitForNotVisible("wait.loader");
		for (QAFWebElement tab : getUpcomingEventTabs()) {
			if (tab.getText().toLowerCase().contains(eventType.toLowerCase())) {
				NestUtils.scrollUpToElement(tab);
				((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 150);");
				tab.click();
				waitForNotVisible("wait.loader");
				break;
			} else continue;
		}
	}

	public List<QAFWebElement> getUpcomingEventTabs() {
		Reporter.log("Finding elements");
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("list.events.tabs.userhomepage");
	}

	public List<QAFWebElement> getRequestsTabs(String loc) {
		return new QAFExtendedWebElement("xpath=/html").findElements(loc);
	}

	@QAFTestStep(description = "user click on {0} Request Tab")
	public void selectRequestTabs(String reqTabs) {

		for (int i = 0; i < selectTabs().size(); i++) {
			String actValue = selectTabs().get(i).getText();
			if (actValue.contains(reqTabs)) {
				selectTabs().get(i).click();
				waitForNotVisible("wait.loader");
				break;
			}

		}
		waitForNotVisible("wait.loader");
	}
	public List<QAFWebElement> getExpenseRequesticons() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("list.expenserow.viewdetails.userhomepage");
	}
	@QAFTestStep(description = "user click on expense Request row icon")
	public void clickOnExpenseRowIcon() {
		waitForNotVisible("wait.loader");
		Actions action = new Actions(driver);
		action.moveToElement(getExpenseRequesticons().get(0));
		action.perform();

		getExpenseRequesticons().get(0).click();
	}

	@QAFTestStep(description = "user verify navigation to Travel details page")
	public void verifyTravelDetailsPage() {

		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.contains("travelrequestdetails")) {
			Reporter.logWithScreenShot("user navigate to Travel details page",
					MessageTypes.Pass);
		}

		else {
			Reporter.logWithScreenShot("user NOT navigate to Travel details page",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verify navigation to Leave details page")
	public void verifyNavigationToLeaveDetailsPage() {

		waitForNotVisible("wait.loader");
		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.contains("myleavelist")) {
			Reporter.logWithScreenShot("User navigate to leave details page",
					MessageTypes.Pass);
		}

		else {
			Reporter.logWithScreenShot("User NOT navigate to leave details page",
					MessageTypes.Fail);
		}
	}
	@QAFTestStep(description = "user should see the floating holidays List section")
	public void verifyFloatingHolidaysListSection() {
		verifyPresent("section.floatingHolidays.userhomepage");
	}

	@QAFTestStep(description = "user verifies the breadcrumb on tranning page {0}")
	public void verifyBreadcrumbtranning(String title) {
		waitForNotVisible("wait.loader");
		String breadcrumb =
				new QAFExtendedWebElement("txt.tranning.tranningpage").getText();

		if (breadcrumb.contains(title)) {
			Reporter.log("breadcrumb is present as per menu navigation",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("breadcrumb is not present as per menu navigation",

					MessageTypes.Fail);

		}
	}
	
	@QAFTestStep(description = "user verify Approve ,Reject,View options are displayed for leave request.")
	public void userVerifyApproveRejectViewOptionsAreDisplayed() {
		if (verifyPresent("button.viewleaverequest.managerViewPage")
				&& verifyPresent("button.approveleaverequest.managerViewPage")
				&& verifyPresent("button.rejectleaverequest.managerViewPage")) {
			Reporter.logWithScreenShot(
					"Approve ,Reject,View options are present in leave request tab",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot(
					"Approve ,Reject,View options are Not present in leave request tab",
					MessageTypes.Info);
		}
	}
	@QAFTestStep(description = "user verify Approve ,Reject,View options are displayed for expense.")
	public void userVerifyApproveRejectViewOptionsAreDisplayedForExpense() {
		click("tab.expense.managerViewPage");
		if (verifyPresent("button.viewexpenserequest.managerViewPage")
				&& verifyPresent("button.approveepenserequest.managerViewPage")
				&& verifyPresent("button.rejectexpnserequest.managerViewPage")) {
			Reporter.logWithScreenShot(
					"Approve ,Reject,View options are present in expense request tab",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot(
					"Approve ,Reject,View options are Not present in expense request tab",
					MessageTypes.Info);
		}
	}

	
	

}
