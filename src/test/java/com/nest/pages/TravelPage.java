package com.nest.pages;


import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.verifyEnabled;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForPresent;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;


import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.SendKeysAction;

import com.nest.beans.AddTravelRequestBean;
import com.nest.components.CalendarComponent;
import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;


public class TravelPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
	}

	PublisherPage publisherpage = new PublisherPage();
	LeavePage leaveobj = new LeavePage();
	CalendarComponent calendar = new CalendarComponent();

	@QAFTestStep(description = "user verify option of action buttons")
	public void verifyActionButtons() {
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		Validator.verifyThat("verify option of action buttons",
				verifyPresent("btn.approve.travelrequest"),
				Matchers.equalTo(true));
		Validator.verifyThat("verify option of action buttons",
				verifyPresent("btn.reject.travelrequest"),
				Matchers.equalTo(true));

	}

	@QAFTestStep(description = "user verify user should be able to take action using this action buttons")
	public void verifyActionButtonsEnable() {

		Validator.verifyThat("able to take action using this action buttons",
				verifyEnabled("btn.approve.travelrequest"),
				Matchers.equalTo(true));
		Validator.verifyThat("able to take action using this action buttons",
				verifyEnabled("btn.reject.travelrequest"),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user select the Travel menu and My Travel Requests submenu")
	public void selectTravelMenuSubmenu() {
		waitForNotVisible("loader");
		click("menu.travel.userhomepage");
		click("submenu.mytravelrequest.userhomepage");
	}

	// @QAFTestStep(description = "user select filter {0} and click on Search
	// button")
	// public void userSelectFilterAndClickOnSearchButton(String requestid) {
	// sendKeys(requestid, "textbox.travelrequestid.travelrequestpage");
	// click("btn.search.travelrequestpage");
	// }

	@QAFTestStep(description = "user should see the travel details page with colors according to the theme of the application")
	public void userShouldSeeTheTravelDetailsPageWithColorsAccordingToTheThemeOfTheApplication() {

		String color = driver.findElement("btn.search.travelrequestpage")
				.getCssValue("background-color");
		Reporter.log(color);
		Validator.verifyThat("rgba(235, 114, 3, 1)", Matchers.containsString(color));
	}

	

	@QAFTestStep(description = "user should see all the filters {0} reset to default values")
	public void userShouldSeeAllTheFiltersResetToDefaultValues(String resetvalue) {

		waitForNotVisible("loader.userhomepage");
		String requestidtxtboxval =
				driver.findElement("textbox.travelrequestid.travelrequestpage").getText();
		Validator.verifyThat(requestidtxtboxval, Matchers.containsString(""));
		String actualrequestid =
				driver.findElement("text.requestidfromtable.travelrequestpage").getText();
		Reporter.log(actualrequestid);
		Validator.verifyThat(actualrequestid, Matchers.not(resetvalue));

	}

	@QAFTestStep(description = "user click on new travel request button")
	public void userClickOnNewTravelRequestButton() {
		waitForNotVisible("wait.loader");
		verifyPresent("btn.new.travel.request.mytravelrequestpage");
		click("btn.new.travel.request.mytravelrequestpage");
	}

	@QAFTestStep(description = "user navigate to add travel request page")
	public void userNavigateToAddTravelRequestPage() {

		waitForNotVisible("wait.loader");
		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.contains("addtravelrequest")) {
			Reporter.logWithScreenShot("User navigate to add Travel request page",
					MessageTypes.Pass);
		}

		else {
			Reporter.logWithScreenShot("User NOT navigate to Travel details page",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user should navigate to my travel request page")
	public void userShouldNavigateToMyTravelRequestPage() {
		waitForNotVisible("wait.loader");
		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.contains("mytravelrequests")) {
			Reporter.logWithScreenShot("User navigate to add Travel request page",
					MessageTypes.Pass);
		}

		else {
			Reporter.logWithScreenShot("User NOT navigate to Travel details page",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verify all the fields of page")
	public void userVerifyAllTheFieldsOfPage() {
		if (verifyPresent("list.fields.addtravelrequestpage")
				| verifyPresent("label.id.proof.addtravelrequestpage")
				| verifyPresent("label.car.booking.addtravelrequestpage")) {

			Reporter.logWithScreenShot("Fileds are Display");
		} else Reporter.logWithScreenShot("Fileds are not Display");

		// checking mandatory fileds
		List<QAFWebElement> options =
				NestUtils.getQAFWebElements("list.fields.addtravelrequestpage");
		for (QAFWebElement e : options) {
			String text = e.getText();
			if ((text).contains("*")) {

				Reporter.logWithScreenShot(text + " is mandatory field");
			} else {
				Reporter.logWithScreenShot(text + " is not mandatory field");
			}
		}

	}

	@QAFTestStep(description = "user should see Travel details page")
	public void userShouldSeeTravelDetailsPage() {
		verifyPresent("submenu.mytravelrequest.userhomepage");
	}

	@QAFTestStep(description = "user fill {0} details and click on back button")
	public void userFillDetailsAndClickOnBackButton(String key) {
		AddTravelRequestBean data = new AddTravelRequestBean();
		data.fillFromConfig(key);
		waitForNotVisible("wait.loader");
		sendKeys(data.getTravelpurpose(),
				"txtbox.purpose.of.travel.addtravelrequestpage");
		click("dropdown.trip.type.addtravelrequestpage");
		click("dropdown.round.trip.adtravelrequestpage");
		sendKeys(data.getJourneyfrom(), "txtbox.journey.from.addtravelrequestpage");
		sendKeys(data.getJourneyto(), "txtbox.journey.to.addtravelrequestpage");
		waitForNotVisible("wait.loader");
		CommonPage
				.waitForLocToBeClickable("date.journey.start.date.addtravelrequestpage");
		selectStartDate("date.journey.start.date.addtravelrequestpage",
				"datepicker.date.addtravelrequestpage", "20");
		selectEndDate("date.journey.end.date.addtravelrequestpage",
				"datepicker.date.addtravelrequestpage", "26");
		sendKeys(data.getProjectname(), "txtbox.project.name.addtravelrequestpage");
		sendKeys(data.getExtrainfo(), "txtbox.extra.info.addtravelrequestpage");
		/*
		 * String filelocation="/nest-automation/resources/data/SampleInput.zip";
		 * sendKeys(filelocation,
		 * "xpath=.//div[@class=\"attachment_dotted_box m-b-15 flot-left background_white m-r-10 drop-box cursor-pointer\"]"
		 * );
		 */
		click("btn.clear.addtravelrequestpage");

		waitForNotVisible("wait.loader");
		String currentUrl = driver.getCurrentUrl();
		if (currentUrl.contains("mytravelrequests")) {
			Reporter.logWithScreenShot("User navigate to add Travel request page",
					MessageTypes.Pass);
		}

		else {
			Reporter.logWithScreenShot("User not navigate to Travel details page",
					MessageTypes.Fail);
		}

		Reporter.logWithScreenShot("Clear button is properly working");
	}

	

	@QAFTestStep(description = "user clicks on reset")
	public void userClicksOnReset() {
		waitForNotVisible("loader.userhomepage");
		click("btn.reset.travelrequestpage");

	}

//	@QAFTestStep(description = "user select filter {0} and click on Search button")
//	public void selectFilter(String empName) throws AWTException {
//		waitForNotVisible("loader.userhomepage");
//		String handle=driver.getWindowHandle();
//		driver.switchTo().window(handle);
//		click("btn.employeename.travelrequest");
//		sendKeys(empName, "textbox.employeename.travelrequest");
//	Robot rob=new Robot();
//	rob.keyPress(KeyEvent.VK_ENTER);
//	rob.keyRelease(KeyEvent.VK_ENTER);
//		waitForNotVisible("loader.userhomepage");
//		click("btn.search.travelrequestpage");
//		
//	}

	@QAFTestStep(description = "user select filter {0} using requestid and click on Search button")
	public void selectFilterUsingRequestId(String requestId)
	{
		waitForNotVisible("loader.userhomepage");
		WebElement idtextbox = new QAFExtendedWebElement("textbox.travelrequestid.travelrequestpage");
		idtextbox.clear();
		sendKeys(requestId,"textbox.travelrequestid.travelrequestpage");
		waitForNotVisible("loader.userhomepage");
		click("btn.search.travelrequestpage");
	}
	
	@QAFTestStep(description="user should see data populated based on the applied filter requestid {0}")
	public void verifyFilterDataByRequestId(String reqId)
	{
		waitForNotVisible("loader.userhomepage");
		String requestId =driver.findElement("text.requestidfromtable.travelrequestpage").getText();
		Validator.verifyThat(requestId, Matchers.containsString(reqId));
		
	}


	
@QAFTestStep(description = "user should see data populated based on the applied filter {0}")
	public void verifyFilteredData(String empname) {
		waitForNotVisible("loader.userhomepage");
		String empNameFromTable= driver.findElement("text.empname.travelrequestpage").getText();
		Validator.verifyThat(empNameFromTable, Matchers.containsString(empname));
		
	}
	

	@QAFTestStep(description = "user clicks on reset")
	public void clickResetButton() {
		waitForNotVisible("loader.userhomepage");
		click("btn.reset.travelrequestpage");

	}

	@QAFTestStep(description = "user should see all the filters {0} reset to default values")
	public void verifyResetFilters(String resetvalue) {

		waitForNotVisible("loader.userhomepage");
		String requestidtxtboxval=driver.findElement("textbox.travelrequestid.travelrequestpage").getText();
		if(requestidtxtboxval.contains(""))
			Reporter.logWithScreenShot("The filter is reset");
		else
			Reporter.logWithScreenShot("The fileter is not reset");
		
	}
	@QAFTestStep(description = "user should see the travel details page with colors according to the theme of the application")
	public void verifyColor() {

		String color = driver.findElement("btn.search.travelrequestpage")
				.getCssValue("background-color");
		if (color.contains("rgba(235, 114, 3, 1)"))
			Reporter.logWithScreenShot(
					"Colors on Travel details page are according to the theme",
					MessageTypes.Pass);
		else
			Reporter.log("Colors on Travel details page are not according to the theme",
					MessageTypes.Fail);

	}

		
	
	@QAFTestStep(description = "the request should get Approved")
	public void verifyApproved() {
		waitForNotVisible("wait.loader");
		String status = driver.findElement("text.approved.travelrequestpage").getText();
		Reporter.log(status);
		if (status.contains("Approved"))
			Reporter.logWithScreenShot("Request Approved", MessageTypes.Pass);
		else Reporter.logWithScreenShot("Request not Approved", MessageTypes.Fail);
	}

		@QAFTestStep(description = "user selects Approve from the drop down menu for the request and click on Submit")
		public void selectApprove() {
			waitForNotVisible("wait.loader");
			click("drdwn.select.travelrequestpage");
			click("btn.approve.travelrequestpage");
			sendKeys("Request Approved", "text.commentarea.approvalcomments");
			waitForNotVisible("wait.loader");
			click("btn.submit.approvalcomments");
			waitForNotVisible("wait.loader");
			JavascriptExecutor jse=(JavascriptExecutor)driver;
			jse.executeScript("window.scrollTo(0,document.body.scrollHeight)");
			click("btn.submit.travelreuestpage");
			jse.executeScript("scroll(0, -250)");
		}
	
	@QAFTestStep(description = "the request should get Rejected")
	public void verifyRejected() {
		waitForNotVisible("wait.loader");
		String status = driver.findElement("text.approved.travelrequestpage").getText();
		if (status.contains("Rejected"))
			Reporter.logWithScreenShot("Request Rejected", MessageTypes.Pass);
		else Reporter.logWithScreenShot("Request not Rejected", MessageTypes.Fail);
	}

	@QAFTestStep(description = "user navigates to travel request inside the travel requests list")
	public void navigateToTravelRequest() {
		waitForNotVisible("wait.loader");
		click("lnk.travelpurpose.travelrequestpage");

	}

	@QAFTestStep(description = "User should be navigated to travel details page.")
	public void verifyTravelRequestDetailsPage() {
		waitForNotVisible("wait.loader");
		verifyPresent("text.travelrequestDetails.travelrequestpage");
	}
	
		@QAFTestStep(description = "user verifies the breadcrumb on travel request page {0}")
		public void verifyBreadcrumb(String title) {
			waitForNotVisible("wait.loader");
			String breadcrumb=
					new QAFExtendedWebElement("breadcrumb.travelrequestpage").getText();
			
			if (breadcrumb.contains(title)) {
				Reporter.log("breadcrumb is present as per menu navigation",
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot("breadcrumb is not present as per menu navigation",
					
						MessageTypes.Fail);
			
			}
			
			
			
			
			
			
		}

	

	@QAFTestStep(description = "user cliks on back button")
	public void clickOnBack() {
		waitForNotVisible("wait.loader");
		driver.navigate().back();
	}


		@QAFTestStep(description = "user selects Reject from the drop down menu for the request and click on Submit")
		public void selectReject() {
			waitForNotVisible("wait.loader");
			click("drdwn.select.travelrequestpage");
			waitForPresent("btn.approve.travelrequestpage");
			click("btn.reject.travelrequestpage");
			sendKeys("Request Rejected", "text.commentarea.approvalcomments");
			click("btn.submit.approvalcomments");
			waitForNotVisible("wait.loader");
			JavascriptExecutor jse=(JavascriptExecutor)driver;
			jse.executeScript("window.scrollTo(0,document.body.scrollHeight)");
			click("btn.submit.travelreuestpage");
			
			
		
	}
	@QAFTestStep(description = "user clicks on bookings button on travel request details page")
	public void clickOnBookings() {
		waitForNotVisible("wait.loader");
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollTo(0,document.body.scrollHeight)");
		click("btn.bookings.travelrequestpage");

	}

	@QAFTestStep(description = "User should be navigated to Travel Bookings page")
	public void verifyTravelBookingsPage() {
		waitForNotVisible("wait.loader");
		verifyPresent("text.travelbookingstext.travelrequestpage");

	}

	@QAFTestStep(description = "user observe the ui of add travel request page")
	public void userObserveTheUiOfAddTravelRequestPage() {
		String color = driver.findElement("btn.submit.addtravelrequestpage").getCssValue("background-color");
		Reporter.log(color);
		Validator.verifyThat("rgba(235, 114, 3, 1)", Matchers.containsString(color));
	}

	@QAFTestStep(description = "user observe the ui of my travel request page")
	public void userObserveTheUiOfMyTravelRequestPage() {
		List<QAFWebElement> options = NestUtils.getQAFWebElements("btn.all.mytravelrequestpage");
		for (QAFWebElement option : options) {
			String color = option.getCssValue("background-color");
			Reporter.log(color);
			Validator.verifyThat("rgba(235, 114, 3, 1)", Matchers.containsString(color));
		}
	}

	public void selectStartDate(String type, String loc, String date) {
		waitForNotVisible("image.loader.common");
		waitForPageToLoad();
		click(type);
		QAFExtendedWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), date));
		element.click();
	}

	public void selectEndDate(String type, String loc, String date) {
		waitForNotVisible("image.loader.common");
		waitForPageToLoad();
		click(type);
		QAFExtendedWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), date));
		element.click();
	}

		@QAFTestStep(description = "user verify Breadcrumbs should be {0}")
	public void userVerifyBreadcrumbsShouldBe(String breadcrumb) {
		waitForNotVisible("wait.loader");
		publisherpage.verifyBreadcrumbs("titel.mypreferences.post.publisher", breadcrumb);
	}
		
		@QAFTestStep(description ="user adds a new travel request using {0}")
		public String addTravelRequest(String key)
		{
			AddTravelRequestBean data = new AddTravelRequestBean();
			data.fillFromConfig(key);
			waitForNotVisible("wait.loader");
			new QAFExtendedWebElement("button.logout.userhomepage").isPresent();
			CommonPage.clickUsingJavaScript("btn.new.travel.request.mytravelrequestpage");
			sendKeys(data.getTravelpurpose(),"txtbox.purpose.of.travel.addtravelrequestpage");
			click("txtbox.journey.from.addtravelrequestpage");
			click("text.mumbai.journeyfrom.travelrequestpage");
			waitForNotVisible("wait.loader");
			click("txtbox.journey.to.addtravelrequestpage");
			click("text.Pune.journeyto.travelrequestPage");
			waitForNotVisible("wait.loader");
			selectStartDate("date.journey.start.date.addtravelrequestpage","datepicker.date.addtravelrequestpage", "20");
			selectEndDate("date.journey.end.date.addtravelrequestpage","datepicker.date.addtravelrequestpage", "26");
			sendKeys(data.getProjectname(), "txtbox.project.name.addtravelrequestpage");
			waitForNotVisible("wait.loader");
			CommonPage.clickUsingJavaScript("btn.submit.addtravelrequest.travelrequestpage");
			waitForNotVisible("wait.loader");
			String travelRequestId =driver.findElement("text.requestid.mytravelrequest.travelrequestpage").getText();
			Reporter.log(travelRequestId);
			return travelRequestId;	
			
		}
		
		@QAFTestStep(description = "user performs pagination")
		public void clickPageNumber() {
			waitForNotVisible("wait.loader");
			JavascriptExecutor jse=(JavascriptExecutor)driver;
			jse.executeScript("window.scrollTo(0,document.body.scrollHeight)");
			waitForNotVisible("wait.loader");
			click("lnk.nextpage.travelrequestpage");
			if(driver.findElement("lnk.number.travelrequestpage").isEnabled())
				Reporter.logWithScreenShot("Pagination successful",MessageTypes.Pass);
			else
				Reporter.logWithScreenShot("Pagination failed",MessageTypes.Fail);
		}

		
		@QAFTestStep(description = "user select filter {0} using empname and click on Search button")
		public void selectFilterUsingEmpName(String empName) throws AWTException {
			waitForNotVisible("loader.userhomepage");
			String handle=driver.getWindowHandle();
			driver.switchTo().window(handle);
			click("btn.employeename.travelrequest");
			sendKeys(empName, "textbox.employeename.travelrequest");
		Robot rob=new Robot();
		rob.keyPress(KeyEvent.VK_ENTER);
		rob.keyRelease(KeyEvent.VK_ENTER);
			waitForNotVisible("loader.userhomepage");
			
			click("btn.search.travelrequestpage");
			
		}
		
		@QAFTestStep(description = "user selects the record and clicks on bookings button")
		public void clickBookingsButton()
		{
			CommonPage.clickUsingJavaScript("lnk.travelpurpose.travelrequestpage");
			JavascriptExecutor jse1=(JavascriptExecutor)driver;
			jse1.executeScript("window.scrollTo(0,document.body.scrollHeight)");
			CommonPage.clickUsingJavaScript("btn.bookings.travelrequestpage");
		}
		
		
		
		@QAFTestStep(description = "user verifies bookings listing page")
		public void verifyBookingsListingPage()
		{
			
			
		String travelBookingHeading=driver.findElement("heading.travelbookings.travelrequestpage").getText();
		if(travelBookingHeading.contains("Travel Bookings"))
			Reporter.logWithScreenShot("Travel Bookings Page Is Displayed");
		else
			Reporter.logWithScreenShot("Travel Bookings Page Is Not Displayed");
		
			
		}
		
		
		@QAFTestStep(description ="user logs out")
		public void logoutApp()
		{
			CommonPage.clickUsingJavaScript("button.logout.userhomepage");
		}

			

		
}
