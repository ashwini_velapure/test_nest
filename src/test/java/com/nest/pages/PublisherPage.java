package com.nest.pages;
import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import java.util.List;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import static com.qmetry.qaf.automation.step.CommonStep.*;
import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import com.nest.beans.CreateNewPostBean;
import com.qmetry.qaf.automation.core.ConfigurationManager;

public class PublisherPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stub
	}

	@QAFTestStep(description = "user clicks on create newpost")
	public void clicksCreateNewpost() {

		waitForNotVisible("wait.loader");
		WebElement element = driver.findElement("btn.createNewPost.publisher");
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
		waitForNotVisible("wait.loader");
	}

	@QAFTestStep(description = "user verify Titel of the page should {0}")
	public void verifyTitelNewpost(String titel) {

		waitForNotVisible("wait.loader");
		String currentUrl = driver.getCurrentUrl();
		Reporter.log(currentUrl);
		if (currentUrl.contains(titel)) {
			Reporter.logWithScreenShot("verify Titel of the page is verified",
					MessageTypes.Pass);
		}

		else {
			Reporter.logWithScreenShot("verify Titel of the page is not verified",
					MessageTypes.Fail);
		}
	}

	public void verifyBreadcrumbs(String loc, String breadcrumb) {
		boolean flag = CommonPage.verifyLocContainsText(loc, breadcrumb);
		if (flag) {
			Reporter.logWithScreenShot("Breadcrumb is displayed", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("Breadcrumb is not displayed", MessageTypes.Fail);
		}
	}
	public List<QAFWebElement> allTextField() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("textField.list.mypost.publisher");
	}

	@QAFTestStep(description = "user verify All the given Text field")
	public void verifyTextField() {
		for (int i = 0; i < allTextField().size(); i++) {
			allTextField().get(i).verifyPresent();
		}
	}

	public List<QAFWebElement> allDropDown() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("dropdown.list.mypost.publisher");
	}

	@QAFTestStep(description = "user verify {0} given below")
	public void verifyDropdowns(String value) {

		for (int i = 0; i < allDropDown().size(); i++) {
			String result = allDropDown().get(i).getAttribute("class");
			if (result.contains(value)) {
				Reporter.logWithScreenShot("Dropdown is displayed", MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot("Dropdown is not displayed",
						MessageTypes.Fail);
			}
		}
	}

	@QAFTestStep(description = "User click on submit")
	public void clickSubmit() {
		CommonPage.clickUsingJavaScript("btn.submit.mypost.publisher");
		waitForNotVisible("wait.loader");
	}

	public List<QAFWebElement> compulsoryField() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("compulsory.field.mypost.publisher");
	}

	@QAFTestStep(description = "User verify compulsory field display popover {0}")
	public void verifyCompulsoryFieldTooltip(String actual) {

		for (int i = 0; i < compulsoryField().size(); i++) {
			Actions toolAct = new Actions(driver);
			toolAct.moveToElement(compulsoryField().get(i)).build().perform();
			if (verifyPresent("tooltip.box.mypost.publisher")) {
				Reporter.logWithScreenShot("tool tip is displayed", MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot("tool tip is Not displayed",
						MessageTypes.Fail);
			}
		}
	}

	public void selectDate(String type, String loc, String date) {
		waitForNotVisible("wait.loader");
		waitForVisible(type);
		click(type);
		QAFExtendedWebElement element = new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), date));
		// element.click();
		JavascriptExecutor executor =
				(JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].click();", element);
	}

	@QAFTestStep(description = "user select from date {0} from calender")
	public void selectDateCalender(String date) {

		selectDate("calender.from.date.loc", "datepicker.loc", date);

	}

	@QAFTestStep(description = "user select to date {0} from calender")
	public void selectToDate(String date) {

		selectDate("calender.to.date.loc", "datepicker.loc", date);
	}

	@QAFTestStep(description = "verify display error message {0}")
	public void verifyErrorMessage(String msg) {

		String error = new QAFExtendedWebElement("error.msg.calender.mypost.publisher")
				.getText();
		if (error.contains(msg)) {
			Reporter.logWithScreenShot("error msg display ", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("error msg is Not display ", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user navigate to My Preference Page")
	public void navigateMyPreferencePage() {
		waitForNotVisible("wait.loader");
		CommonPage.clickUsingJavaScript("btn.mypreferences.post.publisher");
		waitForNotVisible("wait.loader");
	}

	@QAFTestStep(description = "user verify titel {0} of the page")
	public void verifyTitelMyPreference(String titel) {
		verifyTitelOfPage("titel.mypreferences.post.publisher", titel);
	}
	@QAFTestStep(description = "user verify Breadcrumb of {0} page")
	public void verifyBreadcrumbMypreferencesPage(String breadcrumb) {
		waitForNotVisible("wait.loader");
		verifyBreadcrumbs("titel.mypreferences.post.publisher", breadcrumb);
	}

	@QAFTestStep(description = "user verify Privacy button should be present on right uper side of page")
	public void verifyPrivacyButton() {
		Validator.verifyThat("Privacy button is present",
				verifyPresent("btn.privecy.mypreferences.post.publisher"),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify slider button should be present")
	public void verifySliderButton() {
		Validator.verifyThat("Slider button is present",
				verifyPresent("btn.slider.mypreferences.post.publisher"),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify all platform should be present")
	public void verifyAllPlatform() {
		for (int i = 0; i < allPlatform().size(); i++) {
			allPlatform().get(i).verifyPresent();
		}
	}
	public List<QAFWebElement> allPlatform() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("btn.dropdown.mypreference.post.publisher");
	}

	@QAFTestStep(description = "user select linkedin checkbox")
	public void selectLinkedin() {
		CommonPage
				.clickUsingJavaScript("checkedbox.linkedin.mypreferences.post.publisher");
	}

	@QAFTestStep(description = "user verify Buttons should be there")
	public void verifyButtonsOnMypreferencesPage() {
		verifyPresent("btn.save.mypreference.post.publisher");
		verifyPresent("btn.back.mypost.publisher");
	}

	@QAFTestStep(description = "user click on privecy button")
	public void clickPrivecyButton() {
		CommonPage.clickUsingJavaScript("btn.privecy.mypreferences.post.publisher");
	}

	@QAFTestStep(description = "user verify the {0} Message")
	public void verifyAlertMessage(String message) {
		verifyPresent("privecy.alert.message.post.publisher");
		boolean flag = CommonPage
				.verifyLocContainsText("privecy.alert.message.post.publisher", message);

		if (flag) {
			Reporter.logWithScreenShot("Privecy Alert is displayed ", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("Privecy Alert is not displayed",
					MessageTypes.Fail);
		}
	}
	public void verifyTitelOfPage(String loc, String titel) {
		waitForNotVisible("wait.loader");
		boolean flag = CommonPage.verifyLocContainsText(loc, titel);

		if (flag) {
			Reporter.logWithScreenShot("Titel is verified", MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("titel is not verified", MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user verify titel of MY Post {0} page")
	public void verifyTitelMYPostPage(String title) {

		verifyTitelOfPage("titel.mypreferences.post.publisher", title);
	}

	@QAFTestStep(description = "user verify breadcrump of Mypost page {0}")
	public void verifyBreadcrumpMypostPage(String breadCrumb) {

		verifyBreadcrumbs("titel.mypreferences.post.publisher", breadCrumb);
	}

	@QAFTestStep(description = "user verify Pagination should be present")
	public void verifyPaginationMyPostPage() {
		Validator.verifyThat("Pagination is present",
				verifyPresent("pagination.mypost.publisher"), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify filter icon should be on right uper side of page")
	public void verifyFilterIcon() {
		Validator.verifyThat("Filter icon is present",
				verifyPresent("btn.filter.mypost.publisher"), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify table column header")
	public void verifyTableHeader() {
		for (int i = 0; i < tableHeader().size(); i++) {
			tableHeader().get(i).verifyPresent();
		}
		Validator.verifyThat("Title must be : ", tableHeader().get(0).getText(),
				Matchers.equalToIgnoringCase("Title"));
		Validator.verifyThat("Category must be : ", tableHeader().get(1).getText(),
				Matchers.equalToIgnoringCase("Category"));
		Validator.verifyThat("Posted On must be : ", tableHeader().get(2).getText(),
				Matchers.equalToIgnoringCase("Posted On"));
		Validator.verifyThat("Action must be : ", tableHeader().get(3).getText(),
				Matchers.equalToIgnoringCase("Action"));
	}
	public List<QAFWebElement> tableHeader() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("table.header.mypost.publish");
	}

	@QAFTestStep(description = "user verify Action column should have edit and delete icons")
	public void verifyButtonEdit_AndDelete() {
		Validator.verifyThat("Edit icon is present",
				verifyPresent("btn.edit.action.mypost.publisher"),
				Matchers.equalTo(true));
		Validator.verifyThat("Delete icon is present",
				verifyPresent("btn.delete.action.mypost.publisher"),
				Matchers.equalTo(true));
	}

	public void selectFromList(String locOfDropdown, String locOfList, String option) {
		waitForVisible(locOfDropdown);
		CommonPage.clickUsingJavaScript(locOfDropdown);
		List<QAFWebElement> list =
				new QAFExtendedWebElement("xpath=/html").findElements(locOfList);
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getText().contains(option)) {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", list.get(i));
				break;
			}
		}
	}

	@QAFTestStep(description = "user select {0} from catagory Dropdown")
	public void selectDataCatagoryDropdown(String valueToSelect) {
		selectFromList("btn.dropdown.catagory.newpost",
				"catagory.dropdown.newpost.publisher", valueToSelect);
	}

	@QAFTestStep(description = "verify date picker should be displayed")
	public void verifyDatePicker() {
		Validator.verifyThat("calender Start Date is present",
				verifyPresent("calender.startDate.catagory.newpost"),
				Matchers.equalTo(true));
		Validator.verifyThat("calender End Date is present",
				verifyPresent("calender.Enddate.catagory.newpost"),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user fill {0} required details")
	public void fillRequiredDetailsNewPost(String key) {

		CreateNewPostBean post = new CreateNewPostBean();
		post.fillFromConfig(key);
		CommonPage.clickUsingJavaScript("textbox.title.newpost.publisher");
		sendKeys(post.getTitle(), "textbox.title.newpost.publisher");
		CommonPage.clickUsingJavaScript("textbox.posturl.newpost.publisher");
		sendKeys(post.getPostUrl(), "textbox.posturl.newpost.publisher");
		CommonPage.clickUsingJavaScript("textbox.description.newpost.publisher");
		sendKeys(post.getDescription(), "textbox.description.newpost.publisher");
	}

	@QAFTestStep(description = "user select {0} from location Dropdown")
	public void selectLocationDropdown(String valueToSelect) {
		selectFromList("btn.dropdown.location.newpost",
				"catagory.dropdown.newpost.publisher", valueToSelect);
	}

	@QAFTestStep(description = "user verify Buttons given below are present")
	public void verifyButtonPresent() {
		Validator.verifyThat("Buttons given below are present",
				verifyPresent("btn.submit.mypost.publisher"), Matchers.equalTo(true));
		Validator.verifyThat("Buttons given below are present",
				verifyPresent("btn.back.mypost.publisher"), Matchers.equalTo(true));
	}
	public List<QAFWebElement> searchField() {
		return new QAFExtendedWebElement("xpath=/html")
				.findElements("table.row.catagory.list.mypost.publisher");
	}

	@QAFTestStep(description = "user verify Search results should be according to selected filters")
	public void verifySearchResultsFilters() {
		boolean flag = false;
		for (int i = 0; i < searchField().size(); i++) {
			if (searchField().get(i).getText().contains("Blog")) {
				flag = true;
			} else {
				flag = false;
				break;
			}
		}
		if (flag) {
			Reporter.logWithScreenShot(
					"Search results should be according to selected filters",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot(
					"Search results should Not according to selected filters",
					MessageTypes.Fail);
		}
	}

	@QAFTestStep(description = "user click on {0}")
	public void clickOnSearch(String btn) {
		QAFExtendedWebElement element = new QAFExtendedWebElement(String
				.format(ConfigurationManager.getBundle().getString("btn.all"), btn));
		waitForNotVisible("wait.loader");
		element.waitForVisible(5000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
		waitForNotVisible("wait.loader");
	}

	@QAFTestStep(description = "user navigate to postdetails page")
	public void navigateToPostDetailsPage() {
		CommonPage.clickUsingJavaScript("title.post.table.mypost.publisher");
		waitForNotVisible("wait.loader");
	}

	@QAFTestStep(description = "user verify title of postDetails {0} page")
	public void verifyTitlePostDetailsPage(String title) {
		waitForNotVisible("wait.loader");
		verifyTitelOfPage("breadcrumb.mypost.publisher", title);
	}

	@QAFTestStep(description = "user verify breadcrump of post details page {0}")
	public void verifyBreadcrumpOfPostDetailsPage(String breadCrumb) {
		verifyBreadcrumbs("titel.mypreferences.post.publisher", breadCrumb);
	}

	@QAFTestStep(description = "user verify post details page")
	public void verifyPostDetailsPage() {
		List<QAFWebElement> list = new QAFExtendedWebElement("xpath=/html")
				.findElements("postdetails.all.data.publisher");
		for (QAFWebElement element : list) {
			element.verifyPresent();
		}
	}

	@QAFTestStep(description = "user verify Edit details page having filled all details")
	public void verifyRediractionAndAllField() {

		List<QAFWebElement> list = new QAFExtendedWebElement("xpath=/html")
				.findElements("textField.list.mypost.publisher");
		for (QAFWebElement element : list) {
			if (!element.getAttribute("value").isEmpty()) {
				Reporter.logWithScreenShot(element.getAttribute("value"),
						MessageTypes.Pass);
			} else {
				Reporter.logWithScreenShot(
						"Filled details are not displayed in edit post details page",
						MessageTypes.Fail);
			}
		}
	}

	@QAFTestStep(description = "user verify its redirected to the Edit details page")
	public void verifyRediractionToEditDetailsPage() {
		waitForNotVisible("wait.loader");
		verifyTitelOfPage("breadcrumb.mypost.publisher", "Edit Post");
	}

	@QAFTestStep(description = "user verify Breadcrumb of newPost should be {0}")
	public void verifyBreadcrumbNewPost(String breadcrumb) {
		verifyBreadcrumbs("breadcrumb.mypost.publisher", breadcrumb);
	}

	@QAFTestStep(description = "user click on any checkbox in postpage")
	public void clickCheckBoxPostPage() {
		waitForNotVisible("wait.loader");
		CommonPage.clickUsingJavaScript("checkbox.post.publisher");
	}

	@QAFTestStep(description = "user verify All the platforms button should be enabled")
	public void verifyPlatformsButton() {
		Validator.verifyThat("Platforms Buttons are Enabled",
				verifyEnabled("btn.platform.post.publisher"), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify lable of datepicker")
	public void verifyDatepickers() {
		Validator.verifyThat("To Date datePicker", verifyEnabled("calender.to.date.loc"),
				Matchers.equalTo(true));
		Validator.verifyThat("From Date datePicker",
				verifyEnabled("calender.from.date.loc"), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify Check box for Published post")
	public void verifyCheckBoxPublishedPost() {
		Validator.verifyThat("Check box for Published is present",
				verifyPresent("checkbox.post.publisher"), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify platform icon should be present")
	public void verifyPlatformIcon() {
		Validator.verifyThat("platform icon is present",
				verifyPresent("btn.platform.post.publisher"), Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify Social media column have icon of all platforms")
	public void verifySocialMediaColumnIcon() {
		List<QAFWebElement> list = new QAFExtendedWebElement("xpath=/html")
				.findElements("icon.table.post.publisher");
		for (QAFWebElement element : list) {
			Validator.verifyThat("Social Media icon is present", element.verifyPresent(),
					Matchers.equalTo(true));
		}
	}

	@QAFTestStep(description = "user verify every row in table has check box")
	public void verifyRowCheckBox() {
		List<QAFWebElement> listRow = new QAFExtendedWebElement("xpath=/html")
				.findElements("table.row.post.publisher");
		List<QAFWebElement> listCheckBox = new QAFExtendedWebElement("xpath=/html")
				.findElements("checkbox.table.list.post.publisher");
		if (listRow == listCheckBox) {
			Reporter.logWithScreenShot("checkBox are display for each Row",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("checkBox are NOT display for each Row",
					MessageTypes.Pass);
		}
	}

	@QAFTestStep(description = "user verify all the header columns of the table")
	public void verifyHeaderColumnsOfTable() {
		List<QAFWebElement> tableHeader = new QAFExtendedWebElement("xpath=/html")
				.findElements("table.header.post.publisher");
		Validator.verifyThat("Header must be : ", tableHeader.get(0).getText(),
				Matchers.equalToIgnoringCase("Title"));
		Validator.verifyThat("Header must be : ", tableHeader.get(1).getText(),
				Matchers.equalToIgnoringCase("Category"));
		Validator.verifyThat("Header must be : ", tableHeader.get(2).getText(),
				Matchers.equalToIgnoringCase("Posted On"));
		Validator.verifyThat("Header must be : ", tableHeader.get(3).getText(),
				Matchers.equalToIgnoringCase("Social Media"));
		Validator.verifyThat("Header must be : ", tableHeader.get(4).getText(),
				Matchers.equalToIgnoringCase("Status"));
	}

	@QAFTestStep(description = "user verify breadcrump of post page {0}")
	public void verifyBreadcrumbPostPage(String breadCrumb) {
		verifyBreadcrumbs("breadcrumb.mypost.publisher", breadCrumb);
	}

	@QAFTestStep(description = "user verify title of post {0} page")
	public void verifyTitlePostPage(String title) {
		waitForNotVisible("wait.loader");
		verifyTitelOfPage("breadcrumb.mypost.publisher", title);
	}

	@QAFTestStep(description = "user verify mypreference button")
	public void verifyMyPreferenceButton() {
		Validator.verifyThat("mypreference button is present",
				verifyPresent("btn.mypreferences.post.publisher"),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify {0} {1} given below")
	public void verifyDropdown(String type, String value) {
		switch (value) {
			case "dropdown" :
				QAFExtendedWebElement element1 =
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("dropdown.all.publisher"), value));
				String result = element1.getAttribute("class");
				if (result.contains(value)) {
					Reporter.logWithScreenShot(type + "Dropdown is displayed",
							MessageTypes.Pass);
				} else {
					Reporter.logWithScreenShot(type + "Dropdown is not displayed",
							MessageTypes.Fail);
				}

				break;
			case "Textbox" :
				QAFExtendedWebElement element =
						new QAFExtendedWebElement(String.format(ConfigurationManager
								.getBundle().getString("textbox.all.publisher"), type));
				String attribute = element.getAttribute("placeholder");
				if (attribute.contains(type)) {
					Reporter.logWithScreenShot(value + " is displayed",
							MessageTypes.Pass);
				} else {
					Reporter.logWithScreenShot(value + " is not displayed",
							MessageTypes.Fail);
				}
				break;
		}

	}

	@QAFTestStep(description = "user performs action on filter icon")
	public void click_On_FilterIcon() {
		waitForNotVisible("wait.loader");
		CommonPage.clickUsingJavaScript("btn.filter.mypost.publisher");
		// click("btn.filter.mypost.publisher");
	}

	@QAFTestStep(description = "user verify filter options are not visible")
	public void verify_FilterOptions() {
		List<QAFWebElement> calenderOption = new QAFExtendedWebElement("xpath=/html")
				.findElements("filter.calender.mypost.publisher");
		for (int i = 0; i < calenderOption.size(); i++) {
			Validator.verifyThat("filter options are not visible",
					calenderOption.get(i).verifyNotVisible(), Matchers.equalTo(true));
		}
		List<QAFWebElement> button = new QAFExtendedWebElement("xpath=/html")
				.findElements("button.search.reset.mypost.publisher");
		for (int j = 0; j < button.size(); j++) {
			Validator.verifyThat("search and reset buttons are not visible",
					button.get(j).verifyNotVisible(), Matchers.equalTo(true));
		}
	}

	@QAFTestStep(description = "user navigate to the postdetail page")
	public void click_On_PostTitle() {
		waitForNotVisible("wait.loader");
		click("calender.fromdate.myposts.publisher");
		List<QAFWebElement> calenderValue = new QAFExtendedWebElement("xpath=/html")
				.findElements("options.calender.myposts.publisher");
		for (int i = 0; i < calenderValue.size(); i++) {
			calenderValue.get(1).click();
			break;
		}
		List<QAFWebElement> monthValue = new QAFExtendedWebElement("xpath=/html")
				.findElements("options.month.myposts.publisher");
		for (int j = 0; j < monthValue.size(); j++) {
			if (monthValue.get(j).getText().contains("February")) {
				monthValue.get(j).click();
				break;
			}
		}
		List<QAFWebElement> dateValue = new QAFExtendedWebElement("xpath=/html")
				.findElements("options.calender.myposts.publisher");
		dateValue.get(6).click();
		List<QAFWebElement> buttons = new QAFExtendedWebElement("xpath=/html")
				.findElements("button.search.reset.mypost.publisher");
		buttons.get(0).click();
		waitForNotVisible("wait.loader");
		List<QAFWebElement> pageNumber = new QAFExtendedWebElement("xpath=/html")
				.findElements("number.page.expensepage");
		for (int i = 0; i < pageNumber.size(); i++) {
			JavascriptExecutor js = ((JavascriptExecutor) driver);
			js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			pageNumber.get(i).click();
			if (new QAFExtendedWebElement("title.newopening.myposts.publisher")
					.isPresent()) {
				click("title.newopening.myposts.publisher");
				break;
			}
		}
	}

	@QAFTestStep(description = "user verify the postdetail page")
	public void verify_PostDetail_Page() {
		List<QAFWebElement> list = new QAFExtendedWebElement("xpath=/html")
				.findElements("postdetails.all.data.publisher");
		for (QAFWebElement element : list) {
			element.verifyPresent();
		}
		Validator.verifyThat("date and time is visible",
				verifyPresent("text.datetime.postdetails.publisher"),
				Matchers.equalTo(true));
		Validator.verifyThat("image is present",
				verifyPresent("image.postimage.postdetails.publisher"),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user verify posts count")
	public void verify_PostMade_PostPending_Count() {
		List<QAFWebElement> postCount = new QAFExtendedWebElement("xpath=/html")
				.findElements("count.postsmade.statistics.publisher");
		for (QAFWebElement count : postCount) {
			Validator.verifyThat("Posts Made and Posts pending count are present",
					count.verifyPresent(), Matchers.equalTo(true));
		}
	}

	@QAFTestStep(description = "user verify filter options are not present")
	public void verify_Filters() {
		List<QAFWebElement> calenderOption = new QAFExtendedWebElement("xpath=/html")
				.findElements("filter.calender.mypost.publisher");
		for (int i = 0; i < calenderOption.size(); i++) {
			Validator.verifyThat("filter options are not visible",
					calenderOption.get(i).verifyNotPresent(), Matchers.equalTo(true));
		}
		List<QAFWebElement> button = new QAFExtendedWebElement("xpath=/html")
				.findElements("button.search.reset.mypost.publisher");
		for (int j = 0; j < button.size(); j++) {
			Validator.verifyThat("search and reset buttons are not visible",
					button.get(j).verifyNotPresent(), Matchers.equalTo(true));
		}
	}

	@QAFTestStep(description = "user verify {0} Button")
	public void verifyAllButton(String btn) {
		QAFExtendedWebElement element = new QAFExtendedWebElement(String
				.format(ConfigurationManager.getBundle().getString("btn.all"), btn));
		element.waitForVisible(5000);
		Validator.verifyThat("Button is present", element.verifyPresent(),
				Matchers.equalTo(true));

	}
}
