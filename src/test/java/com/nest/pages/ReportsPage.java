package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.getText;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotPresent;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class ReportsPage {

	RnRPage requests = new RnRPage();

	@QAFTestStep(description = "verify reports look and feel")
	public void verifyReportsPage() {
		Validator.verifyThat("Reports header is visible!",
				CommonStep.getText("reports.header").toString().equals("Reports"),
				Matchers.equalTo(true));
		verifyFiltersForReports();
		verifyThreeButtons();
		verifyDateFormat();
		verifyTableColumnsForReports();
	}

	public static boolean isValidFormat(String format, String value) {
		Date date = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			date = sdf.parse(value);
			if (!value.equals(sdf.format(date))) {
				date = null;
			}
		} catch (ParseException ex) {
			ex.printStackTrace();
		}
		return date != null;
	}

	private void verifyDateFormat() {
		List<QAFWebElement> dateList =
				NestUtils.getQAFWebElements("//tr/td[@data-title='Posted Date']");
		for (QAFWebElement date : dateList) {
			isValidFormat("dd-MMM-yyyy", date.getText().toString().trim());
		}
	}

	private void verifyThreeButtons() {
		CommonStep.verifyVisible("search.button");
		CommonStep.verifyVisible("reset.button");
		CommonStep.verifyVisible("export.button");

	}

	private void verifyFiltersForReports() {
		String Actual_BreadcrumbLabel =
				CommonStep.getText("breadcrumb.label").toString().replaceAll(" ", "");
		String expected_BreadcrumbLabel = ("Home / R & R / Reports").replaceAll(" ", "");
		Validator.verifyThat("Breadcrumb is visible",
				Actual_BreadcrumbLabel.equals(expected_BreadcrumbLabel),
				Matchers.equalTo(true));

		CommonStep.verifyVisible("datepicker.from");
		CommonStep.verifyVisible("datepicker.to");
		Validator.verifyThat("Status is visible",
				requests.getElement("filter.label", "Status").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Reward name is visible",
				requests.getElement("filter.label", "Select Rewards").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Groups is visible",
				requests.getElement("filter.label", "Select Groups").isDisplayed(),
				Matchers.equalTo(true));

	}

	public void verifyTableColumnsForReports() {
		Validator.verifyThat("Nominator column is visible",
				requests.getElement("table.column", "Nominator").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Reward Name column is visible",
				requests.getElement("table.column", "Reward").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Nominee column is visible",
				requests.getElement("table.column", "Nominee").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Department column is visible",
				requests.getElement("table.column", "Group").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Posted Date column is visible",
				requests.getElement("table.column", "Posted Date").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Approval date column is visible",
				requests.getElement("table.column", "Approval Date").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("Manager Status column is visible",
				requests.getElement("table.column", "Manager Status").isDisplayed(),
				Matchers.equalTo(true));
		Validator.verifyThat("HR Status column is visible",
				requests.getElement("table.column", "HR Status").isDisplayed(),
				Matchers.equalTo(true));
	}

	@QAFTestStep(description = "user selects a reward card", stepName = "userSelectsACard")
	public void userSelectsACard() {
		int len = NestUtils.getQAFWebElements("list.cards").size();
		if (len > 0)
			NestUtils.getQAFWebElements("list.cards").get(0).click();
	}

	@QAFTestStep(description = "user posts a comment")
	public void postsComment() {
		QAFWebElement iframeLocator = NestUtils.getDriver()
				.findElement(By.xpath("//iframe[@id='ui-tinymce-1_ifr']"));
		NestUtils.getDriver().switchTo().frame(iframeLocator);
		QAFWebElement txtbox =
				NestUtils.getDriver().findElement(By.xpath("html/body/p[2]"));
		NestUtils.scrollUpToElement(txtbox);
		Actions actions = new Actions(NestUtils.getDriver());
		actions.moveToElement(txtbox);
		actions.click();
		actions.sendKeys(new RR_RequestPage().getRandomString(15));;
		actions.build().perform();
		NestUtils.getDriver().switchTo().defaultContent();
	}

	@QAFTestStep(description = "user enter key contributions")
	public void userEnterKeyContributions() {
		sendKeys("Excellent performance!", "input.key.contributions");
	}

	@QAFTestStep(description = "user clicks on {0} button")
	public void userClickOnPostButton(String buttonname) {
		NestUtils.scrollUpToElement(requests.getElement("button.name", buttonname));
		click(String.format(ConfigurationManager.getBundle().getString("button.name"),
				buttonname));
		CommonStep.waitForNotPresent(String.format(
				ConfigurationManager.getBundle().getString("button.name"), buttonname));
		waitForNotPresent("success.nominated.msg");
	}

	@QAFTestStep(description = "user verifies My Rewards for the same employee {0} and observe")
	public void verifyMyRewardsSection(String username) {
		Validator
				.verifyThat(
						"Nominated by verified!", getText("nominated.by.label").toString()
								.contains(username.replaceAll(".", "")),
						Matchers.equalTo(true));

	}

	@QAFTestStep(description = "user verifies R&R reports where {0} is {1}")
	public void userVerifiesRRReportsWhereIs(String filtername, String filterValue) {
		verifyTableColumnsForReports();
		verifyFilterRnRReportPages(filtername, filterValue);

	}

	public void verifyFilterRnRReportPages(String filtername, String filterValue) {
		List<QAFWebElement> statusList =
				NestUtils.getQAFWebElements(String.format(
						ConfigurationManager.getBundle().getString(
								"request.list.table.column.values.rnrreportspage"),
						filtername));
		for (QAFWebElement status : statusList) {
			Validator.verifyThat("status is " + filterValue,
					status.getText().toString().trim(),
					Matchers.equalToIgnoringWhiteSpace(filterValue));
		}
	}

	@QAFTestStep(description = "user selects from date {0} from calender of report page")
	public void userSelectsFromDateFromCalenderOfReportPage(String date) {
		RR_RequestPage.selectDate("date.to.rnrrequestpage", "btn.all.date.rnrrequestpage",
				date);

	}

	@QAFTestStep(description = "user selects to date {0} from calender of report page")
	public void userSelectsToDateFromCalenderOfReportPage(String date) {
		RR_RequestPage.selectDate("date.to.rnrrequestpage", "btn.all.date.rnrrequestpage",
				date);

	}

	@QAFTestStep(description = "user click reset button")
	public void userClickResetButton() {
		QAFWebElement resetButton = NestUtils.getDriver().findElement("reset.button");
		NestUtils.scrollUpToElement(resetButton);
		waitForVisible("search.button");
		NestUtils.clickUsingJavaScript(resetButton);
	}

	@QAFTestStep(description = "user verifies that R&R reports fields should be clear")
	public void userVerifiesThatRRReportsFieldsShouldBeClear() {
		List<QAFWebElement> options =
				NestUtils.getQAFWebElements("dropdown.select.fileds.rnrreportpage");
		for (QAFWebElement e : options) {
			String text = e.getText();
			if ((text).contains("Select")) {
				Reporter.log(text + " field has been cleared", MessageTypes.Pass);

			} else {
				Reporter.log(text + " field not cleared", MessageTypes.Fail);

			}
		}
		
		LocalDate today = LocalDate.now();
		String firstDay=today.withDayOfMonth(1).toString();
		String lastDay=today.withDayOfMonth(today.lengthOfMonth()).toString();
		String fromDate = CommonStep.getText("datepicker.from").toString();
		String toDate = CommonStep.getText("datepicker.to").toString();		
		verifyDate(firstDay, fromDate);
		verifyDate(lastDay, toDate);
	}
	
	public static void verifyDate(String actutualDate,String comparedate)
	{
		if(actutualDate.contains(comparedate)) 
		{	
			Reporter.logWithScreenShot("Date hase been cleared", MessageTypes.Pass);
		}
		else
			Reporter.logWithScreenShot("Date hase not been cleared", MessageTypes.Fail);
	}

}
