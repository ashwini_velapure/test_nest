package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.verifyNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;
import static com.qmetry.qaf.automation.step.CommonStep.verifyVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;

import java.util.List;

import org.hamcrest.Matchers;

import com.nest.components.VisaRequestComponent;
import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class VisaRequests extends WebDriverBaseTestPage<WebDriverTestPage> {

	CommonPage common = new CommonPage();
	@FindBy(locator = "rr.requests.table")
	private List<VisaRequestComponent> visasRequestList;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	@QAFTestStep(description = "user switch the window to manager view")
	public void switchwindow() {
		common.switchToViews();
	}

	@QAFTestStep(description = "user clicks on New Visa Requests")
	public void clickNewVisaRequest() {
		// common.switchToViews();
		waitForNotVisible("wait.loader");
		click("btn.newvisarequest.visapage");

	}

	@QAFTestStep(description = "user should see Initiate Visa Request page")
	public void verifyNewVisaRequestpage() {
		waitForNotVisible("wait.loader");
		Validator.verifyThat(CommonStep.getText("label.heading.visapage"),
				Matchers.containsString("Initiate Visa Request"));
		verifyPresent("label.employeename.visapage");
	}

	@QAFTestStep(description = "user verifies the breadcrumb on visa page {0}")
	public void verifybreadcrumbvisapage(String visabreadcrumb) {
		waitForNotVisible("wait.loader");
		String breadcrumb =
				new QAFExtendedWebElement("label.breadcrumb.visapage").getText();
		if (breadcrumb.contains(visabreadcrumb)) {
			Reporter.log("breadcrumb is present as per menu navigation",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("breadcrumb is not present as per menu navigation",

					MessageTypes.Fail);

		}
	}
	
	@QAFTestStep(description = "user should be able to click on employee name dropdown")
	public void clickonemployeename() {
		waitForNotVisible("wait.loader");
		click("drpdown.employeename.visapage");
	}
	
	@QAFTestStep(description = "user enters text {0} in search textbox")
	public void enteremployeename(String employeename) {
		sendKeys(employeename, "txtbox.search.visapage");
	}
	
	@QAFTestStep(description = "user should be able to see employee name in dropdown")
	public void verifyemployeename() {
		verifyPresent("label.searchemployeename.visapage");
	}
	
	@QAFTestStep(description = "click on employeename")
	public void clickonserachemployeename() {
		waitForNotVisible("wait.loader");
		click("label.clickemployeename.visapage");
	}

	@QAFTestStep(description = "user should verify the details with name and designation as {0} {1}")
	public void verifyfieldformat(String employeename, String designation) {
		waitForNotVisible("wait.loader");
		verifyPresent("img.employeename.visapage");
		Validator.verifyThat(CommonStep.getText("label.employeedetail.visapage"),
				Matchers.containsString(employeename));
		// verifyPresent("label.employeedetail.visapage");
		verifyPresent("btn.reset.visapage");
		Validator.verifyThat(CommonStep.getText("label.designation.visapage"),
				Matchers.containsString(designation));
		QAFWebElement editButton = NestUtils.getDriver().findElement("img.edit.visapage");
		NestUtils.scrollUpToElement(editButton);
		verifyPresent("img.edit.visapage");
		driver.findElement("txtbox.lastname.visapage").verifyDisabled();
		driver.findElement("txtbox.firstname.visapage").verifyDisabled();
		driver.findElement("txtbox.title.visapage").verifyDisabled();
		Validator.verifyThat(CommonStep.getText("label.rmg.visapage"),
				Matchers.containsString("Existing Allocation Details (RMG)"));
		String countrytovisit = driver.findElement("drpdown.countrytovisit.visapage")
				.getAttribute("data-toggle");
		if (countrytovisit.contains("dropdown")) {
			Reporter.log("field format is dropdown", MessageTypes.Pass);
		}
		String visareadydate = driver.findElement("calendar.visareadydate.visapage")
				.getAttribute("uib-datepicker-popup");
		if (visareadydate.contains("dropdown")) {
			Reporter.log("field format is dropdown", MessageTypes.Pass);
		}
		QAFWebElement initiateButton =
				NestUtils.getDriver().findElement("label.heading.visapage");
		NestUtils.scrollUpToElement(initiateButton);
		verifyPresent("img.attachment.visapage");
		verifyPresent("btn.back.visapage");
		verifyPresent("label.heading.visapage");

	}

	@QAFTestStep(description = "user clicks on filter icon")
	public void clicksOnFilter() {
		NestUtils.scrollUpToElement(new QAFExtendedWebElement("filter.icon"));
		NestUtils.clickUsingJavaScript(new QAFExtendedWebElement("filter.icon"));
	}

	@QAFTestStep(description = "user verifies filters are {0}")
	public void launchNest(String visible) {
		if (visible.equals("visible")) {
			verifyVisible("date.picker.initited.from");
			verifyVisible("date.picker.initited.to");
			verifyVisible("dropdown.select.employee");
			verifyVisible("dropdown.select.visa.request.status");
			verifyVisible("dropdown.select.visa.status");
			verifyVisible("dropdown.select.country");
			verifyVisible("dropdown.select.type.of.visa");

		} else {
			verifyNotVisible("date.picker.initited.from");
			verifyNotVisible("date.picker.initited.to");
			verifyNotVisible("dropdown.select.employee");
			verifyNotVisible("dropdown.select.visa.request.status");
			verifyNotVisible("dropdown.select.visa.status");
			verifyNotVisible("dropdown.select.country");
			verifyNotVisible("dropdown.select.type.of.visa");
		}
	}

	@QAFTestStep(description = "click on employee name")
	public void clickemployeenametable() {
		waitForNotVisible("wait.loader");
		click("label.employeedetail.visapage");
	}

	@QAFTestStep(description = "user should verify the employee detail")
	public void verifyemployeedetail() {
		Validator.verifyThat(CommonStep.getText("label.visarequest.visapage"),
				Matchers.containsString("Visa Request Details"));
	}

	@QAFTestStep(description = "user should see visa details page with colors according to the theme of the application")
	public void verifyUI() {

		String color =
				driver.findElement("btn.search.visapage").getCssValue("background-color");
		Reporter.log(color);
		Validator.verifyThat("rgba(235, 114, 3, 1)", Matchers.containsString(color));
	}

	@QAFTestStep(description = "user verifies the breadcrumb on visa request page {0}")
	public void verifyvisarequestbreadcrumb(String visarequestbreadcrumb) {
		waitForNotVisible("wait.loader");
		String breadcrumb =
				new QAFExtendedWebElement("label.breadcrumbvisarequest.visapage")
						.getText();
		if (breadcrumb.contains(visarequestbreadcrumb)) {
			Reporter.log("breadcrumb is present as per menu navigation",
					MessageTypes.Pass);
		} else {
			Reporter.logWithScreenShot("breadcrumb is not present as per menu navigation",

					MessageTypes.Fail);

		}
	}
}
