package com.nest.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.verifyVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;
import java.util.List;
import java.util.Set;
import org.hamcrest.Matchers;
import org.openqa.selenium.WebElement;

import com.nest.beans.LoginBean;
import com.nest.utilities.NestUtils;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class LandingPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "confirmation.reviews.employees")
	private List<QAFWebElement> reviewConfirmationEmployees;
	@FindBy(locator = "post.confirmation.feedback.icon")
	private List<QAFWebElement> postConfirmationFeedbackIcon;
	
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {

	}

	@QAFTestStep(description = "user on nest landing page")
	public void userAmOnNestLandingPage() {
		driver.manage().deleteAllCookies();
		driver.get("/");		
	}

	@QAFTestStep(description = "user login with {0} and {1}")
	public void doLogin(String username, String password) {
		sendKeys(username, "input.username.loginpage");
		sendKeys(password, "input.password.loginpage");
		WebElement login = new QAFExtendedWebElement("button.login.loginpage");
		NestUtils.clickUsingJavaScript(login);
		driver.manage().window().maximize();
		waitForNotVisible("wait.loader");

	}
	
	@QAFTestStep(description = "user enter login details as {0}")
	public void loginDetails(Object userDetails) {
		LoginBean loginBean = new LoginBean();
		if (userDetails instanceof String) {

			loginBean.fillFromConfig((String) userDetails);
		} else {
			loginBean.fillData(userDetails);
		}
		
		sendKeys(loginBean.getUserName(), "input.username.loginpage");
		sendKeys(loginBean.getPassword(), "input.password.loginpage");
		WebElement login = new QAFExtendedWebElement("button.login.loginpage");
		NestUtils.clickUsingJavaScript(login);
		driver.manage().window().maximize();
		waitForNotVisible("wait.loader");
	}

	@QAFTestStep(description = "user enter login details as {0} and select remember-me checkbox")
	public void loginDetailsWithRememberMe(Object userDetails) {
		LoginBean loginBean = new LoginBean();
		if (userDetails instanceof String) {

			loginBean.fillFromConfig((String) userDetails);
		} else {
			loginBean.fillData(userDetails);
		}
		
		sendKeys(loginBean.getUserName(), "input.username.loginpage");
		sendKeys(loginBean.getPassword(), "input.password.loginpage");
		CommonPage.clickUsingJavaScript("rememberme.checkbox");
		WebElement login = new QAFExtendedWebElement("button.login.loginpage");
		NestUtils.clickUsingJavaScript(login);
		driver.manage().window().maximize();
		waitForNotVisible("wait.loader");
	}
	@QAFTestStep(description = "user should see error message {0}")
	public void errorMessageVerification(String errorMsg) {
		WebElement error = new QAFExtendedWebElement("error.authentication");
		
		String actualError = NestUtils.getText(driver,error);
		
		Validator.verifyThat("Authentication Validation",
				actualError,Matchers.containsString(errorMsg));
}

	

	@QAFTestStep(description = "user clicks on forgot password")
	public void verifyForgotPasswordLink() {
		WebElement forgotPassword = new QAFExtendedWebElement("btn.forgotpasswd.loginpage");
		NestUtils.clickUsingJavaScript(forgotPassword);
		
	}

	@QAFTestStep(description = "user should be navigated to forget password URL of the Infostretch")
	public void verifyNavigationToInfostretchPage() {

		Set<String> winHandle = driver.getWindowHandles();

		for (String win : winHandle) {
			driver.switchTo().window(win);
		}
		Validator.verifyThat(driver.getTitle(),
				Matchers.containsString("Password Reset Infostretch"));
	}

	@QAFTestStep(description = "user logged out from account")
	public void loggedOut() {
		waitForNotVisible("loader.userhomepage");
		NestUtils.clickUsingJavaScript("button.logout.userhomepage");
		
	}

	@QAFTestStep(description = "user verify {0} and {1}")
	public void verifyLoginPage(String username, String password) {

		Validator.verifyThat(CommonStep.getText("input.username.loginpage"),
				Matchers.containsString(username));
	}
	
	@QAFTestStep(description = "user switches to manager view")
	public void switchToManagerView() {
		waitForVisible("manager.view");
		CommonPage.clickUsingJavaScript("manager.view");
	}

	@QAFTestStep(description = "user verifies confirmation reviews section")
	public void verifyConfirmationReviewsSection() {
		verifyVisible("confirmation.reviews.section");
	}

	@QAFTestStep(description = "user clicks on employee on confirmation review section")
	public void clickOnEmployeeOnConfirmationReviewSection() {
		if (new QAFExtendedWebElement("confirmation.noreviewspending.text").isPresent()) {
			Reporter.log("There are no employees available to review!-> " + CommonStep
					.getText("confirmation.noreviewspending.text").toString());
		} else {
			waitForVisible("confirmation.reviews.employees");
			Validator.verifyThat("employee list is available!",
					reviewConfirmationEmployees.size() > 0, Matchers.equalTo(true));
			if (reviewConfirmationEmployees.size() > 0) {
				NestUtils.clickUsingJavaScript(postConfirmationFeedbackIcon.get(0));
			}
			Validator.verifyThat(
					"User should be navigated to confirmation page of the employee",
					driver.getCurrentUrl().toString().contains(
							"postconfirmationfeedback"),
					Matchers.equalTo(true));
		}
	}

	
	@QAFTestStep(description = "user click on back button")
	public void userClickOnBackButton() {
		waitForNotVisible("loader.userhomepage");
		driver.navigate().back();
	}

	
	@QAFTestStep(description = "user should be navigated to login page")
	public void userShouldBeNavigatedToLoginPage() {
		Validator.verifyThat(driver.getTitle(),
				Matchers.containsString("Infostretch NEST"));
	}

}
